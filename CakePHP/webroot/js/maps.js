$( document ).ready(function() {
    $( ".sites" ).click(function() {
        var siteId = $(this).data("site-id");
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/getSiteLatLon",
            data: {siteId : siteId},
            success: function(data) {
                var parkingLotsInfo = JSON.parse(data);
                inittMap(parkingLotsInfo);

            },
        });
    });

    function inittMap(parkingLotsInfo) {
        var w = window.innerWidth;
        var zoom = 18
        console.log(w);
        if (w < 500) {
            zoom = 17;
        }
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: parkingLotsInfo['siteLatLon'],
            mapTypeId: 'terrain',
            maxWidth: 200
        });

        var description = [];
        var status = [];
        var color = [];
        var parkingId = [];
        for (var key in parkingLotsInfo['details']) {

            var obj = parkingLotsInfo['details'][key];
            for (var prop in obj) {
                description.push([obj[prop]['description']]);
                status.push([obj[prop]['status']]);
                color.push([obj[prop]['color']]);
                parkingId.push([obj[prop]['id']]);
            }
        }

        infowindow = new google.maps.InfoWindow({});
        var menu ="";
        for (i = 0; i < parkingId.length; i++) {
            menu +='<li><a class="md-menu-click" data-parking-id="'+parkingId[i]+'"> P' + parkingId[i] + '</a></li>';
        }
        console.log(menu);
        for (var key in parkingLotsInfo['parkingLatLon']) {

            // Define the LatLng coordinates for the polygon's path.
            var triangleCoords = [
                parkingLotsInfo['parkingLatLon'][key]
            ];
            // Construct the polygon.
            var bermudaTriangle = new google.maps.Polygon({
                paths: triangleCoords,
                strokeColor: color[key],
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: color[key],
                fillOpacity: 0.6
            });

            var contentString = '<div class="md-modal md-effect-1" id="modal-1" style="width:450px;height:600px">'+
                '<div class="md-content">'+
                    '<div id="bodyContent">'+
                        '<div id="md-menu">'+
                            '<ul>'+
                                menu+
                            '</ul>'+
                        '</div>'+
                        '<div id="md-description" style="margin-top: 20px;border-bottom: 1px solid #0000cc; padding-bottom:15px;">'+
                            '<div id="md-sub-menu">'+
                                '<ul>'+
                                    '<li><a>Status</a></li>'+
                                    '<li><a class="modal-reservation" data-reservation-parking-id="'+parkingId[key]+'">Reservation</a></li>'+
                                    '<li><a class="modal-reservation-request" data-reservation-parking-id="'+parkingId[key]+'">Reservation Request</a></li>'+
                                '</ul>'+
                            '</div>'+
                            '<p style="border-bottom: 1px solid #0000cc"><b>'+description[key]+'</b></p>'+
                            '<p>Current Status: '+ status[key]+'</p>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '</div>';

            google.maps.event.addListener(bermudaTriangle,'click', (function(bermudaTriangle,contentString,infowindow){
                return function() {
                    infowindow.setContent(contentString);
                    infowindow.open(map,bermudaTriangle);

                };
            })(bermudaTriangle,contentString,infowindow));

            google.maps.event.addListener(bermudaTriangle, 'click', function ( event) {
                infowindow.setPosition(event.latLng);
            });

            bermudaTriangle.setMap(map);
        }

    }


    $( "body" ).delegate( ".md-menu-click", "click", function() {
        var parkingId = $(this).data("parking-id");
        baseUrl = window.location;
        console.log("test");
        $.ajax({
            type: "GET",
            url: baseUrl+"/getParkingLot",
            data: {parkingId : parkingId},
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".free-click", "click", function() {
        var freeParkingId = $(this).data("free-parking-id");
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/getParkingLot",
            data: {freeParkingId : freeParkingId},
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".block-click", "click", function() {
        var blockParkingId = $(this).data("block-parking-id");
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/getParkingLot",
            data: {blockParkingId : blockParkingId, block: "blocked"},
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".modal-status", "click", function() {
        var reservationParkingId = $(this).data("reservation-parking-id");
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/getParkingLot",
            data: {parkingId : reservationParkingId},
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".modal-reservation", "click", function() {
        var reservationParkingId = $(this).data("reservation-parking-id");
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/getReservationsList",
            data: {reservationParkingId : reservationParkingId},
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".modal-reservation-request", "click", function() {
        var reservationParkingId = $(this).data("reservation-parking-id");
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/getReservationRequestsList",
            data: {reservationParkingId : reservationParkingId},
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".reservation-add-modal", "click", function() {
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/reservationAdd",
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".reservation-request-add-modal", "click", function() {
        baseUrl = window.location;
        $.ajax({
            type: "GET",
            url: baseUrl+"/reservationRequestAdd",
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".delete-reservation", "click", function() {
        var reservationId = $(this).data("reservation-id");
        var parkingId = $(this).data("parking-id");
        console.log ("parking  ::  "+parkingId);
        var modal = 1;
        baseUrl =  window.location
        console.log("id: "+reservationId);
        $.ajax({
            type: "GET",
            url: baseUrl+"/reservationDelete",
            data: {id : reservationId, parkingId: parkingId },
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".delete-reservation-request", "click", function() {
        var reservationRequestId = $(this).data("reservation-request-id");
        var parkingId =  $(this).data("parking-id");
        baseUrl =  window.location
        $.ajax({
            type: "GET",
            url: baseUrl+"/reservationRequestDelete",
            data: {reservationRequestId : reservationRequestId, parkingId: parkingId },
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);
            },
        });
    });

    $( "body" ).delegate( ".reservation-request-add-reservation", "click", function() {
        var reservationRequestId = $(this).data("reservation-request-id");
        var parkingId =  $(this).data("parking-id");
        baseUrl =  window.location
        $.ajax({
            type: "GET",
            url: baseUrl+"/reservationRequestAccept",
            data: {reservationRequestId : reservationRequestId, parkingId: parkingId },
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( ".edit-reservation", "click", function() {
        var reservationId = $(this).data("reservation-id");
        var parkingId = $(this).data("parking-id");
        console.log ("parking  ::  "+parkingId);
        var modal = 1;
        baseUrl =  window.location
        console.log("id: "+reservationId);
        $.ajax({
            type: "GET",
            url: baseUrl+"/reservationEdit",
            data: {id : reservationId, parkingId: parkingId },
            success: function(data) {
                //    var parkingLotsInfo = JSON.parse(data);
                //   inittMap(parkingLotsInfo);
                $('#md-description').html(data);

            },
        });
    });

    $( "body" ).delegate( "#md-menu li a", "click", function() {
        $('#md-menu li a').removeClass('active');
        $(this).addClass('active');
    });

    $( ".nav .map-menu a" ).click(function() {
        $('.nav .map-menu a').removeClass('active');
        $(this).addClass('active');
    });

    $( "body" ).delegate( ".reservation-edit-submit-form form", "submit", function(event) {
        event.preventDefault();
        console.log("test");
        var $form = $(this),
            url = $form.attr('action');
        console.log($form);
        // Use Ajax to submit form data
        $.ajax({
            url: url,
            type: 'POST',
            data: $form.serialize(),
            success: function(data) {
                $('#md-description').html(data);
            }
        });
    });

    $( "body" ).delegate( ".reservation-request-accept-submit-form form", "submit", function(event) {
        event.preventDefault();
        var $form = $(this),
            url = $form.attr('action');
        // Use Ajax to submit form data
        $.ajax({
            url: url,
            type: 'POST',
            data: $form.serialize(),
            success: function(data) {
                $('#md-description').html(data);
            }
        });
    });

    $( "body" ).delegate( ".reservation-add-submit-form form", "submit", function(event) {
        event.preventDefault();
        console.log("reservation add");
        var $form = $(this),
            url = $form.attr('action');
        // Use Ajax to submit form data
        $.ajax({
            url: url,
            type: 'POST',
            data: $form.serialize(),
            success: function(data) {
                $('#md-description').html(data);
            }
        });
    });
});

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: {lat: 49.8743061, lng: 8.6599848},
        mapTypeId: 'terrain'
    });

    // Define the LatLng coordinates for the polygon's path.
    var triangleCoords = [
    ];

    // Construct the polygon.
    var bermudaTriangle = new google.maps.Polygon({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35
    });
    bermudaTriangle.setMap(map);
}

// This example creates a simple polygon representing the Bermuda Triangle.

