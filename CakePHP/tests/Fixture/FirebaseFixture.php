<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FirebaseFixture
 *
 */
class FirebaseFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'firebase';


    /**
     * Records
     *
     * @var array
     */
    public $records = array(
      array('users_id' => '1','token' => 'FirebaseToken1'),
      array('users_id' => '2','token' => 'FirebaseToken2'),
      array('users_id' => '3','token' => 'FirebaseToken3.1'),
      array('users_id' => '3','token' => 'FirebaseToken3.2'),
      array('users_id' => '4','token' => 'FirebaseToken4'),
      array('users_id' => '5','token' => 'FirebaseToken5')


    );

}
