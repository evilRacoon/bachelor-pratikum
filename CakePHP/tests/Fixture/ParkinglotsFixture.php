<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ParkinglotsFixture
 *
 */
class ParkinglotsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'pid' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'description' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'mqttchannel' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'gateway_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'status' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'error', 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ip' => ['type' => 'string', 'length' => 15, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'hwid' => ['type' => 'string', 'length' => 18, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'lastStateChange' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        'type' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'locationId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'pid_2' => ['type' => 'index', 'columns' => ['pid'], 'length' => []],
            'pid_3' => ['type' => 'index', 'columns' => ['pid'], 'length' => []],
            'gateway_id_fk_idx' => ['type' => 'index', 'columns' => ['gateway_id'], 'length' => []],
            'fk_parkinglots_locationId' => ['type' => 'index', 'columns' => ['locationId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'pid' => ['type' => 'unique', 'columns' => ['pid'], 'length' => []],
            'pid_4' => ['type' => 'unique', 'columns' => ['pid'], 'length' => []],
            'fk_gateway_id' => ['type' => 'foreign', 'columns' => ['gateway_id'], 'references' => ['gateways', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_parkinglots_locationId' => ['type' => 'foreign', 'columns' => ['locationId'], 'references' => ['locations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'pid' => 1,
            'description' => 'Lorem ipsum dolor sit amet',
            'mqttchannel' => 'parking/1',
            'gateway_id' => 1,
            'status' => 'free',
            'ip' => 'Lorem ipsum d',
            'hwid' => 'Lorem ipsum dolo',
            'lastStateChange' => '2018-01-01 00:00:00',
            'type' => 'SINGLE',
            'locationId' => 1
        ],
        [
            'id' => 2,
            'pid' => 2,
            'description' => 'Lorem ipsum dolor sit amet',
            'mqttchannel' => 'parking/2',
            'gateway_id' => 1,
            'status' => 'free',
            'ip' => 'Lorem ipsum d',
            'hwid' => 'Lorem ipsum dolo',
            'lastStateChange' => '2018-01-01 00:00:00',
            'type' => 'SINGLE',
            'locationId' => 2
        ],
        [
            'id' => 3,
            'pid' => 3,
            'description' => 'Lorem ipsum dolor sit amet',
            'mqttchannel' => 'parking/3',
            'gateway_id' => 1,
            'status' => 'free',
            'ip' => 'Lorem ipsum d',
            'hwid' => 'Lorem ipsum dolo',
            'lastStateChange' => '2018-01-01 00:00:00',
            'type' => 'SINGLE',
            'locationId' => 3
        ],
        [
            'id' => 4,
            'pid' => 4,
            'description' => 'Lorem ipsum dolor sit amet',
            'mqttchannel' => 'parking/4',
            'gateway_id' => 1,
            'status' => 'free',
            'ip' => 'Lorem ipsum d',
            'hwid' => 'Lorem ipsum dolo',
            'lastStateChange' => '2018-01-01 00:00:00',
            'type' => 'SINGLE',
            'locationId' => 4
        ],

        [
            'id' => 5,
            'pid' => 5,
            'description' => 'Lorem ipsum dolor sit amet',
            'mqttchannel' => 'parking/5',
            'gateway_id' => 1,
            'status' => 'free',
            'ip' => 'Lorem ipsum d',
            'hwid' => 'Lorem ipsum dolo',
            'lastStateChange' => '2018-01-01 00:00:00',
            'type' => 'SINGLE',
            'locationId' => 5
        ]
    ];
}
