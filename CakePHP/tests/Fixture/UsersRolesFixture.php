<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersRolesFixture
 *
 */
class UsersRolesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'role_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'user_id' => 1,
            'role_id' => 1
        ],

        [
            'id' => 2,
            'user_id' => 2,
            'role_id' => 2
        ],

        [
            'id' => 3,
            'user_id' => 3,
            'role_id' => 3
        ],

        [
            'id' => 4,
            'user_id' => 4,
            'role_id' => 4
        ],

        [
            'id' => 5,
            'user_id' => 5,
            'role_id' => 5
        ],

        [
            'id' => 6,
            'user_id' => 6,
            'role_id' => 1
        ],

        [
            'id' => 7,
            'user_id' => 7,
            'role_id' => 2
        ],

        [
            'id' => 8,
            'user_id' => 8,
            'role_id' => 3
        ],

        [
            'id' => 9,
            'user_id' => 9,
            'role_id' => 4
        ],

        [
            'id' => 10,
            'user_id' => 10,
            'role_id' => 5
        ]
    ];
}
