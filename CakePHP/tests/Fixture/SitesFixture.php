<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SitesFixture
 *
 */
class SitesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    /*
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'prefix' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'locationID' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'isPublic' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'publicStart' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'publicEnd' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'isPublicRecurring' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'recurringId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_sites_locationID' => ['type' => 'index', 'columns' => ['locationID'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_sites_locationID' => ['type' => 'foreign', 'columns' => ['locationID'], 'references' => ['locations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];*/
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'Site von 1',
            'description' => 'Lorem ipsum dolor sit amet',
            'prefix' => 'Lorem ipsum dolor sit amet',
            'locationID' => 1,
            //'isPublic' => 'Lorem ipsum dolor sit amet',
            'publicStart' => '2017-09-11 18:20:20',
            'publicEnd' => '2018-09-11 18:20:20',
            //'isPublicRecurring' => 'Lorem ipsum dolor sit amet',
            'recurringId' => 1
        ],

        [
            'id' => 2,
            'name' => 'Site von 2',
            'description' => 'Lorem ipsum dolor sit amet',
            'prefix' => 'Lorem ipsum dolor sit amet',
            'locationID' => 2,
            //'isPublic' => 'Lorem ipsum dolor sit amet',
            'publicStart' => '2017-09-11 18:20:20',
            'publicEnd' => '2018-09-11 18:20:20',
            //'isPublicRecurring' => 'Lorem ipsum dolor sit amet',
            'recurringId' => 1
        ],

        [
            'id' => 3,
            'name' => 'Site von 3',
            'description' => 'Lorem ipsum dolor sit amet',
            'prefix' => 'Lorem ipsum dolor sit amet',
            'locationID' => 3,
            //'isPublic' => 'Lorem ipsum dolor sit amet',
            'publicStart' => '2017-09-11 18:20:20',
            'publicEnd' => '2018-09-11 18:20:20',
            //'isPublicRecurring' => 'Lorem ipsum dolor sit amet',
            'recurringId' => 1
        ],

        [
            'id' => 4,
            'name' => 'Site von 4',
            'description' => 'Lorem ipsum dolor sit amet',
            'prefix' => 'Lorem ipsum dolor sit amet',
            'locationID' => 4,
            //'isPublic' => 'Lorem ipsum dolor sit amet',
            'publicStart' => '2017-09-11 18:20:20',
            'publicEnd' => '2018-09-11 18:20:20',
            //'isPublicRecurring' => 'Lorem ipsum dolor sit amet',
            'recurringId' => 1
        ],

        [
            'id' => 5,
            'name' => 'Site von 5',
            'description' => 'Lorem ipsum dolor sit amet',
            'prefix' => 'Lorem ipsum dolor sit amet',
            'locationID' => 5,
            //'isPublic' => 'Lorem ipsum dolor sit amet',
            'publicStart' => '2017-09-11 18:20:20',
            'publicEnd' => '2018-09-11 18:20:20',
            //'isPublicRecurring' => 'Lorem ipsum dolor sit amet',
            'recurringId' => 1
        ]
    ];
}
