<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;
/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.failed_login_attempts',
        'app.users',
        'app.roles',
        'app.users_roles',
        'app.authAcl',
        'app.zones',
        'app.users_zones'
    ];


    public function setUp(){
        parent::setup();  
        //If Users are added in the UsersFixture, they should be included
        $this->global_users = TableRegistry::get('Users')->find()->toArray(); 
    }


    /** 
        CONFIGURATION
        ===============================================================================
    */
        //Define which keys should be checked to be included/excluded from a response -> See "Schnittstellendefinition"
        public $json_return_keys = ['id', 
                            'username', 
                            'created', 
                            'modified', 
                            'email', 
                            'mobileNumber',
                            'landlineNumber',
                            'firstname',
                            'lastname',
                            'title',
                            'gender',
                            'userState',
                            'company',
                            'department',
                            'street',
                            'ZIP',
                            ];

        //Define which ROLES are allowed and not allowed to access the location /users/getUser.json
         public $authorized_roles = ['Admin',  'Visitor', 'Reservation Manager'];                  
         public $unauthorized_roles = ['Guest', 'Facility Manager'];  

         public $path = '/users/getUser.json' ;

         public $global_users;

        /*
        ======================================================================================
        */
        
        // HELPERS
        function doesContain($array){
            foreach ($array as $key) {
                $this->assertResponseContains($key);
            }
        }

        function doesNOTContain($array){
            foreach ($array as $key) {
                $this->assertResponseNOTContains($key);
            }
        }

    /**
       ===============================================================================
    */

    /**
     * Test getUser method
     * Key Assertions: Requester is given no Information about the Database or the Users inside it while informing about the 
     * problem of not beeing Authenticated
     * For further details check the documentation
     * @return void
     */
    public function testGetUserUnauthenticated()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'No Valid User',
                'PHP_AUTH_PW' => 'Random',
            ]
        ]);

        $this->get($this->path);
        //debug($this->_response->body());
        $this->assertResponseError();
        $this->assertResponseCode(401);
        $this->assertResponseNotEmpty();
        $this->assertResponseContains('Unauthorized');

        // Response Contains no Username
        foreach ($this->global_users as $global_user) {
            $this->assertResponseNotContains($global_user['username']);
        }
        //

        //Check if no fields according to the "Schnittstellendefinition" are retrieved
        $this->doesNOTContain($this->json_return_keys);
        //------------------//

        $this->assertContentType('application/json');
    }

    /**
     * Test getUser method
     * Key Assertion: Requester is given no Information about the Database or the Users inside it while informing about
     * the problem of not beeing Authenticated
     * For further details check the documentation 
     * @return void
     */
    public function testGetUserUnAuthorized()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are NOT allowed to access the requested function. Thus are authorized.
        $users = $this->unauthorized_roles;

        //Send a request for every role that is NOT allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);

            $this->get($this->path);
            //debug($this->_response->body());

            $this->assertResponseError();
            $this->assertResponseCode(403);
            $this->assertResponseNotEmpty();
            $this->assertResponseContains('You are not authorized to access that location.');

            $this->assertResponseNotContains($user);

            //Check if no fields according to the "Schnittstellendefinition" are retrieved
            $this->doesNOTContain($this->json_return_keys);
            //------------------//
            
            // Response Contains no User
            foreach ($this->global_users as $global_user) {
                $this->assertResponseNotContains($global_user['username']);
            }
            //
            
           
            
            $this->assertContentType('application/json');
        }

        if(empty($users)){
            echo("\n\nWARNING: No meaningfull assertions were made in UsersController.testGetUserUnAuthorized()! \nIf every role is authorized to access /users/getUser.json, ignore this message\n\n");
            $this->assertEquals(true, true);
            
        }
        
    }

    /**
     * Test getUser method
     * Key Assertion: Authorized requester is given exactly his and only his information about his corresponding User
     * For further details check the documentation
     * @return void
     */
    public function testGetUserAuthorized()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = $this->authorized_roles;

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);

            $this->get($this->path);
            //debug($this->_response->body());
            $this->assertResponseOk();
            $this->assertResponseNotEmpty();
            $this->assertResponseCode(200);
            $this->assertResponseContains($user);

            //Check if all fields according to the "Schnittstellendefinition" are retrieved
            $this->doesContain($this->json_return_keys);
            
            //Check if names of all other users are not retrieved
            foreach ($this->global_users as $global_user) {
                if($user == $global_user['username']) continue;
                $this->assertResponseNotContains($global_user['username']);
            }

                       
            
            $this->assertContentType('application/json');
        }

        if(empty($users)){
            echo("\n\nWARNING: No meaningfull assertions were made in UsersController.testGetUserAuthorized()! \n\nIf no role is authorized to access /users/getUser.json, ignore this message");
            $this->assertEquals(true, true);
            
        }
        
    }

    /**
     * Test index method with a session
     * Key Assertion: Authorized requester is given exactly his and only his information about his corresponding User
     * For further details check the documentation
     * @return void
     */
    public function testIndexAuthorizedSession()
    {
        //Fixtures are chosen so that username == password == role
        //Should contain all roles that are allowed to access the requested function. Thus are authorized.
        $users = ['Admin', 'Guest'];

        //Send a request for every role that is allowed to make the request
        foreach ($users as $user)  {
            //debug($user);
            $this->configRequest([
                'environment' => [
                    'PHP_AUTH_USER' => $user,
                    'PHP_AUTH_PW' => $user,
                ]
            ]);
            $this->session([
                'Auth' => [
                    'User' => [
                        'id'=>1,
                        'username' => 'Admin',
                        'password' => 'Admin'
                    ]
                ]
            ]);

            $this->get('users/index');
            
            //debug($this->_response->body());
            $this->assertResponseOk();
            $this->assertResponseNotEmpty();
            $this->assertResponseCode(200);
            $this->assertResponseContains($user);

            
            //Check if names of all other users are retrieved
            
            foreach ($this->global_users as $global_user) {
                $this->assertResponseContains($global_user['username']);
            }
            

                       
            
            //$this->assertContentType('application/json');
        }

        if(empty($users)){
            echo("\n\nWARNING: No meaningfull assertions were made in UsersController.testGetUserAuthorized()! \n\nIf no role is authorized to access /users/getUser.json, ignore this message");
            $this->assertEquals(true, true);
            
        }
        
    }

    

    /**
    Tests for view, add, edit and delete are missing
    */

}
