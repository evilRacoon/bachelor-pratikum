<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SitesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;
/**
 * App\Controller\SitesController Test Case
 */
class FirebaseControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.firebase',
        'app.users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
       
    ];

    public function setUp(){
        parent::setup();  
    }


    /** 
        CONFIGURATION
        ===============================================================================
    */

        //Define which ROLES are allowed and not allowed to access the location /sites.json
         public $authorized_roles = ['Visitor'];                  
         public $unauthorized_roles = [ 'Admin','Guest', 'Facility Manager', 'Reservation Manager'];                   
        /*
        ======================================================================================
        */
        

    /**
       ===============================================================================
    */

    /**
     * Test get method
     * Key Assertions: Requester is given no Information about the Database or the Users inside it while informing about the 
     * problem of not beeing Authenticated
     * For further details check the documentation
     * @return void
     */
    public function testGetAuthorized()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Visitor',
                'PHP_AUTH_PW' => 'Visitor',
            ]
        ]);

        $this->get('/firebase/get.json');
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');
    }

    /**
     * Test get method
     * Key Assertions: Requester is given no Information about the Database or the Users inside it while informing about the 
     * problem of not beeing Authenticated
     * For further details check the documentation
     * @return void
     */
    public function testAddAuthorized()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Visitor',
                'PHP_AUTH_PW' => 'Visitor',
            ]
        ]);

        $post_data = ['token'=> 'testtoken'];

        $this->get('/firebase/get.json');
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->post('/firebase/add.json', $post_data);
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->get('/firebase/get.json');
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->post('/firebase/delete.json',$post_data);
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->get('/firebase/get.json');
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');
    }

    /**
     * Test Add method
     * Key Assertions: Requester is given no Information about the Database or the Users inside it while informing about the 
     * problem of not beeing Authenticated
     * For further details check the documentation
     * @return void
     */
    public function testAddAuthorizedMultiple()
    {
        $this->configRequest([
            'environment' => [
                'PHP_AUTH_USER' => 'Visitor',
                'PHP_AUTH_PW' => 'Visitor',
            ]
        ]);

        $post_data = ['token'=> 'testtoken'];

        $this->post('/firebase/add.json', $post_data);
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->post('/firebase/add.json', $post_data);
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->post('/firebase/add.json', $post_data);
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $this->post('/firebase/add.json', $post_data);
        debug($this->_response->body());
        $this->assertResponseOk();
        $this->assertResponseCode(200);
        $this->assertResponseNotEmpty();

        $this->assertContentType('application/json');

        $firebase = TableRegistry::get('firebase');
        debug($firebase->find()->all());
    }

    


    
}
