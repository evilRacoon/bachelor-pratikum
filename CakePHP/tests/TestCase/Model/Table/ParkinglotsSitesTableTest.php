<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ParkinglotsSitesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParkinglotsSitesTable Test Case
 */
class ParkinglotsSitesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ParkinglotsSitesTable
     */
    public $ParkinglotsSites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.parkinglots_sites'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ParkinglotsSites') ? [] : ['className' => ParkinglotsSitesTable::class];
        $this->ParkinglotsSites = TableRegistry::get('ParkinglotsSites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ParkinglotsSites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
