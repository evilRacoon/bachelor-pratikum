<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SensorlogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SensorlogsTable Test Case
 */
class SensorlogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SensorlogsTable
     */
    public $Sensorlogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sensorlogs',
        'app.parkinglots',
        'app.gateways',
        'app.sites',
        'app.issued_comands',
        'app.remote_keys',
        'app.reservations',
        'app.users',
        'app.parkinglots_sites',
        'app.zones',
        'app.parkinglots_zones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sensorlogs') ? [] : ['className' => SensorlogsTable::class];
        $this->Sensorlogs = TableRegistry::get('Sensorlogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sensorlogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
