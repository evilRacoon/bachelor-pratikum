<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuthAclTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuthAclTable Test Case
 */
class AuthAclTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuthAclTable
     */
    public $AuthAcl;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.auth_acl',
        'app.roles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AuthAcl') ? [] : ['className' => AuthAclTable::class];
        $this->AuthAcl = TableRegistry::get('AuthAcl', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AuthAcl);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
