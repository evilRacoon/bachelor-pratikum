<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TimestampsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TimestampsTable Test Case
 */
class TimestampsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TimestampsTable
     */
    public $Timestamps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.timestamps'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Timestamps') ? [] : ['className' => TimestampsTable::class];
        $this->Timestamps = TableRegistry::get('Timestamps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Timestamps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
