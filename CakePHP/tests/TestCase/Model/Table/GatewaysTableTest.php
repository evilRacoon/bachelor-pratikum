<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GatewaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GatewaysTable Test Case
 */
class GatewaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GatewaysTable
     */
    public $Gateways;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gateways',
        'app.sites',
        'app.parkinglots',
        'app.remote_keys',
        'app.reservations',
        'app.users',
        'app.database_logs',
        'app.tenants',
        'app.tenants_users',
        'app.roles',
        'app.auth_acl',
        'app.users_roles',
        'app.zones',
        'app.users_zones',
        'app.zones_sites',
        'app.sensorlogs',
        'app.parkinglots_sites',
        'app.issued_comands'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Gateways') ? [] : ['className' => GatewaysTable::class];
        $this->Gateways = TableRegistry::get('Gateways', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gateways);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
