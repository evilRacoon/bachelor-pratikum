-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2017 at 01:07 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_acl`
--

CREATE TABLE `auth_acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `actions` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '*',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fqdn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `label`, `fqdn`, `site_id`) VALUES
(1, 'RTST', 'rtst.gateway.smartparking.hornjak.de', 1),
(2, 'MST', 'mst.gateway.smartparking.hornjak.de', 4);

-- --------------------------------------------------------

--
-- Table structure for table `issuedComands`
--

CREATE TABLE `issuedComands` (
  `id` int(11) NOT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `cmd` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `destNode` int(11) DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots`
--

CREATE TABLE `parkinglots` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mqttchannel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `status` enum('blocked','free','blocking','freeing','occupied','error') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'error',
  `lat1` double NOT NULL,
  `lon1` double NOT NULL,
  `lat2` double NOT NULL,
  `lon2` double NOT NULL,
  `lat3` double NOT NULL,
  `lon3` double NOT NULL,
  `lat4` double NOT NULL,
  `lon4` double NOT NULL,
  `ip` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `hwid` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `public` bit(1) DEFAULT b'0',
  `lastStateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `site_id`, `gateway_id`, `status`, `lat1`, `lon1`, `lat2`, `lon2`, `lat3`, `lon3`, `lat4`, `lon4`, `ip`, `hwid`, `public`, `lastStateChange`) VALUES
(1, 13, 'S3|19 P7', 'parking/13', 1, 1, 'blocked', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, '192.168.10.51', '00-aa-bb-cc-de-02', b'0', '2017-04-20 12:44:34'),
(7, 99, 'devTest', 'parking/99', 4, 2, 'blocked', 0, 0, 0, 0, 0, 0, 0, 0, '192.168.18.68', '00-aa-bb-cc-de-04', b'0', '2017-03-08 11:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots_zones`
--

CREATE TABLE `parkinglots_zones` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `remoteKeys`
--

CREATE TABLE `remoteKeys` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_until` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservationrequests`
--

CREATE TABLE `reservationrequests` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('m','f') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'notAccepted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `visitedPersonName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parkinglot_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'guest'),
(3, 'visitor');

-- --------------------------------------------------------

--
-- Table structure for table `sensorlogs`
--

CREATE TABLE `sensorlogs` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `z` int(11) DEFAULT NULL,
  `zThreshold` int(11) DEFAULT NULL,
  `state` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplyVoltage` float DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `RSSI_gwRX` int(11) DEFAULT NULL,
  `RSSI_nodeRX_avg` int(11) DEFAULT NULL,
  `numRetries_NodeToGW` int(11) DEFAULT NULL,
  `numRxTimeouts` int(11) DEFAULT NULL,
  `errorCode` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagepath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coordinates` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `name`, `imagepath`, `coordinates`, `prefix`, `lat`, `lon`) VALUES
(1, 'KOM / Dez. V', '', '', 'kom_', 49.8743061, 8.6599848),
(2, 'Lichtwiese', NULL, NULL, NULL, 49.861583, 8.679464),
(3, 'Lab', NULL, NULL, NULL, 49.874238, 8.660568),
(4, 'MST', NULL, NULL, NULL, 49.875911, 8.61749);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD');

-- --------------------------------------------------------

--
-- Table structure for table `tenants_users`
--

CREATE TABLE `tenants_users` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`) VALUES
(3, 'smart', '$2y$10$b2lie4vTGXfZDxm/jsD27uQOXdIZqe2eBLStIk4KV8CXMt9.4iLUG', '2015-07-02 22:23:43', '2017-01-10 12:15:27'),
(4, 'admin', '912ec803b2ce49e4a541068d495ab570', '2016-09-14 00:00:00', '2017-05-15 22:01:50'),
(5, 'CustomerTest', '$2y$10$7xYO502gP3wfvryoVdtjw.P7mOpSTKkxe5cPk6yMj3tmCu99nes6e', '2016-12-22 15:05:16', '2016-12-22 15:05:16'),
(6, 'mah', '$2y$10$ZZKdcIGBpALHMQiYh6C8d.0YzNDuOQON7KZvPWm2hZdEKsYr5sff2', '2017-01-09 23:22:29', '2017-01-09 23:22:29'),
(7, 'bsafa', '$2y$10$2szzlPIj9l5KwRE9eY6zKedwi9eNNxEHjga7N.qQNu6ojR9Upgi66', '2017-03-30 15:45:57', '2017-03-30 15:45:57'),
(8, 'arehman', '$2y$10$3lwxmXUZ4XjZ450UYMcqEeg8z6nqGM6wmSobvzPEYgU1P94P6Hs6W', '2017-04-20 15:22:44', '2017-04-20 15:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 3, 1),
(2, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_zones`
--

CREATE TABLE `users_zones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`) VALUES
(1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `gender` enum('m','f') COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `gender`, `title`, `name`, `mail`, `mobile`) VALUES
(1, 'm', NULL, 'Mathias', 'mathias@traincloak.de', ''),
(3, 'm', NULL, 'Mathias', 'ttn@traincloak.de', ''),
(4, 'm', NULL, 'Mathias', 'mathias@traincloak.de', ''),
(5, 'm', NULL, 'Mathias', 'mathias@traincloak.de', '');

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`, `site_id`) VALUES
(1, 'kom', 'KOM Parking Zone P5-P7', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `site_id_idx` (`site_id`);

--
-- Indexes for table `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destNode` (`destNode`);

--
-- Indexes for table `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pid` (`pid`),
  ADD KEY `pid_2` (`pid`),
  ADD KEY `pid_3` (`pid`),
  ADD KEY `gateway_id_fk_idx` (`gateway_id`),
  ADD KEY `site_id_idx` (`site_id`);

--
-- Indexes for table `parkinglots_zones`
--
ALTER TABLE `parkinglots_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remoteKeys`
--
ALTER TABLE `remoteKeys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkinglotId` (`parkinglot_id`);

--
-- Indexes for table `reservationrequests`
--
ALTER TABLE `reservationrequests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visitor_id` (`visitor_id`),
  ADD KEY `parkingLot_id` (`parkinglot_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkinglot_id_fk_idx` (`parkinglot_id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_zones`
--
ALTER TABLE `users_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_id_fk_idx` (`site_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_acl`
--
ALTER TABLE `auth_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `issuedComands`
--
ALTER TABLE `issuedComands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parkinglots`
--
ALTER TABLE `parkinglots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `parkinglots_zones`
--
ALTER TABLE `parkinglots_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `remoteKeys`
--
ALTER TABLE `remoteKeys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reservationrequests`
--
ALTER TABLE `reservationrequests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tenants_users`
--
ALTER TABLE `tenants_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_zones`
--
ALTER TABLE `users_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gateways`
--
ALTER TABLE `gateways`
  ADD CONSTRAINT `site_id` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `remoteKeys`
--
ALTER TABLE `remoteKeys`
  ADD CONSTRAINT `parkinglot_id` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD CONSTRAINT `parkinglot_id_fk` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zones`
--
ALTER TABLE `zones`
  ADD CONSTRAINT `site_id_fk` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
