-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 21. Sep 2017 um 10:33
-- Server Version: 5.5.57-0+deb8u1
-- PHP-Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `smartparking`
--

--
-- TRUNCATE Tabelle vor dem Einfügen `apps`
--

TRUNCATE TABLE `apps`;
--
-- TRUNCATE Tabelle vor dem Einfügen `auth_acl`
--

TRUNCATE TABLE `auth_acl`;
--
-- Daten für Tabelle `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1),
(6, 'AuthAcl', '*', 1),
(7, 'Maps', '*', 1),
(8, 'Roles', '*', 1),
(9, 'Users', '*', 4),
(10, 'DatabaseLogs', '*', 1),
(13, 'Reservations', '*', 4),
(14, 'Dashboard', '*', 4),
(15, 'ParkingLots', '*', 1),
(16, 'SensorLogs', '*', 1),
(17, 'ParkingLots', 'view', 4);

--
-- TRUNCATE Tabelle vor dem Einfügen `calender`
--

TRUNCATE TABLE `calender`;
--
-- TRUNCATE Tabelle vor dem Einfügen `calender_exceptions`
--

TRUNCATE TABLE `calender_exceptions`;
--
-- TRUNCATE Tabelle vor dem Einfügen `database_logs`
--

TRUNCATE TABLE `database_logs`;
--
-- Daten für Tabelle `database_logs`
--

INSERT INTO `database_logs` (`id`, `type`, `message`, `context`, `created`, `ip`, `hostname`, `uri`, `refer`, `user_agent`, `count`, `tenant`, `action`, `userName`, `userRole`, `oldValue`, `newValue`, `user_id`) VALUES
(23, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewCustomer , Reservation Manager\n    [newValue] => BrandNewCustomer , Guest\n)', '2017-08-26 22:14:27', '::1', 'localhost', '/users/edit/5', 'https://localhost/users/edit/5', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewCustomer , Reservation Manager', 'BrandNewCustomer , Guest', 3),
(24, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhkdi , Visitor\n)', '2017-08-26 22:15:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nhkdi , Visitor', 3),
(25, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhkdi , Visitor\n    [newValue] => NewFacility , Facility Manager\n)', '2017-08-26 22:16:49', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhkdi , Visitor', 'NewFacility , Facility Manager', 3),
(26, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewFacility , Facility Manager\n    [newValue] => ndhaknd , Reservation Manager\n)', '2017-08-26 22:18:39', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewFacility , Facility Manager', 'ndhaknd , Reservation Manager', 3),
(27, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ksmjs , Visitor\n)', '2017-08-26 22:54:59', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'ksmjs , Visitor', 3),
(28, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => dacaa , Reservation Manager\n)', '2017-08-26 22:56:13', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'dacaa , Reservation Manager', 3),
(29, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => mah , \n)', '2017-08-26 23:03:05', '::1', 'localhost', '/users/delete/6', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'mah , ', 'NULL', 3),
(30, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Fac , Facility Manager\n)', '2017-08-26 23:04:05', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'Fac , Facility Manager', 3),
(31, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => Fac , Facility Manager\n)', '2017-08-26 23:04:11', '::1', 'localhost', '/users/delete/36', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'Fac , Facility Manager', 'NULL', 3),
(32, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Reservation , Reservation Manager\n)', '2017-08-28 14:14:01', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(33, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Admin\n)', '2017-08-28 14:15:22', '::1', 'localhost', '/users/delete/38', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'guest , Admin', 'NULL', 3),
(34, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Reservation , Reservation Manager\n    [newValue] => Visitor , Visitor\n)', '2017-08-28 14:16:26', '::1', 'localhost', '/users/edit/39', 'https://localhost/users/edit/39', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'Reservation , Reservation Manager', 'Visitor , Visitor', 3),
(35, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 14:17:31', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'Facility , Facility Manager', 3),
(36, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => vis , Visitor\n)', '2017-08-28 17:55:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'vis , Visitor', 3),
(37, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis , Visitor\n    [newValue] => facilit , Facility Manager\n)', '2017-08-28 17:56:32', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'vis , Visitor', 'facilit , Facility Manager', 3),
(38, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => facilit , Facility Manager\n)', '2017-08-28 17:57:03', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'facilit , Facility Manager', 'NULL', 3),
(39, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Facility , Facility Manager\n    [newValue] => FacilityAdmin , Admin\n)', '2017-08-29 00:42:52', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(40, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => FacilityAdmin , Admin\n    [newValue] => Facility , Facility Manager\n)', '2017-08-29 00:43:59', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'FacilityAdmin , Admin', 'Facility , Facility Manager', 3),
(41, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-08-29 14:35:27', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'res , Reservation Manager', 3),
(42, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => visme , Visitor\n)', '2017-08-29 14:41:43', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'res', 'Reservation Manager', 'NULL', 'visme , Visitor', 41),
(43, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVisi , Visitor\n)', '2017-08-30 20:00:42', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(44, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVisi , Visitor\n)', '2017-08-30 20:02:08', '::1', 'localhost', '/users/delete/43', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newVisi , Visitor', 'NULL', 3),
(45, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => visme , Visitor\n    [newValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 20:02:36', '::1', 'localhost', '/users/edit/42', 'https://localhost/users/edit/42', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'visme , Visitor', 'vismejdhd , Reservation Manager', 3),
(46, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => gu , Guest\n)', '2017-08-30 20:03:04', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'gu , Guest', 3),
(47, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => gu , Guest\n)', '2017-08-30 20:31:30', '::1', 'localhost', '/users/delete/44', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'gu , Guest', 'NULL', 3),
(48, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 20:32:18', '::1', 'localhost', '/users/delete/42', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'vismejdhd , Reservation Manager', 'NULL', 3),
(49, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Reservation Manager\n    [newValue] => resd , Guest\n)', '2017-08-30 20:55:05', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'res , Reservation Manager', 'resd , Guest', 3),
(50, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => resd , Guest\n)', '2017-08-30 21:01:21', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'resd , Guest', 'NULL', 3),
(51, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nfh , Reservation Manager\n)', '2017-08-30 21:05:26', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nfh , Reservation Manager', 3),
(52, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhdkan , Facility Manager\n)', '2017-08-30 21:13:26', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nhdkan , Facility Manager', 3),
(53, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nfh , Reservation Manager\n    [newValue] => nfhddada , Guest\n)', '2017-08-30 21:13:50', '::1', 'localhost', '/users/edit/45', 'https://localhost/users/edit/45', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nfh , Reservation Manager', 'nfhddada , Guest', 3),
(54, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => nfhddada , Guest\n)', '2017-08-30 21:14:03', '::1', 'localhost', '/users/delete/45', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'nfhddada , Guest', 'NULL', 3),
(55, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhdkan , Facility Manager\n    [newValue] => nhd , Admin\n)', '2017-08-31 13:12:29', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhdkan , Facility Manager', 'nhd , Admin', 3),
(56, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhd , Admin\n    [newValue] => noone , Visitor\n)', '2017-09-05 13:06:13', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(57, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ris , Reservation Manager\n)', '2017-09-05 13:13:02', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(58, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => riservations , Facility Manager\n    [newValue] => riservations , Facility Manager\n)', '2017-09-05 14:13:17', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(59, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => riservations , Facility Manager\n    [newValue] => guest , Guest\n)', '2017-09-05 14:32:19', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'riservations , Facility Manager', 'guest , Guest', 3),
(60, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Guest\n    [newValue] => newVisistor , Visitor\n)', '2017-09-05 14:48:43', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'guest , Guest', 'newVisistor , Visitor', 3),
(61, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => noone , Visitor\n    [newValue] => newGuestore , Guest\n)', '2017-09-05 15:54:11', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'noone , Visitor', 'newGuestore , Guest', 3),
(62, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => kjdu , Reservation Manager\n)', '2017-09-06 13:45:06', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'kjdu , Reservation Manager', 3),
(63, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => kjdu , Reservation Manager\n    [newValue] => kikjd , Visitor\n)', '2017-09-06 13:45:24', '::1', 'localhost', '/users/edit/48', 'https://localhost/users/edit/48', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'kjdu , Reservation Manager', 'kikjd , Visitor', 3),
(64, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => kikjd , Visitor\n)', '2017-09-06 13:45:34', '::1', 'localhost', '/users/delete/48', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'kikjd , Visitor', 'NULL', 3),
(65, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVksn , Visitor\n)', '2017-09-06 14:00:38', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'newVksn , Visitor', 3),
(66, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVksn , Visitor\n    [newValue] => newGuestlooo , Guest\n)', '2017-09-06 14:00:57', '::1', 'localhost', '/users/edit/49', 'https://localhost/users/edit/49', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'newVksn , Visitor', 'newGuestlooo , Guest', 3),
(67, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newGuestlooo , Guest\n)', '2017-09-06 14:01:08', '::1', 'localhost', '/users/delete/49', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newGuestlooo , Guest', 'NULL', 3),
(68, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-09-09 02:16:30', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'res , Reservation Manager', 3),
(69, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Visitor , Visitor\n    [newValue] => Vis , Reservation Manager\n)', '2017-09-14 19:51:05', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/39', 'https://www.smartparking.tu-darmstadt.de/users/edit/39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'res', 'Reservation Manager', 'Visitor , Visitor', 'Vis , Reservation Manager', 50),
(70, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => smart , Admin\n    [newValue] => sproot , Admin\n)', '2017-09-15 11:11:09', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/3', 'https://www.smartparking.tu-darmstadt.de/users/edit/3', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'smart , Admin', 'sproot , Admin', 3),
(71, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => admin , \n)', '2017-09-15 11:12:34', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/delete/4', 'https://www.smartparking.tu-darmstadt.de/users', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'admin , ', NULL, 3),
(72, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Vis , Reservation Manager\n    [newValue] => Vis , Admin\n)', '2017-09-15 11:14:03', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/39', 'https://www.smartparking.tu-darmstadt.de/users/edit/39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'Vis , Reservation Manager', 'Vis , Admin', 3),
(73, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Facility , Facility Manager\n    [newValue] => Facility , Admin\n)', '2017-09-15 11:19:18', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/40', 'https://www.smartparking.tu-darmstadt.de/users/edit/40', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'Facility , Facility Manager', 'Facility , Admin', 3),
(74, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Reservation Manager\n    [newValue] => res , Admin\n)', '2017-09-15 11:19:36', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/50', 'https://www.smartparking.tu-darmstadt.de/users/edit/50', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'res , Reservation Manager', 'res , Admin', 3),
(75, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Vis , Admin\n    [newValue] => Vis , Admin\n)', '2017-09-15 12:48:13', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/39', 'https://www.smartparking.tu-darmstadt.de/users/edit/39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'Vis , Admin', 'Vis , Admin', 3),
(76, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Admin\n    [newValue] => res , Admin\n)', '2017-09-15 12:48:56', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/50', 'https://www.smartparking.tu-darmstadt.de/users/edit/50', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'res , Admin', 'res , Admin', 3),
(77, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Admin\n    [newValue] => res , Admin\n)', '2017-09-15 12:51:22', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/50', 'https://www.smartparking.tu-darmstadt.de/users/edit/50', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'res , Admin', 'res , Admin', 3),
(78, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => gfdgfdg , Reservation Manager\n)', '2017-09-15 18:39:28', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'sproot', 'Admin', NULL, 'gfdgfdg , Reservation Manager', 3),
(79, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Admin , CANCELED\n    [newValue] => res , Reservation Manager , ACTIVE\n)', '2017-09-15 18:45:08', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/50', 'https://www.smartparking.tu-darmstadt.de/users/edit/50', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'res , Admin , CANCELED', 'res , Reservation Manager , ACTIVE', 3),
(80, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => vis2 , Visitor\n)', '2017-09-15 18:50:41', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'res', 'Reservation Manager', NULL, 'vis2 , Visitor', 50),
(81, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis2 , Visitor , \n    [newValue] => vis2 , Guest , ACTIVE\n)', '2017-09-15 18:52:52', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/52', 'https://www.smartparking.tu-darmstadt.de/users/edit/52', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'res', 'Reservation Manager', 'vis2 , Visitor , ', 'vis2 , Guest , ACTIVE', 50),
(82, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis2 , Guest , ACTIVE\n    [newValue] => vis2 , Guest , ACTIVE\n)', '2017-09-15 18:55:09', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/52', 'https://www.smartparking.tu-darmstadt.de/users/edit/52', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'res', 'Reservation Manager', 'vis2 , Guest , ACTIVE', 'vis2 , Guest , ACTIVE', 50),
(83, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => tu_kom , Reservation Manager\n)', '2017-09-17 19:31:48', '87.157.195.206', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1', 1, 'TU Darmstadt', 'CREATE', 'sproot', 'Admin', NULL, 'tu_kom , Reservation Manager', 3),
(84, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => tu_dezv , Reservation Manager\n)', '2017-09-19 14:24:20', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'sproot', 'Admin', NULL, 'tu_dezv , Reservation Manager', 3),
(85, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Vis , Admin , BLOCKED\n    [newValue] => Vis , Reservation Manager , DELETED\n)', '2017-09-19 14:30:11', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/39', 'https://www.smartparking.tu-darmstadt.de/users/edit/39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'Vis , Admin , BLOCKED', 'Vis , Reservation Manager , DELETED', 3),
(86, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Facility , Admin , BLOCKED\n    [newValue] => Facility , Facility Manager , DELETED\n)', '2017-09-19 14:30:34', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/40', 'https://www.smartparking.tu-darmstadt.de/users/edit/40', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'Facility , Admin , BLOCKED', 'Facility , Facility Manager , DELETED', 3),
(87, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Reservation Manager , ACTIVE\n    [newValue] => res , Reservation Manager , DELETED\n)', '2017-09-19 14:30:55', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/50', 'https://www.smartparking.tu-darmstadt.de/users/edit/50', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'res , Reservation Manager , ACTIVE', 'res , Reservation Manager , DELETED', 3),
(88, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => gfdgfdg , Reservation Manager , \n    [newValue] => gfdgfdg , Reservation Manager , DELETED\n)', '2017-09-19 14:31:08', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/51', 'https://www.smartparking.tu-darmstadt.de/users/edit/51', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'gfdgfdg , Reservation Manager , ', 'gfdgfdg , Reservation Manager , DELETED', 3),
(89, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis2 , Guest , ACTIVE\n    [newValue] => vis2 , Guest , DELETED\n)', '2017-09-19 14:31:56', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/52', 'https://www.smartparking.tu-darmstadt.de/users/edit/52', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'vis2 , Guest , ACTIVE', 'vis2 , Guest , DELETED', 3),
(90, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => tu_kom , Reservation Manager , \n    [newValue] => tu_kom , Reservation Manager , ACTIVE\n)', '2017-09-19 14:32:12', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/53', 'https://www.smartparking.tu-darmstadt.de/users/edit/53', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'tu_kom , Reservation Manager , ', 'tu_kom , Reservation Manager , ACTIVE', 3),
(91, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => tu_dezv , Reservation Manager , \n    [newValue] => tu_dezv , Reservation Manager , ACTIVE\n)', '2017-09-19 14:32:21', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/54', 'https://www.smartparking.tu-darmstadt.de/users/edit/54', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'tu_dezv , Reservation Manager , ', 'tu_dezv , Reservation Manager , ACTIVE', 3),
(92, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => sproot , Admin , \n    [newValue] => sproot , Admin , ACTIVE\n)', '2017-09-19 14:32:29', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/3', 'https://www.smartparking.tu-darmstadt.de/users/edit/3', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'sproot , Admin , ', 'sproot , Admin , ACTIVE', 3),
(93, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => tu_kom , Reservation Manager , ACTIVE\n    [newValue] => tu_kom , Reservation Manager , ACTIVE\n)', '2017-09-19 14:33:20', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/53', 'https://www.smartparking.tu-darmstadt.de/users/edit/53', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'tu_kom , Reservation Manager , ACTIVE', 'tu_kom , Reservation Manager , ACTIVE', 3),
(94, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => tu_sp , Reservation Manager\n)', '2017-09-19 14:36:23', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'sproot', 'Admin', NULL, 'tu_sp , Reservation Manager', 3),
(95, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Gisela Scholz-Schmidt , Reservation Manager\n)', '2017-09-19 15:25:21', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'tu_kom', 'Reservation Manager', NULL, 'Gisela Scholz-Schmidt , Reservation Manager', 53),
(96, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Gisela Scholz-Schmidt , Reservation Manager , \n    [newValue] => Gisela Scholz-Schmidt , Guest , ACTIVE\n)', '2017-09-19 15:25:31', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/56', 'https://www.smartparking.tu-darmstadt.de/users/edit/56', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'tu_kom', 'Reservation Manager', 'Gisela Scholz-Schmidt , Reservation Manager ,', 'Gisela Scholz-Schmidt , Guest , ACTIVE', 53),
(97, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Gisela Scholz-Schmidt , Guest , ACTIVE\n    [newValue] => Gisela Scholz-Schmidt , Reservation Manager , ACTIVE\n)', '2017-09-19 15:25:45', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/56', 'https://www.smartparking.tu-darmstadt.de/users/edit/56', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'tu_kom', 'Reservation Manager', 'Gisela Scholz-Schmidt , Guest , ACTIVE', 'Gisela Scholz-Schmidt , Reservation Manager ,', 53),
(98, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => tu_sp , Reservation Manager , \n    [newValue] => tu_sp , Reservation Manager , ACTIVE\n)', '2017-09-19 15:40:17', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/55', 'https://www.smartparking.tu-darmstadt.de/users/edit/55', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'Gisela Scholz-Schmidt', 'Reservation Manager', 'tu_sp , Reservation Manager , ', 'tu_sp , Reservation Manager , ACTIVE', 56),
(99, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Michael Schaab , Guest\n)', '2017-09-19 15:42:31', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'tu_kom', 'Reservation Manager', NULL, 'Michael Schaab , Guest', 53),
(100, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Michael Schaab , Guest , \n    [newValue] => Michael Schaab , Guest , ACTIVE\n)', '2017-09-19 15:42:39', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/57', 'https://www.smartparking.tu-darmstadt.de/users/edit/57', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'tu_kom', 'Reservation Manager', 'Michael Schaab , Guest , ', 'Michael Schaab , Guest , ACTIVE', 53),
(101, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => arroot , Admin\n)', '2017-09-19 16:15:10', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/add', 'https://www.smartparking.tu-darmstadt.de/users/add', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'sproot', 'Admin', NULL, 'arroot , Admin', 3),
(102, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => arroot , Admin , \n    [newValue] => arroot , Admin , ACTIVE\n)', '2017-09-19 16:15:26', '130.83.139.74', 'www.smartparking.tu-darmstadt.de', '/users/edit/58', 'https://www.smartparking.tu-darmstadt.de/users/edit/58', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'sproot', 'Admin', 'arroot , Admin , ', 'arroot , Admin , ACTIVE', 3);

--
-- TRUNCATE Tabelle vor dem Einfügen `gateways`
--

TRUNCATE TABLE `gateways`;
--
-- Daten für Tabelle `gateways`
--

INSERT INTO `gateways` (`id`, `label`, `fqdn`, `site_id`) VALUES
(1, 'RTST', 'rtst.gateway.smartparking.hornjak.de', 1),
(2, 'MST', 'mst.gateway.smartparking.hornjak.de', 4),
(3, 'LIWI', 'liwi.gateway.smartparking.hornjak.de', 1);

--
-- TRUNCATE Tabelle vor dem Einfügen `issuedComands`
--

TRUNCATE TABLE `issuedComands`;
--
-- TRUNCATE Tabelle vor dem Einfügen `locations`
--

TRUNCATE TABLE `locations`;
--
-- Daten für Tabelle `locations`
--

INSERT INTO `locations` (`id`, `description`, `type`, `upperLeftLongitude`, `upperLeftLatitude`, `upperRightLongitude`, `upperRightLatitude`, `lowerLeftLongitude`, `lowerLeftLatitude`, `lowerRigthLongitude`, `lowerRigthLatitude`, `width`, `length`, `imagePath`) VALUES
(1, 'parking/13', 'SQARE', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, NULL, NULL, 'NULL'),
(2, 'parking/99', 'CIRCLE', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'NULL');

--
-- TRUNCATE Tabelle vor dem Einfügen `parkinglots`
--

TRUNCATE TABLE `parkinglots`;
--
-- Daten für Tabelle `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `gateway_id`, `status`, `ip`, `hwid`, `lastStateChange`, `type`, `locationId`) VALUES
(1, 13, 'Parkplatz #7', 'parking/13', 1, 'blocked', '192.168.10.51', '00-aa-bb-cc-de-02', '2017-09-19 11:44:16', 'SINGLE', 1),
(7, 99, 'devTest', 'parking/99', 2, 'blocked', '192.168.18.68', '00-aa-bb-cc-de-04', '2017-09-19 12:03:29', 'SINGLE', 2),
(8, 14, 'Parkplatz #6', 'parking/14', 1, 'blocked', '192.168.10.52', '00-aa-bb-cc-de-04', '2017-09-19 11:44:16', 'SINGLE', 1),
(9, 15, 'Parkplatz #5', 'parking/15', 1, 'blocked', '192.168.10.53', '00:aa:bb:cc:de:06', '2017-09-19 11:44:16', 'SINGLE', 1),
(10, 16, 'Parkplatz #4', 'parking/16', 1, 'blocked', '192.168.10.53', '00-aa-bb-cc-de-07', '2017-09-19 11:44:16', 'SINGLE', 1),
(11, 21, 'Parkplatz #1', 'parking/21', 3, 'blocked', '192.168.11.51', '00-aa-bb-cc-dc-01', '2017-09-19 12:10:23', 'SINGLE', 1),
(12, 22, 'Parkplatz #2', 'parking/22', 3, 'blocked', '192.168.11.52', '00-aa-bb-cc-dc-02', '2017-09-19 12:10:23', 'SINGLE', 1);

--
-- TRUNCATE Tabelle vor dem Einfügen `parkinglots_sites`
--

TRUNCATE TABLE `parkinglots_sites`;
--
-- Daten für Tabelle `parkinglots_sites`
--

INSERT INTO `parkinglots_sites` (`id`, `sitesId`, `parkingLotsId`) VALUES
(2, 8, 1),
(4, 3, 7),
(6, 5, 9),
(7, 1, 8),
(8, 5, 10),
(9, 6, 11),
(10, 6, 12);

--
-- TRUNCATE Tabelle vor dem Einfügen `reservations`
--

TRUNCATE TABLE `reservations`;
--
-- Daten für Tabelle `reservations`
--

INSERT INTO `reservations` (`id`, `token`, `start`, `end`, `parkinglot_id`, `users_id`, `visitedPersonId`, `licencePlate`, `reservationState`, `messageInfo`, `isRecurring`, `recurringID`, `created`) VALUES
(18, '322072516324a9e6b7026a76146ed326', '2017-09-06 17:00:00', '2017-09-06 20:00:00', 1, 3, 40, 'ikjs', '', '', '', NULL, '2017-09-06 17:01:09'),
(19, '5aa17cb032f71d3959f053447d5d9380', '2017-09-07 13:01:00', '2017-09-07 16:01:00', 1, 3, 39, 'fsadd', '', '', '', NULL, '2017-09-07 13:01:42'),
(21, '1fd3edf4bccef4845c6e7fe0ac02df29', '2017-09-11 11:22:00', '2017-09-14 14:45:00', 1, 3, 40, 'licenceplate', 'RESERVED', '', '', NULL, '2017-09-11 11:24:57'),
(22, 'f7f373054550d31e437f67cdf4bae212', '2017-09-15 18:39:00', '2017-09-15 21:39:00', 1, 3, 40, 'fgfdgfdgdgf', '', 'fdgfdgf', NULL, NULL, '2017-09-15 18:40:12'),
(23, '8d195cac78ee630c37cfee1b3a956c9a', '2017-09-15 18:53:00', '2017-09-15 21:53:00', 7, 52, 3, 'hfghdh', '', 'gfdg', NULL, NULL, '2017-09-15 18:54:06'),
(24, '0b4baa0f5a9413f7cc6ff3e8ef7861f9', '2017-09-19 15:42:00', '2017-09-19 18:42:00', 1, 53, 57, 'TU-DA 296', '', 'test Reservierung', NULL, NULL, '2017-09-19 15:43:33'),
(25, '90c4a124c3e59fa9f3abf918b9203a04', '2017-09-20 13:08:00', '2017-09-20 16:08:00', 1, 55, 53, 'fsdfsad', '', 'test Reservierung', NULL, NULL, '2017-09-20 13:08:55'),
(26, '3f0b6aab99e74a81491c3bed1f67b252', '2017-09-20 16:09:00', '2017-09-20 16:10:00', 1, 55, 3, 'gfdgf', '', 'test Reservierung', NULL, NULL, '2017-09-20 13:11:27'),
(27, 'd25544a5be93cb4e6fcd55d2eab01316', '2017-09-20 16:16:00', '2017-09-20 16:16:00', 1, 55, 3, 'khjkhk', '', 'ghgj', NULL, NULL, '2017-09-20 13:16:42'),
(28, '9006e18e4f30f0afe15264a8558c436e', '2017-09-20 16:24:00', '2017-09-20 16:24:00', 1, 55, 3, 'gsfdg', '', 'test Reservierung', NULL, NULL, '2017-09-20 13:24:32'),
(29, '509189a6b66245d4d7e49eac09cd952e', '2017-09-20 16:28:00', '2017-09-20 16:28:00', 1, 55, 3, 'fhfgh', '', 'test Reservierung', NULL, NULL, '2017-09-20 13:28:18'),
(30, 'a485e671bdd644b7e51b066ace9ab75e', '2017-09-13 17:30:00', '2017-09-20 17:30:00', 1, 55, 3, 'llhj', '', 'test Reservierung', NULL, NULL, '2017-09-20 14:30:51');

--
-- TRUNCATE Tabelle vor dem Einfügen `reservations_users`
--

TRUNCATE TABLE `reservations_users`;
--
-- TRUNCATE Tabelle vor dem Einfügen `roles`
--

TRUNCATE TABLE `roles`;
--
-- Daten für Tabelle `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Host'),
(3, 'Visitor'),
(4, 'Reservation Manager'),
(5, 'Facility Manager');

--
-- TRUNCATE Tabelle vor dem Einfügen `sensorlogs`
--

TRUNCATE TABLE `sensorlogs`;
--
-- TRUNCATE Tabelle vor dem Einfügen `sites`
--

TRUNCATE TABLE `sites`;
--
-- Daten für Tabelle `sites`
--

INSERT INTO `sites` (`id`, `name`, `description`, `prefix`, `locationID`, `isPublic`, `publicStart`, `publicEnd`, `isPublicRecurring`, `recurringId`) VALUES
(1, 'KOM / Rundeturmstrasse 12', 'Rundeturmstrasse 12, P6', 'kom_', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(2, 'FB Maschinenbau / Lichtwiese', 'Parkplätze P1-P10, Fr. Fornoff/Hr. Hofferer', 'NULL', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(3, 'Test Lab', 'für Testzwecke Entwickler', 'NULL', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(4, 'MST', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(5, 'Dez V / Rundeturmstrasse 12', 'Rundeturmstrasse 12, S3|19, P4/5', 'kom_', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(6, 'Dez IV / Lichtwiese', 'Parkplätze P11 -30 vor L1|01, Fr. Keil', 'NULL', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(7, 'Messestand ParkHere', 'Demo auf EVS30', 'NULL', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(8, 'SmartParking / Rundeturmstrasse 12', 'Rundeturmstrasse 12, P7', 'kom_', 1, 'NO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL);

--
-- TRUNCATE Tabelle vor dem Einfügen `tenants`
--

TRUNCATE TABLE `tenants`;
--
-- Daten für Tabelle `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD'),
(2, 'Dev Test', 'Test'),
(3, 'EVS - Messe Stuttgart', 'EVS30');

--
-- TRUNCATE Tabelle vor dem Einfügen `tenants_users`
--

TRUNCATE TABLE `tenants_users`;
--
-- Daten für Tabelle `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3),
(2, 2, 50),
(3, 1, 53),
(4, 1, 54),
(5, 1, 55),
(6, 1, 56);

--
-- TRUNCATE Tabelle vor dem Einfügen `timestamps`
--

TRUNCATE TABLE `timestamps`;
--
-- TRUNCATE Tabelle vor dem Einfügen `users`
--

TRUNCATE TABLE `users`;
--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`, `email`, `mobileNumber`, `landlineNumber`, `firstname`, `lastname`, `title`, `gender`, `userState`, `company`, `department`, `street`, `ZIP`) VALUES
(3, 'sproot', '$2y$10$BjBQbHQ48ct3qj0lAVDL5.slxtiM7zgR/RSUf2v3PgiOrd4qfwlBK', '2015-07-02 22:23:43', '2017-09-19 14:32:29', 'torsten.uhlig@kom.tu-darmstadt.de', '', '+4961511620897', 'Torsten', 'Uhlig', '', 'male', 'ACTIVE', 'TU Darmstadt, FB18, KOM', 'KOM', 'Rundeturmstrasse 12', '64283'),
(39, 'Vis', '$2y$10$vJDXCaEG7serNOGo4BMqKukx4CrhBn/7U4GP7gkd3UqpI4OHWXHDK', '2017-08-28 12:14:00', '2017-09-19 14:30:11', 're@r.com', '', '', 'rese', 'mand', '', '', 'DELETED', '', '', '', ''),
(40, 'Facility', '$2y$10$TBrw62X624X75.J3RPogg.c2eDw8HWehHvaunBZewL/1hejxB.poS', '2017-08-28 12:17:31', '2017-09-19 14:30:34', 'fa@e.com', '', '', 'Facility', 'Manager', '', '', 'DELETED', '', '', '', ''),
(50, 'res', '$2y$10$IFi9KS1zVzRA7VtKRs/pMOWrOfDg2vZqXvx19VejD0CSS0I3h3q0a', '2017-09-09 00:16:30', '2017-09-19 14:30:55', 'res@res.com', '', '', 'res', 'manager', '', 'NULL', 'DELETED', '', '', '', ''),
(51, 'gfdgfdg', '$2y$10$kCGXEOEoDeE9ee7VqLl6X.tMb8ELjX9GM1.BGm1A2WnEozS47pSm2', '2017-09-15 18:39:28', '2017-09-19 14:31:08', 'gfdgsfdgd@dfgdgf.de', '', '', 'gfdg', 'fgfdg', '', '', 'DELETED', '', '', '', ''),
(52, 'vis2', '$2y$10$0rBQqg72/C1XTyL5OLhb3eowAodcOFtpzH5BD2I.wenYGfN87pTCu', '2017-09-15 18:50:41', '2017-09-19 14:31:56', 'torsten.uhlig@gmxold.de', '', '', 'vis2', 'vis2', '', '', 'DELETED', '', '', '', ''),
(53, 'tu_kom', '$2y$10$ceKSEnCsCHfJe3pxdAJ2HeM8qxd6ec8PQGuqxlfo4emaIXyhXYCiy', '2017-09-17 19:31:48', '2017-09-19 14:33:20', 'tuhlig@kom.tu-darmstadt.de', '0175/9379038', '-20897', 'Torsten', 'Uhlig', '', '', 'ACTIVE', 'TU Darmstadt', 'Multimedia Communications', 'Rundeturmstrasse 12', '64283'),
(54, 'tu_dezv', '$2y$10$AiNQqfIbYrdHFwLH9FgHbe7uxDyVg7ykEsJWkETzirUTEhRiXvjea', '2017-09-19 14:24:20', '2017-09-19 14:32:21', 'uhlig67@web.de', '0175/9379038', '+4961511620897', 'Torsten', 'Uhlig', '', '', 'ACTIVE', 'TU Darmstadt, FB18, KOM', 'KOM', 'Rundeturmstrasse 12', '64283'),
(55, 'tu_sp', '$2y$10$9OGIqsxPSt9l9X8tgBplPusVnGKwzaSk.exCMERP8yZDWaj.cqbbC', '2017-09-19 14:36:23', '2017-09-19 15:40:17', 'torsten.uhlig@gmx.de', '0175/9379038', '+4961511620897', 'Torsten', 'Uhlig', '', '', 'ACTIVE', 'TU Darmstadt, FB18, KOM', 'KOM', 'Rundeturmstrasse 12', '64283'),
(56, 'Gisela Scholz-Schmidt', '$2y$10$AnyMsemFYgA3uIj07O47ce4CFRzB/LE93a2W7Qmp.s7JvEV7pfYFi', '2017-09-19 15:25:21', '2017-09-19 15:25:45', 'steinmetz.office@kom.tu-darmstadt.de', '', '+49 (0)6151 16-29100', 'Gisela', 'Scholz-Schmidt', '', '', 'ACTIVE', 'TU Darmstadt, FB18, KOM', 'KOM', 'Rundeturmstrasse 10', '64283'),
(57, 'Michael Schaab', '$2y$10$omLyq/zZimJr9Gh8ODl3Duk4TIRqGNS2fh/D4rDHqZDMC9pLqFHHa', '2017-09-19 15:42:30', '2017-09-19 15:42:39', 'michael.schaab@kom.tu-darmstadt.de', '', '', 'Michael', 'Schaab', '', '', 'ACTIVE', 'TU Darmstadt', 'KOM', 'Rundeturmstrasse 12, Raum 0.9', '64283'),
(58, 'arroot', '$2y$10$1ItOe7OyQL1UGb8UN0EWwOb40A6O6rs3.mNolJDKTVe8AKpeDY/We', '2017-09-19 16:15:10', '2017-09-19 16:15:26', 'ahmedabdul.rehman@stud.tu-darmstadt.de', '', '', 'Ahmed Abdul', 'Rehman', '', '', 'ACTIVE', 'TU Darmstadt', 'KOM', 'Rundeturmstrasse 12, Raum 0.9', '64283');

--
-- TRUNCATE Tabelle vor dem Einfügen `users_roles`
--

TRUNCATE TABLE `users_roles`;
--
-- Daten für Tabelle `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 3, 1),
(23, 25, 1),
(24, 26, 1),
(25, 27, 1),
(26, 28, 1),
(27, 29, 1),
(28, 30, 1),
(29, 31, 1),
(30, 32, 4),
(74, 51, 4),
(75, 50, 4),
(77, 52, 2),
(78, 53, 4),
(79, 54, 4),
(80, 39, 4),
(81, 40, 5),
(82, 55, 4),
(85, 56, 4),
(86, 57, 2),
(87, 58, 1);

--
-- TRUNCATE Tabelle vor dem Einfügen `users_zones`
--

TRUNCATE TABLE `users_zones`;
--
-- Daten für Tabelle `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`, `keyName`, `start`, `end`) VALUES
(1, 3, 1, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 50, 2, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 53, 1, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 54, 3, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 55, 7, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 56, 1, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- TRUNCATE Tabelle vor dem Einfügen `zones`
--

TRUNCATE TABLE `zones`;
--
-- Daten für Tabelle `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`) VALUES
(1, 'Multimedia Communications', 'TU Darmstadt, Rundeturmstrasse 10, KOM'),
(2, 'Dezernat V', 'TU Darmstadt, Baumanagement und Technischer Betrieb'),
(3, 'Dezernat IV', 'TU Darmstadt, Immobilienmanagement'),
(4, 'FB Maschinenbau', 'TU Darmstadt, Fachbereich Maschinenbau, Lichtwiese'),
(5, 'Testzone', 'Zone zum Testen für Entwicklung'),
(6, 'Messehalle', 'EVS 30 in Stuttgart'),
(7, 'SmartParking', 'SmartParking Projekt Zone');

--
-- TRUNCATE Tabelle vor dem Einfügen `zones_sites`
--

TRUNCATE TABLE `zones_sites`;
--
-- Daten für Tabelle `zones_sites`
--

INSERT INTO `zones_sites` (`id`, `zonesId`, `sitesId`) VALUES
(1, 1, 1),
(2, 2, 5),
(3, 3, 6),
(4, 4, 2),
(5, 5, 3),
(6, 6, 7),
(7, 7, 8);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
