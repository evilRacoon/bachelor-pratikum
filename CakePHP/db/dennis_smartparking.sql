-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 22. Feb 2018 um 17:14
-- Server-Version: 10.1.26-MariaDB-0+deb9u1
-- PHP-Version: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `smartparking`
--
USE smartparking
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `failed_login_attempts`
--

CREATE TABLE `failed_login_attempts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------


--
-- Tabellenstruktur für Tabelle `apps`
--

CREATE TABLE `apps` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `secret` varchar(45) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `state` enum('ACTIVE','BLOCKED','DELETED') DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `auth_acl`
--

CREATE TABLE `auth_acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(200) CHARACTER SET utf8 NOT NULL,
  `actions` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '*',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1),
(6, 'AuthAcl', '*', 1),
(7, 'Maps', '*', 1),
(8, 'Roles', '*', 1),
(9, 'Users', '*', 4),
(10, 'DatabaseLogs', '*', 1),
(13, 'Reservations', '*', 4),
(14, 'Dashboard', '*', 4),
(15, 'Reservations', '*', 3),
(18, 'Users', 'index, getUser', 3),
(19, 'Zones', 'index, view', 3),
(20, 'Sites', 'index, view', 3),
(21, 'Parkinglots', 'index, view', 3),
(22, 'Parkinglots', '*', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `calender`
--

CREATE TABLE `calender` (
  `recurringId` int(11) NOT NULL,
  `recurringTypeId` int(11) NOT NULL,
  `separationCount` int(11) DEFAULT NULL,
  `maxNumOfOccurences` int(11) DEFAULT NULL,
  `dayOfWeek` int(11) DEFAULT NULL,
  `WeekOfMonth` int(11) DEFAULT NULL,
  `DayOfMonth(11)` int(11) DEFAULT NULL,
  `MonthOfYear` int(11) DEFAULT NULL COMMENT 'Reservation Reccurances '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `calender_exceptions`
--

CREATE TABLE `calender_exceptions` (
  `id` int(11) NOT NULL,
  `res_id` int(11) DEFAULT NULL,
  `isScheduled` bit(1) DEFAULT NULL,
  `isCancelled` bit(1) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `isFullDayReservation` bit(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `database_logs`
--

CREATE TABLE `database_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `message` text,
  `context` text,
  `created` timestamp NULL DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `refer` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `count` int(10) DEFAULT '0',
  `tenant` varchar(45) DEFAULT NULL,
  `action` enum('CREATE','UPDATE','DELETE','OTHER') DEFAULT 'OTHER',
  `userName` varchar(45) DEFAULT NULL,
  `userRole` varchar(45) DEFAULT NULL,
  `oldValue` varchar(45) DEFAULT NULL,
  `newValue` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `database_logs`
--

INSERT INTO `database_logs` (`id`, `type`, `message`, `context`, `created`, `ip`, `hostname`, `uri`, `refer`, `user_agent`, `count`, `tenant`, `action`, `userName`, `userRole`, `oldValue`, `newValue`, `user_id`) VALUES
(24, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhkdi , Visitor\n)', '2017-08-26 22:15:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nhkdi , Visitor', 3),
(25, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhkdi , Visitor\n    [newValue] => NewFacility , Facility Manager\n)', '2017-08-26 22:16:49', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhkdi , Visitor', 'NewFacility , Facility Manager', 3),
(35, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 14:17:31', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'Facility , Facility Manager', 3),
(40, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => FacilityAdmin , Admin\n    [newValue] => Facility , Facility Manager\n)', '2017-08-29 00:43:59', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'FacilityAdmin , Admin', 'Facility , Facility Manager', 3),
(44, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVisi , Visitor\n)', '2017-08-30 20:02:08', '::1', 'localhost', '/users/delete/43', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newVisi , Visitor', 'NULL', 3),
(50, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => resd , Guest\n)', '2017-08-30 21:01:21', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'resd , Guest', 'NULL', 3),
(52, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhdkan , Facility Manager\n)', '2017-08-30 21:13:26', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'nhdkan , Facility Manager', 3),
(54, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => nfhddada , Guest\n)', '2017-08-30 21:14:03', '::1', 'localhost', '/users/delete/45', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'nfhddada , Guest', 'NULL', 3),
(55, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhdkan , Facility Manager\n    [newValue] => nhd , Admin\n)', '2017-08-31 13:12:29', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhdkan , Facility Manager', 'nhd , Admin', 3),
(56, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhd , Admin\n    [newValue] => noone , Visitor\n)', '2017-09-05 13:06:13', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(57, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ris , Reservation Manager\n)', '2017-09-05 13:13:02', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(58, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => riservations , Facility Manager\n    [newValue] => riservations , Facility Manager\n)', '2017-09-05 14:13:17', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'NULL', 'OTHER', 'NULL', 'NULL', 'NULL', 'NULL', NULL),
(59, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => riservations , Facility Manager\n    [newValue] => guest , Guest\n)', '2017-09-05 14:32:19', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'riservations , Facility Manager', 'guest , Guest', 3),
(60, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Guest\n    [newValue] => newVisistor , Visitor\n)', '2017-09-05 14:48:43', '::1', 'localhost', '/users/edit/47', 'https://localhost/users/edit/47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'guest , Guest', 'newVisistor , Visitor', 3),
(61, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => noone , Visitor\n    [newValue] => newGuestore , Guest\n)', '2017-09-05 15:54:11', '::1', 'localhost', '/users/edit/46', 'https://localhost/users/edit/46', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'noone , Visitor', 'newGuestore , Guest', 3),
(64, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => kikjd , Visitor\n)', '2017-09-06 13:45:34', '::1', 'localhost', '/users/delete/48', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'kikjd , Visitor', 'NULL', 3),
(65, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVksn , Visitor\n)', '2017-09-06 14:00:38', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'newVksn , Visitor', 3),
(66, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVksn , Visitor\n    [newValue] => newGuestlooo , Guest\n)', '2017-09-06 14:00:57', '::1', 'localhost', '/users/edit/49', 'https://localhost/users/edit/49', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'newVksn , Visitor', 'newGuestlooo , Guest', 3),
(67, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newGuestlooo , Guest\n)', '2017-09-06 14:01:08', '::1', 'localhost', '/users/delete/49', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newGuestlooo , Guest', 'NULL', 3),
(68, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-09-09 02:16:30', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', 'NULL', 'res , Reservation Manager', 3),
(69, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Visitor\n)', '2017-12-14 13:46:36', '127.0.0.1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Visitor', 3),
(70, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ad , Admin\n)', '2017-12-14 13:57:37', '127.0.0.1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'ad , Admin', 3),
(71, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => reserv , Reservation Manager\n)', '2017-12-14 14:09:17', '127.0.0.1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'reserv , Reservation Manager', 3),
(73, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => app , \n)', '2017-12-14 14:27:30', '127.0.0.1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'app , ', 3),
(74, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => ad , Admin\n)', '2017-12-14 17:31:57', '::1', 'localhost:8765', '/users/delete/52', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'ad , Admin', NULL, 3),
(75, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Guest\n)', '2017-12-14 17:33:43', '::1', 'localhost:8765', '/users/delete/54', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'guest , Guest', NULL, 3),
(76, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Visitor\n)', '2017-12-14 17:34:38', '::1', 'localhost:8765', '/users/delete/51', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Visitor', NULL, 3),
(77, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 15:54:59', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(78, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 15:55:17', '::1', 'localhost:8765', '/users/delete/56', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(79, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 15:56:12', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(80, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 16:00:18', '::1', 'localhost:8765', '/users/delete/57', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(81, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 16:00:44', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(82, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 16:09:14', '::1', 'localhost:8765', '/users/delete/58', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(83, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 16:09:33', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(84, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 16:09:54', '::1', 'localhost:8765', '/users/delete/59', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(85, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 16:13:25', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(86, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2018-01-04 16:14:05', '::1', 'localhost:8765', '/users/edit/60', 'http://localhost:8765/users/edit/60', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Admin , ACTIVE', 'test , Admin , ACTIVE', 3),
(87, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 16:37:33', '::1', 'localhost:8765', '/users/delete/60', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(88, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 16:50:49', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(89, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2018-01-04 16:50:49', '::1', 'localhost:8765', '/users/edit/61', 'http://localhost:8765/users/edit/61', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Admin , ACTIVE', 'test , Admin , ACTIVE', 3),
(90, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 16:50:49', '::1', 'localhost:8765', '/users/delete/61', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(91, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 16:59:55', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(92, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2018-01-04 16:59:55', '::1', 'localhost:8765', '/users/edit/62', 'http://localhost:8765/users/edit/62', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Admin , ACTIVE', 'test , Admin , ACTIVE', 3),
(93, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 16:59:55', '::1', 'localhost:8765', '/users/delete/62', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(94, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 17:03:54', '::1', 'localhost:8765', '/users/add', 'http://localhost:8765/users/add', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(95, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2018-01-04 17:03:54', '::1', 'localhost:8765', '/users/edit/63', 'http://localhost:8765/users/edit/63', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Admin , ACTIVE', 'test , Admin , ACTIVE', 3),
(96, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 17:03:54', '::1', 'localhost:8765', '/users/delete/63', 'http://localhost:8765/users', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(97, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 17:21:14', '::1', 'localhost:8765', '/users/add', NULL, 'curl/7.52.1', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(98, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2018-01-04 17:21:14', '::1', 'localhost:8765', '/users/edit/64', NULL, 'curl/7.52.1', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Admin , ACTIVE', 'test , Admin , ACTIVE', 3),
(99, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 17:21:14', '::1', 'localhost:8765', '/users/delete/64', NULL, 'curl/7.52.1', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(100, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => test , Admin\n)', '2018-01-04 17:27:11', '::1', 'localhost:8765', '/users/add', NULL, 'curl/7.52.1', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'test , Admin', 3),
(101, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin , ACTIVE\n    [newValue] => test , Admin , ACTIVE\n)', '2018-01-04 17:27:11', '::1', 'localhost:8765', '/users/edit/65', NULL, 'curl/7.52.1', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'test , Admin , ACTIVE', 'test , Admin , ACTIVE', 3),
(102, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => test , Admin\n)', '2018-01-04 17:27:11', '::1', 'localhost:8765', '/users/delete/65', NULL, 'curl/7.52.1', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'test , Admin', NULL, 3),
(103, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => app ,  , ACTIVE\n    [newValue] => app , Visitor , ACTIVE\n)', '2018-01-06 16:24:10', '::1', 'localhost:8765', '/users/edit/55', 'http://localhost:8765/users/edit/55', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'app ,  , ACTIVE', 'app , Visitor , ACTIVE', 3),
(104, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => admin , Admin\n)', '2018-02-22 16:07:54', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(105, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => app , Visitor\n)', '2018-02-22 16:09:30', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(106, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => dennis , Visitor\n)', '2018-02-22 16:49:06', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(107, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => dennis , Visitor , ACTIVE\n    [newValue] => dennis , Admin , ACTIVE\n)', '2018-02-22 16:53:04', '::1', 'localhost', '/users/edit/58', 'https://localhost/users/edit/58', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 NOT NULL,
  `fqdn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table Kommentar\nlabel - Bezeichnung des Ortes\nfqdn URL für Gateway';

--
-- Daten für Tabelle `gateways`
--

INSERT INTO `gateways` (`id`, `label`, `fqdn`, `site_id`) VALUES
(1, 'RTST', 'rtst.gateway.smartparking.hornjak.de', 1),
(2, 'MST', 'mst.gateway.smartparking.hornjak.de', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `issuedComands`
--

CREATE TABLE `issuedComands` (
  `id` int(11) NOT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `cmd` varchar(10) CHARACTER SET utf8 NOT NULL,
  `destNode` int(11) DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` enum('SQARE','CIRCLE','ELLIPSE') DEFAULT NULL COMMENT 'SQUARE=four corners, CIRCLE=upperLeft-upperRight means diameter,  ELLIPSE=upperLeft-upperRight means greatest expansion, lowerLeft-lowerLeft means smallest expansion',
  `upperLeftLongitude` double DEFAULT NULL,
  `upperLeftLatitude` double DEFAULT NULL,
  `upperRightLongitude` double DEFAULT NULL,
  `upperRightLatitude` double DEFAULT NULL,
  `lowerLeftLongitude` double DEFAULT NULL,
  `lowerLeftLatitude` double DEFAULT NULL,
  `lowerRightLongitude` double DEFAULT NULL,
  `lowerRightLatitude` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL COMMENT 'pictures for location'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `locations`
--

INSERT INTO `locations` (`id`, `description`, `type`, `upperLeftLongitude`, `upperLeftLatitude`, `upperRightLongitude`, `upperRightLatitude`, `lowerLeftLongitude`, `lowerLeftLatitude`, `lowerRightLongitude`, `lowerRightLatitude`, `width`, `length`, `imagePath`) VALUES
(1, 'parking/13', 'SQARE', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, NULL, NULL, 'NULL'),
(2, 'parking/14', 'SQARE', 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, NULL, NULL, 'NULL'),
(3, 'parking/15', 'SQARE', 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, NULL, NULL, 'NULL'),
(4, 'parking/16', 'SQARE', 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, NULL, NULL, 'NULL'),
(5, 'parking/17', 'SQARE', 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, NULL, NULL, 'NULL'),
(6, 'parking/18', 'SQARE', 49.874404, 8.66052, 49.874395, 8.66046, 49.874376, 8.660467, 49.874386, 8.660526, NULL, NULL, 'NULL'),
(7, 'parking/19', 'SQARE', 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, 49.874428, 8.660471, NULL, NULL, 'NULL'),
(8, 'parking/20', 'SQARE', 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, 49.874416, 8.66047, NULL, NULL, 'NULL'),
(9, 'parking/21', 'SQARE', 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, 49.874335, 8.660485, NULL, NULL, 'NULL'),
(10, 'parking/22', 'SQARE', 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, 49.874241, 8.660506, NULL, NULL, 'NULL');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `parkinglots`
--

CREATE TABLE `parkinglots` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL COMMENT 'interne parkinglot id',
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mqttchannel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `status` enum('blocked','free','blocking','freeing','occupied','error') CHARACTER SET utf8 NOT NULL DEFAULT 'error' COMMENT 'current state BLOCKED=barrier up,FREE=barier down,no car, BLOCKING=barrier is moving up,FREEING=barrier is moving down, OCCUPIED=car present, hopefully barrier down, ERROR=see error code',
  `ip` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `hwid` varchar(18) CHARACTER SET utf8 DEFAULT NULL COMMENT 'mac adress ??',
  `lastStateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'timestamp of last state change',
  `type` enum('SINGLE','BARRIER') COLLATE utf8_unicode_ci NOT NULL COMMENT 'barrier type: SINGLE=one barrier fr each parking space, BARRIER=one barrier for mass parking space (Schranke)',
  `locationId` int(11) DEFAULT NULL COMMENT 'reference to location place (coordinates)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `gateway_id`, `status`, `ip`, `hwid`, `lastStateChange`, `type`, `locationId`) VALUES
(1, 13, 'S3|19 P7', 'parking/13', 1, 'blocked', '192.168.10.51', '00-aa-bb-cc-de-02', '2017-09-11 17:48:14', '', 1),
(2, 14, 'S3|19 P8', 'parking/14', 1, 'blocked', '192.168.10.52', '00-aa-bb-cc-de-03', '2018-02-22 15:44:19', '', 2),
(3, 15, 'S3|19 P9', 'parking/15', 1, 'blocked', '192.168.10.53', '00-aa-bb-cc-de-04', '2018-02-22 15:44:19', '', 3),
(4, 16, 'S3|19 P10', 'parking/16', 1, 'blocked', '192.168.10.54', '00-aa-bb-cc-de-05', '2018-02-22 15:44:19', '', 4),
(5, 17, 'S3|19 P11', 'parking/17', 1, 'blocked', '192.168.10.55', '00-aa-bb-cc-de-06', '2018-02-22 15:44:19', '', 5),
(6, 18, 'S3|19 P12', 'parking/18', 1, 'blocked', '192.168.10.56', '00-aa-bb-cc-de-06', '2018-02-22 16:04:17', '', 6),
(7, 19, 'S3|19 P13', 'parking/19', 1, 'blocked', '192.168.10.57', '00-aa-bb-cc-de-07', '2018-02-22 16:04:40', '', 7),
(8, 20, 'S3|19 P14', 'parking/20', 1, 'blocked', '192.168.10.58', '00-aa-bb-cc-de-08', '2018-02-22 16:04:40', '', 8),
(9, 21, 'S3|19 P15', 'parking/21', 1, 'blocked', '192.168.10.59', '00-aa-bb-cc-de-09', '2018-02-22 16:04:40', '', 9),
(10, 22, 'S3|19 P16', 'parking/22', 1, 'blocked', '192.168.10.60', '00-aa-bb-cc-de-10', '2018-02-22 16:04:40', '', 10);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `parkinglots_sites`
--

CREATE TABLE `parkinglots_sites` (
  `id` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL,
  `parkingLotsId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `parkinglots_sites`
--

INSERT INTO `parkinglots_sites` (`id`, `sitesId`, `parkingLotsId`) VALUES
(1, 5, 1),
(2, 5, 2),
(3, 5, 3),
(4, 5, 4),
(5, 5, 5),
(6, 6, 6),
(7, 6, 7),
(8, 6, 8),
(9, 6, 9),
(10, 6, 10);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `token` char(32) CHARACTER SET utf8 NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `visitedPersonId` int(11) DEFAULT NULL,
  `licencePlate` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reservationState` enum('RESERVED','BOOKED','CANCELED','SUPERKEY') COLLATE utf8_unicode_ci NOT NULL COMMENT 'SUPERKEY=free/block without reservation, Hausmeisterschlüssel',
  `messageInfo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notifiedOnDevice`  bit(1) DEFAULT 0,
  `isRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringID` int(11) DEFAULT NULL COMMENT 'see http://www.vertabelo.com/blog/technical-articles/again-and-again-managing-recurring-events-in-a-data-model',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `reservations`
--

INSERT INTO `reservations` (`id`, `token`, `start`, `end`, `parkinglot_id`, `users_id`, `visitedPersonId`, `licencePlate`, `reservationState`, `messageInfo`, `isRecurring`, `recurringID`, `created`) VALUES
(39, 'e39b96b7be3d5c89fc9011432e85cc49', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 1, 57, 57, 'TE-ST 7', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:41:31'),
(40, 'db22d8464d728a7e826361d6c75fbea8', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 2, 57, 57, 'TE-ST 8', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:45:46'),
(41, '42ac3b28ea1f1b5c283e7e40ac5e337e', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 3, 57, 57, 'TE-ST 9', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:46:43'),
(42, '45fc49ca58ba3977075e5e7dded5df9c', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 4, 57, 57, 'TE-ST 10', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:47:20'),
(43, '0ed32a3841dfe4f086684425508dc01f', '2018-04-02 08:00:00', '2018-04-06 18:00:00', 5, 57, 57, 'TE-ST 11', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:47:48'),
(44, '5cdec4c73e6dabb2e530fca59a0da610', '2018-02-23 08:00:00', '2018-02-23 18:00:00', 4, 58, 58, 'TE-ST 10', 'RESERVED', 'GENERIC_MESSAGE', NULL, NULL, '2018-02-22 16:52:24');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `reservations_users`
--

CREATE TABLE `reservations_users` (
  `id` int(11) NOT NULL,
  `reservations_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Parker\nAnfrager\nBesucher/Visitor\nBetreiber\nVerwalter\nAdmin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Guest'),
(3, 'Visitor'),
(4, 'Reservation Manager'),
(5, 'Facility Manager');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sensorlogs`
--

CREATE TABLE `sensorlogs` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `z` int(11) DEFAULT NULL,
  `zThreshold` int(11) DEFAULT NULL,
  `state` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `supplyVoltage` float DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `RSSI_gwRX` int(11) DEFAULT NULL,
  `RSSI_nodeRX_avg` int(11) DEFAULT NULL,
  `numRetries_NodeToGW` int(11) DEFAULT NULL,
  `numRxTimeouts` int(11) DEFAULT NULL,
  `errorCode` int(11) DEFAULT NULL,
  `filename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `token` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `validTo` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'short name ',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'long name',
  `prefix` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `locationID` int(11) NOT NULL,
  `isPublic` enum('NO','YES') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO' COMMENT 'TRUE if connected parking lots are partly/full time public',
  `publicStart` datetime DEFAULT NULL,
  `publicEnd` datetime DEFAULT NULL,
  `isPublicRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `sites`
--

INSERT INTO `sites` (`id`, `name`, `description`, `prefix`, `locationID`, `isPublic`, `publicStart`, `publicEnd`, `isPublicRecurring`, `recurringId`) VALUES
(1, 'KOM / Dez. V', 'NULL', 'kom_', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(2, 'Lichtwiese', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(3, 'Lab', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(4, 'MST', 'NULL', 'NULL', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL),
(5, 'KOM_Smart_App1', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(6, 'KOM_Smart_App2', NULL, NULL, 5, 'NO', NULL, NULL, NULL, NULL),
(7, 'KOM_Smart_Facility_Manager1', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(8, 'KOM_Smart_Facility_Manager2', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(9, 'KOM_Smart_Broker1', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL),
(10, 'KOM_Smart_Broker2', NULL, NULL, 1, 'NO', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'short description',
  `label` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT 'long description'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD'),
(2, 'Some Consultant', 'SCON'),
(3, 'KOM_Student_Group', 'KOMSG');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tenants_users`
--

CREATE TABLE `tenants_users` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3),
(8, 1, 56),
(9, 1, 57),
(10, 1, 58);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `timestamps`
--

CREATE TABLE `timestamps` (
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'username',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'password\n',
  `created` datetime DEFAULT NULL COMMENT 'entry creation timestamp',
  `modified` datetime DEFAULT NULL COMMENT 'last modified',
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email adress',
  `mobileNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mobile number',
  `landlineNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'fixed line number',
  `firstname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'firstname',
  `lastname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'lastname',
  `title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'title like dr',
  `gender` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'male/female/other',
  `userState` enum('ACTIVE','CANCELED','BLOCKED','DELETED') COLLATE utf8_unicode_ci NOT NULL COMMENT 'ACTIVE=login allowed, CANCELED=INACTIVE,no login, BLOCKED=temporary inactive, no login, DELETED=finished, could be eraesed,no login',
  `company` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'company name',
  `department` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'department description',
  `street` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'street name and street number',
  `ZIP` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ZIP code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`, `email`, `mobileNumber`, `landlineNumber`, `firstname`, `lastname`, `title`, `gender`, `userState`, `company`, `department`, `street`, `ZIP`) VALUES
(3, 'smart', '$2y$10$b2lie4vTGXfZDxm/jsD27uQOXdIZqe2eBLStIk4KV8CXMt9.4iLUG', '2015-07-02 22:23:43', '2017-08-22 09:25:22', 'borhansafa@yahoo.com', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'ACTIVE', 'NULL', 'NULL', 'NULL', 'NULL'),
(56, 'admin', '$2y$10$IiURaYPtpfJc6GLPPiXa8.wSStnfVDFFniFP2gS1MLgRDymJm7VVK', '2018-02-22 16:07:54', '2018-02-22 16:07:54', 'admin@admin.de', '', '', 'Admin', 'Administrator', '', 'Male', 'ACTIVE', '', '', '', ''),
(57, 'app', '$2y$10$CXYxuyV9W7kBmLXHcNwjeOjmsSbHbYykf9sRQfvu6wycziKkBeG6O', '2018-02-22 16:09:30', '2018-02-22 16:09:30', 'app@app.de', '', '', 'Android', 'App', '', 'other', 'ACTIVE', '', '', '', ''),
(58, 'dennis', '$2y$10$9BzCizUMjkdqMShSw.7g6e8EOXx3jcMxL8GB7XVgH3khlrhNVdIey', '2018-02-22 16:49:06', '2018-02-22 16:53:04', 'dennis.droessler@stud.tu-darmstadt.de', '', '', 'Dennis', 'Droessler', '', 'Male', 'ACTIVE', '', '', '', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 3, 1),
(88, 56, 1),
(89, 57, 3),
(91, 58, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users_zones`
--

CREATE TABLE `users_zones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `keyName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '????',
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`, `keyName`, `start`, `end`) VALUES
(1, 3, 1, 'NULL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 56, 1, NULL, NULL, NULL),
(9, 57, 1, NULL, NULL, NULL),
(10, 58, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL COMMENT 'id=7 means "All zones", e.g. for admins',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'zone name describes extern view, e.g. department',
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'long description of name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`) VALUES
(1, 'kom', 'KOM Parking Zone P5-P7'),
(2, 'Bibliothek', 'p20 -p 30'),
(3, 'KOM_Smart_App', 'Smartparking App Group Zone'),
(4, 'KOM_Smart_Facility_Manager', 'Facility Manager Group'),
(5, 'KOM_Smart_Broker', 'Smartparking Broker Group');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `zones_sites`
--

CREATE TABLE `zones_sites` (
  `id` int(11) NOT NULL,
  `zonesId` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `zones_sites`
--

INSERT INTO `zones_sites` (`id`, `zonesId`, `sitesId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 5),
(4, 3, 5),
(5, 3, 6);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_role_id_aa` (`role_id`);

--
-- Indizes für die Tabelle `calender`
--
ALTER TABLE `calender`
  ADD PRIMARY KEY (`recurringId`,`recurringTypeId`);

--
-- Indizes für die Tabelle `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `res_id_idx` (`res_id`);

--
-- Indizes für die Tabelle `database_logs`
--
ALTER TABLE `database_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_user_id_dl` (`user_id`);

--
-- Indizes für die Tabelle `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `idx_fk_site_id_gate` (`site_id`);

--
-- Indizes für die Tabelle `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destNode` (`destNode`),
  ADD KEY `gateway_id_idx` (`gateway_id`);

--
-- Indizes für die Tabelle `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pid` (`pid`),
  ADD KEY `pid_2` (`pid`),
  ADD KEY `pid_3` (`pid`),
  ADD KEY `idx_fk_gateway_id_park` (`gateway_id`),
  ADD KEY `idx_fk_locationId_park` (`locationId`);

--
-- Indizes für die Tabelle `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_sitesId_parksites` (`sitesId`),
  ADD KEY `idx_fk_parkingLotId_parksites` (`parkingLotsId`);

--
-- Indizes für die Tabelle `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_parkinglot_id_res` (`parkinglot_id`),
  ADD KEY `idx_fk_recurringID_res` (`recurringID`),
  ADD KEY `idx_fk_users_id_res` (`users_id`),
  ADD KEY `idx_fk_visitedPersonId_res` (`visitedPersonId`);

--
-- Indizes für die Tabelle `reservations_users`
--
ALTER TABLE `reservations_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_reservations_id_resusers` (`reservations_id`),
  ADD KEY `idx_fk_users_id_resusers` (`users_id`);

--
-- Indizes für die Tabelle `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_parkinglot_id_sensor` (`parkinglot_id`);

--
-- Indizes für die Tabelle `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_id_session` (`id`);
ALTER TABLE `session` ADD FULLTEXT KEY `idx_token_session` (`token`);

--
-- Indizes für die Tabelle `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_locationID_sites` (`locationID`),
  ADD KEY `idx_fk_recurringId_sites` (`recurringId`);

--
-- Indizes für die Tabelle `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_tenant_id_tenusr` (`tenant_id`),
  ADD KEY `idx_fk_user_id_tenusr` (`user_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_role_id_usroles` (`role_id`),
  ADD KEY `idx_fk_user_id_usroles` (`user_id`);

--
-- Indizes für die Tabelle `users_zones`
--
ALTER TABLE `users_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fk_user_id_uszones` (`user_id`),
  ADD KEY `idx_fk_zone_id_uszones` (`zone_id`);

--
-- Indizes für die Tabelle `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_id_zonsites` (`id`,`zonesId`,`sitesId`),
  ADD KEY `idx_fk_zonesId_zonsites` (`zonesId`),
  ADD KEY `idx_fk_sitesId_zonsites` (`sitesId`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `apps`
--
ALTER TABLE `apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `auth_acl`
--
ALTER TABLE `auth_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT für Tabelle `calender`
--
ALTER TABLE `calender`
  MODIFY `recurringId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `database_logs`
--
ALTER TABLE `database_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT für Tabelle `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `issuedComands`
--
ALTER TABLE `issuedComands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `parkinglots`
--
ALTER TABLE `parkinglots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT für Tabelle `reservations_users`
--
ALTER TABLE `reservations_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `sensorlogs`
--
ALTER TABLE `sensorlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `tenants_users`
--
ALTER TABLE `tenants_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT für Tabelle `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT für Tabelle `users_zones`
--
ALTER TABLE `users_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id=7 means "All zones", e.g. for admins', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `zones_sites`
--
ALTER TABLE `zones_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD CONSTRAINT `fk_role_id_aa` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD CONSTRAINT `res_id` FOREIGN KEY (`res_id`) REFERENCES `calender` (`recurringId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `database_logs`
--
ALTER TABLE `database_logs`
  ADD CONSTRAINT `fk_user_id_dl` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `gateways`
--
ALTER TABLE `gateways`
  ADD CONSTRAINT `fk_site_id_gate` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD CONSTRAINT `gateway_id` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD CONSTRAINT `fk_gateway_id_park` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_locationId_park` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD CONSTRAINT `fk_parkingLotId_parksites` FOREIGN KEY (`parkingLotsId`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sitesId_parksites` FOREIGN KEY (`sitesId`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `fk_parkinglot_id_res` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurringID_res` FOREIGN KEY (`recurringID`) REFERENCES `calender` (`recurringId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id_res` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_visitedPersonId_res` FOREIGN KEY (`visitedPersonId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `reservations_users`
--
ALTER TABLE `reservations_users`
  ADD CONSTRAINT `fk_reservations_id_resusers` FOREIGN KEY (`reservations_id`) REFERENCES `reservations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id_resusers` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD CONSTRAINT `fk_parkinglot_id_sensor` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `fk_locationID_sites` FOREIGN KEY (`locationID`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recurringId_sites` FOREIGN KEY (`recurringId`) REFERENCES `calender` (`recurringId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD CONSTRAINT `fk_tenant_id_tenusr` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id_tenusr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `fk_role_id_usroles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id_usroles` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `users_zones`
--
ALTER TABLE `users_zones`
  ADD CONSTRAINT `fk_user_id_uszones` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zone_id_uszones` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD CONSTRAINT `fk_sitesId_zonsites` FOREIGN KEY (`sitesId`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_zonesId_zonsites` FOREIGN KEY (`zonesId`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
