-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2017 at 07:04 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

CREATE TABLE `apps` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secret` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `state` enum('ACTIVE','BLOCKED','DELETED') COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_acl`
--

CREATE TABLE `auth_acl` (
  `id` int(11) NOT NULL,
  `controller` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `actions` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '*',
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_acl`
--

INSERT INTO `auth_acl` (`id`, `controller`, `actions`, `role_id`) VALUES
(1, 'Reservations', '*', 1),
(2, 'Users', 'index', 2),
(3, 'Users', '*', 1),
(4, 'Dashboard', '*', 1),
(5, 'RemoteKeys', '*', 1),
(6, 'AuthAcl', '*', 1),
(7, 'Maps', '*', 1),
(8, 'Roles', '*', 1),
(9, 'Users', '*', 4),
(10, 'DatabaseLogs', '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `calender`
--

CREATE TABLE `calender` (
  `recurringId` int(11) NOT NULL,
  `recurringTypeId` int(11) NOT NULL,
  `separationCount` int(11) DEFAULT NULL,
  `maxNumOfOccurences` int(11) DEFAULT NULL,
  `dayOfWeek` int(11) DEFAULT NULL,
  `WeekOfMonth` int(11) DEFAULT NULL,
  `DayOfMonth` int(11) DEFAULT NULL,
  `MonthOfYear` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `calender_exceptions`
--

CREATE TABLE `calender_exceptions` (
  `id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `isScheduled` bit(1) NOT NULL,
  `isCancelled` bit(1) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `isFullDayReservation` bit(1) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `database_logs`
--

CREATE TABLE `database_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `context` text COLLATE utf8_unicode_ci,
  `created` timestamp NULL DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hostname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(10) NOT NULL DEFAULT '0',
  `tenant` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` enum('CREATE','UPDATE','DELETE','OTHER') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OTHER',
  `userName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRole` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldValue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newValue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `database_logs`
--

INSERT INTO `database_logs` (`id`, `type`, `message`, `context`, `created`, `ip`, `hostname`, `uri`, `refer`, `user_agent`, `count`, `tenant`, `action`, `userName`, `userRole`, `oldValue`, `newValue`, `user_id`) VALUES
(23, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewCustomer , Reservation Manager\n    [newValue] => BrandNewCustomer , Guest\n)', '2017-08-26 20:14:27', '::1', 'localhost', '/users/edit/5', 'https://localhost/users/edit/5', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewCustomer , Reservation Manager', 'BrandNewCustomer , Guest', 3),
(24, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => nhkdi , Visitor\n)', '2017-08-26 20:15:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'nhkdi , Visitor', 3),
(25, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => nhkdi , Visitor\n    [newValue] => NewFacility , Facility Manager\n)', '2017-08-26 20:16:49', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'nhkdi , Visitor', 'NewFacility , Facility Manager', 3),
(26, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => NewFacility , Facility Manager\n    [newValue] => ndhaknd , Reservation Manager\n)', '2017-08-26 20:18:39', '::1', 'localhost', '/users/edit/33', 'https://localhost/users/edit/33', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'NewFacility , Facility Manager', 'ndhaknd , Reservation Manager', 3),
(27, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => ksmjs , Visitor\n)', '2017-08-26 20:54:59', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'ksmjs , Visitor', 3),
(28, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => dacaa , Reservation Manager\n)', '2017-08-26 20:56:13', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'dacaa , Reservation Manager', 3),
(29, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => mah , \n)', '2017-08-26 21:03:05', '::1', 'localhost', '/users/delete/6', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'mah , ', NULL, 3),
(30, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Fac , Facility Manager\n)', '2017-08-26 21:04:05', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'Fac , Facility Manager', 3),
(31, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => Fac , Facility Manager\n)', '2017-08-26 21:04:11', '::1', 'localhost', '/users/delete/36', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'Fac , Facility Manager', NULL, 3),
(32, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Reservation , Reservation Manager\n)', '2017-08-28 12:14:01', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(33, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => guest , Admin\n)', '2017-08-28 12:15:22', '::1', 'localhost', '/users/delete/38', 'https://localhost/users', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'guest , Admin', NULL, 3),
(34, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Reservation , Reservation Manager\n    [newValue] => Visitor , Visitor\n)', '2017-08-28 12:16:26', '::1', 'localhost', '/users/edit/39', 'https://localhost/users/edit/39', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'Reservation , Reservation Manager', 'Visitor , Visitor', 3),
(35, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 12:17:31', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'Facility , Facility Manager', 3),
(36, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => vis , Visitor\n)', '2017-08-28 15:55:50', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'vis , Visitor', 3),
(37, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => vis , Visitor\n    [newValue] => facilit , Facility Manager\n)', '2017-08-28 15:56:32', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'vis , Visitor', 'facilit , Facility Manager', 3),
(38, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => facilit , Facility Manager\n)', '2017-08-28 15:57:03', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'facilit , Facility Manager', NULL, 3),
(39, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => Facility , Facility Manager\n    [newValue] => FacilityAdmin , Admin\n)', '2017-08-28 22:42:52', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(40, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => FacilityAdmin , Admin\n    [newValue] => Facility , Facility Manager\n)', '2017-08-28 22:43:59', '::1', 'localhost', '/users/edit/40', 'https://localhost/users/edit/40', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'FacilityAdmin , Admin', 'Facility , Facility Manager', 3),
(41, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => res , Reservation Manager\n)', '2017-08-29 12:35:27', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'res , Reservation Manager', 3),
(42, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => visme , Visitor\n)', '2017-08-29 12:41:43', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'res', 'Reservation Manager', NULL, 'visme , Visitor', 41),
(43, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => newVisi , Visitor\n)', '2017-08-30 18:00:42', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, NULL, 'OTHER', NULL, NULL, NULL, NULL, NULL),
(44, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => newVisi , Visitor\n)', '2017-08-30 18:02:08', '::1', 'localhost', '/users/delete/43', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'newVisi , Visitor', NULL, 3),
(45, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => visme , Visitor\n    [newValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 18:02:36', '::1', 'localhost', '/users/edit/42', 'https://localhost/users/edit/42', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'visme , Visitor', 'vismejdhd , Reservation Manager', 3),
(46, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => CREATE\n    [tenant] => TU Darmstadt\n    [newValue] => gu , Guest\n)', '2017-08-30 18:03:04', '::1', 'localhost', '/users/add', 'https://localhost/users/add', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'CREATE', 'smart', 'Admin', NULL, 'gu , Guest', 3),
(47, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => gu , Guest\n)', '2017-08-30 18:31:30', '::1', 'localhost', '/users/delete/44', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'gu , Guest', NULL, 3),
(48, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => vismejdhd , Reservation Manager\n)', '2017-08-30 18:32:18', '::1', 'localhost', '/users/delete/42', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'vismejdhd , Reservation Manager', NULL, 3),
(49, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => UPDATE\n    [tenant] => TU Darmstadt\n    [oldValue] => res , Reservation Manager\n    [newValue] => resd , Guest\n)', '2017-08-30 18:55:05', '::1', 'localhost', '/users/edit/41', 'https://localhost/users/edit/41', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'UPDATE', 'smart', 'Admin', 'res , Reservation Manager', 'resd , Guest', 3),
(50, 'info', 'Add to databaselog.log', 'Array\n(\n    [scope] => operation\n    [action] => DELETE\n    [tenant] => TU Darmstadt\n    [oldValue] => resd , Guest\n)', '2017-08-30 19:01:21', '::1', 'localhost', '/users/delete/41', 'https://localhost/users', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 1, 'TU Darmstadt', 'DELETE', 'smart', 'Admin', 'resd , Guest', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `database_log_phinxlog`
--

CREATE TABLE `database_log_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `database_log_phinxlog`
--

INSERT INTO `database_log_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20161005230232, 'InitDatabaseLogs', '2017-08-23 13:07:57', '2017-08-23 13:07:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fqdn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `label`, `fqdn`, `site_id`) VALUES
(1, 'RTST', 'rtst.gateway.smartparking.hornjak.de', 1),
(2, 'MST', 'mst.gateway.smartparking.hornjak.de', 4);

-- --------------------------------------------------------

--
-- Table structure for table `issuedComands`
--

CREATE TABLE `issuedComands` (
  `id` int(11) NOT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `cmd` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `destNode` int(11) DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('SQARE','CIRCLE','ELLIPSE') COLLATE utf8_unicode_ci DEFAULT NULL,
  `upperLeftLongitude` double DEFAULT NULL,
  `upperLeftLatitude` double DEFAULT NULL,
  `upperRightLongitude` double DEFAULT NULL,
  `upperRightLatitude` double DEFAULT NULL,
  `lowerLeftLongitude` double DEFAULT NULL,
  `lowerLeftLatitude` double DEFAULT NULL,
  `lowerRightLongitude` double DEFAULT NULL,
  `lowerRightLatitude` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `imagePath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots`
--

CREATE TABLE `parkinglots` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mqttchannel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gateway_id` int(11) DEFAULT NULL,
  `status` enum('blocked','free','blocking','freeing','occupied','error') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'error',
  `ip` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `hwid` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `lastStateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('SINGLE','BARRIER') COLLATE utf8_unicode_ci DEFAULT NULL,
  `locationId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parkinglots`
--

INSERT INTO `parkinglots` (`id`, `pid`, `description`, `mqttchannel`, `gateway_id`, `status`, `ip`, `hwid`, `lastStateChange`, `type`, `locationId`) VALUES
(1, 13, 'S3|19 P7', 'parking/13', 1, 'blocked', '192.168.10.51', '00-aa-bb-cc-de-02', '2017-04-20 12:44:34', NULL, 0),
(7, 99, 'devTest', 'parking/99', 2, 'blocked', '192.168.18.68', '00-aa-bb-cc-de-04', '2017-03-08 11:06:11', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots_sites`
--

CREATE TABLE `parkinglots_sites` (
  `id` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL,
  `parkingLotsId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parkinglots_zones`
--

CREATE TABLE `parkinglots_zones` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `remoteKeys`
--

CREATE TABLE `remoteKeys` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_until` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservationrequests`
--

CREATE TABLE `reservationrequests` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('m','f') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'notAccepted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `visitedPersonId` int(11) NOT NULL,
  `licencePlate` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `reservationState` enum('RESERVED','BOOKED','CANCELED','SUPERKEY') COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageInfo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringID` int(11) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Guest'),
(3, 'Visitor'),
(4, 'Reservation Manager'),
(5, 'Facility Manager');

-- --------------------------------------------------------

--
-- Table structure for table `sensorlogs`
--

CREATE TABLE `sensorlogs` (
  `id` int(11) NOT NULL,
  `parkinglot_id` int(11) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `z` int(11) DEFAULT NULL,
  `zThreshold` int(11) DEFAULT NULL,
  `state` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplyVoltage` float DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `RSSI_gwRX` int(11) DEFAULT NULL,
  `RSSI_nodeRX_avg` int(11) DEFAULT NULL,
  `numRetries_NodeToGW` int(11) DEFAULT NULL,
  `numRxTimeouts` int(11) DEFAULT NULL,
  `errorCode` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verbatim` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `token` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `validTo` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locationID` int(11) NOT NULL,
  `isPublic` enum('NO','YES') COLLATE utf8_unicode_ci DEFAULT NULL,
  `publicStart` datetime DEFAULT NULL,
  `publicEnd` datetime DEFAULT NULL,
  `isPublicRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `name`, `description`, `prefix`, `locationID`, `isPublic`, `publicStart`, `publicEnd`, `isPublicRecurring`, `recurringId`) VALUES
(1, 'KOM / Dez. V', NULL, 'kom_', 0, NULL, NULL, NULL, NULL, NULL),
(2, 'Lichtwiese', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL),
(3, 'Lab', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL),
(4, 'MST', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `label`) VALUES
(1, 'TU Darmstadt', 'TUD');

-- --------------------------------------------------------

--
-- Table structure for table `tenants_users`
--

CREATE TABLE `tenants_users` (
  `id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tenants_users`
--

INSERT INTO `tenants_users` (`id`, `tenant_id`, `user_id`) VALUES
(1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landlineNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userState` enum('ACTIVE','CANCELED','BLOCKED','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `company` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIP` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`, `modified`, `email`, `mobileNumber`, `landlineNumber`, `firstname`, `lastname`, `title`, `gender`, `userState`, `company`, `department`, `street`, `ZIP`) VALUES
(3, 'smart', '$2y$10$b2lie4vTGXfZDxm/jsD27uQOXdIZqe2eBLStIk4KV8CXMt9.4iLUG', '2015-07-02 22:23:43', '2017-08-22 09:25:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, NULL, NULL),
(4, 'admin', '912ec803b2ce49e4a541068d495ab570', '2016-09-14 00:00:00', '2017-05-15 22:01:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, NULL, NULL),
(39, 'Visitor', '$2y$10$vJDXCaEG7serNOGo4BMqKukx4CrhBn/7U4GP7gkd3UqpI4OHWXHDK', '2017-08-28 12:14:00', '2017-08-28 12:16:26', 're@r.com', '', '', 'rese', 'mand', '', '', '', '', '', '', ''),
(40, 'Facility', '$2y$10$TBrw62X624X75.J3RPogg.c2eDw8HWehHvaunBZewL/1hejxB.poS', '2017-08-28 12:17:31', '2017-08-28 22:43:59', 'fa@e.com', '', '', 'Facility', 'Manager', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 3, 1),
(23, 25, 1),
(24, 26, 1),
(25, 27, 1),
(26, 28, 1),
(27, 29, 1),
(28, 30, 1),
(29, 31, 1),
(30, 32, 4),
(43, 39, 3),
(48, 40, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users_zones`
--

CREATE TABLE `users_zones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `keyName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_zones`
--

INSERT INTO `users_zones` (`id`, `user_id`, `zone_id`, `keyName`, `start`, `end`) VALUES
(1, 3, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `gender` enum('m','f') COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `gender`, `title`, `name`, `mail`, `mobile`) VALUES
(1, 'm', NULL, 'Mathias', 'mathias@traincloak.de', ''),
(3, 'm', NULL, 'Mathias', 'ttn@traincloak.de', ''),
(4, 'm', NULL, 'Mathias', 'mathias@traincloak.de', ''),
(5, 'm', NULL, 'Mathias', 'mathias@traincloak.de', '');

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`id`, `name`, `description`) VALUES
(1, 'kom', 'KOM Parking Zone P5-P7');

-- --------------------------------------------------------

--
-- Table structure for table `zones_sites`
--

CREATE TABLE `zones_sites` (
  `id` int(11) NOT NULL,
  `zonesId` int(11) NOT NULL,
  `sitesId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_acl`
--
ALTER TABLE `auth_acl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calender`
--
ALTER TABLE `calender`
  ADD PRIMARY KEY (`recurringId`,`recurringTypeId`);

--
-- Indexes for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `database_logs`
--
ALTER TABLE `database_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- Indexes for table `database_log_phinxlog`
--
ALTER TABLE `database_log_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `site_id_idx` (`site_id`);

--
-- Indexes for table `issuedComands`
--
ALTER TABLE `issuedComands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destNode` (`destNode`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkinglots`
--
ALTER TABLE `parkinglots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pid` (`pid`),
  ADD KEY `pid_2` (`pid`),
  ADD KEY `pid_3` (`pid`),
  ADD KEY `gateway_id_fk_idx` (`gateway_id`);

--
-- Indexes for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkinglots_zones`
--
ALTER TABLE `parkinglots_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `remoteKeys`
--
ALTER TABLE `remoteKeys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkinglotId` (`parkinglot_id`);

--
-- Indexes for table `reservationrequests`
--
ALTER TABLE `reservationrequests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingLot_id` (`parkinglot_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkinglot_id_fk_idx` (`parkinglot_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants_users`
--
ALTER TABLE `tenants_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_zones`
--
ALTER TABLE `users_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zones_sites`
--
ALTER TABLE `zones_sites`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_acl`
--
ALTER TABLE `auth_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `calender_exceptions`
--
ALTER TABLE `calender_exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `database_logs`
--
ALTER TABLE `database_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `issuedComands`
--
ALTER TABLE `issuedComands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parkinglots`
--
ALTER TABLE `parkinglots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `parkinglots_sites`
--
ALTER TABLE `parkinglots_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parkinglots_zones`
--
ALTER TABLE `parkinglots_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `remoteKeys`
--
ALTER TABLE `remoteKeys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reservationrequests`
--
ALTER TABLE `reservationrequests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tenants`
--
ALTER TABLE `tenants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tenants_users`
--
ALTER TABLE `tenants_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `users_zones`
--
ALTER TABLE `users_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `zones_sites`
--
ALTER TABLE `zones_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gateways`
--
ALTER TABLE `gateways`
  ADD CONSTRAINT `site_id` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `remoteKeys`
--
ALTER TABLE `remoteKeys`
  ADD CONSTRAINT `parkinglot_id` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sensorlogs`
--
ALTER TABLE `sensorlogs`
  ADD CONSTRAINT `parkinglot_id_fk` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
