-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2017 at 12:53 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `parkinglot_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `visitedPersonId` int(11) NOT NULL,
  `licencePlate` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `reservationState` enum('RESERVED','BOOKED','CANCELED','SUPERKEY') COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageInfo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isRecurring` enum('YES','NO') COLLATE utf8_unicode_ci DEFAULT NULL,
  `recurringID` int(11) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `token`, `start`, `end`, `parkinglot_id`, `users_id`, `visitedPersonId`, `licencePlate`, `reservationState`, `messageInfo`, `isRecurring`, `recurringID`, `created`) VALUES
(18, '322072516324a9e6b7026a76146ed326', '2017-09-06 17:00:00', '2017-09-06 20:00:00', 1, 3, 40, 'ikjs', '', '', '', NULL, '2017-09-06 17:01:09'),
(19, '5aa17cb032f71d3959f053447d5d9380', '2017-09-07 13:01:00', '2017-09-07 16:01:00', 1, 3, 39, 'fsadd', '', '', '', NULL, '2017-09-07 13:01:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingLot_id` (`parkinglot_id`),
  ADD KEY `fk_visited_person_id` (`visitedPersonId`),
  ADD KEY `fk_users_id` (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `fk_parkinglots_id` FOREIGN KEY (`parkinglot_id`) REFERENCES `parkinglots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_visited_person_id` FOREIGN KEY (`visitedPersonId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
