<?php
use Cake\Routing\Router;

Router::plugin('Smartparking', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});
