<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('New RemoteKey'), ['action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('New Reservation'), ['controller' => 'RemoteKeys', 'action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Parking Lots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Sensorlogs'), ['controller' => 'Sensorlogs', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Remotes List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('username') ?></th>
                            <th><?= $this->Paginator->sort('mail') ?></th>
                            <th><?= $this->Paginator->sort('valid_from') ?></th>
                            <th><?= $this->Paginator->sort('valid_until') ?></th>
                            <th><?= $this->Paginator->sort('parkinglot_id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 1; ?>
                        <?php foreach ($remoteKeys as $remoteKey): ?>
                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                        <td class="sorting_1"><?= $this->Number->format($remoteKey->id) ?></td>
                        <td><?= h($remoteKey->username) ?></td>
                        <td><?= h($remoteKey->mail) ?></td>
                        <td><?= h($remoteKey->valid_from) ?></td>
                        <td><?= h($remoteKey->valid_until) ?></td>
                        <td>
                            <?= $this->Html->link($remoteKey->parkinglot->description, ['controller' => 'Parkinglots', 'action' => 'view', $remoteKey->parkinglot->id])?>
                        </td>
                        <td class="actions">
                            <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
                								     'url' => ['controller' => 'RemoteKeys',
                								     		   'action' => 'view', $remoteKey->id]]) ?>
                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
                								     'url' => ['controller' => 'RemoteKeys',
                								     		   'action' => 'edit', $remoteKey->id]
                								   ]) ?>
                            <?= $this->Html->link(
                					  $this->Html->image('mail.png', [ 'alt' => __('Resend'), 'width' => '16px', 'height' => '14px']),
                					  ['controller' => 'RemoteKeys', 'action' => 'resend', $remoteKey->id],
                					  ['confirm' => __('Are you sure you want to resend the remoteKey Email to  # {0}?', $remoteKey->username),
                					   'escapeTitle' => false]
                				     ) ?>
                            <?= $this->Html->link(
                					  $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                					  ['controller' => 'RemoteKeys', 'action' => 'delete', $remoteKey->id],
                					  ['confirm' => __('Are you sure you want to delete # {0}?', $remoteKey->id),
                					   'escapeTitle' => false]
                				     ) ?>
                        </td>
                    </tr>
                    <?php $counter++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Remote Keys', ['controller' => 'RemoteKeys', 'action' => 'index']); ?>