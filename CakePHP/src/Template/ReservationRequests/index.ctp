<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('New Reservation Request'), ['action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('New RemoteKey'), ['controller' => 'RemoteKeys', 'action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('Request Reservation'), ['controller' => 'ReservationRequests', 'action' => 'add']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Reservation Requests List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('firstname') ?></th>
                            <th><?= $this->Paginator->sort('email') ?></th>
                            <th><?= $this->Paginator->sort('start') ?></th>
                            <th><?= $this->Paginator->sort('end') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 1;?>
                        <?php foreach ($reservationRequests as $reservationRequest): ?>
                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                        <td class="sorting_1"><?= $this->Number->format($reservationRequest->id) ?></td>
                        <td><?= h($reservationRequest->firstname) ?></td>
                        <td><?= h($reservationRequest->email) ?></td>
                        <td><?= h($reservationRequest->start) ?></td>
                        <td><?= h($reservationRequest->end) ?></td>
                        <td class="actions center">
                            <?= $this->Html->image('check.png', [ 'alt' => __('Accept'),
                								     'url' => ['controller' => 'ReservationRequests',
                								     		   'action' => 'accept', $reservationRequest->id]
                								   ]) ?>
                            <?= $this->Html->link(
                					  $this->Html->image('delete.png', [ 'alt' => __('Reject')]),
                					  ['controller' => 'ReservationRequests', 'action' => 'delete', $reservationRequest->id],
                					  ['confirm' => __('Are you sure you want to delete # {0}?', $reservationRequest->id),
                					   'escapeTitle' => false]
                				     ) ?>
                        </td>
                    </tr>
                    <?php $counter++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
        <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Reservation Requests', ['controller' => 'ReservationRequests', 'action' => 'index']); ?>