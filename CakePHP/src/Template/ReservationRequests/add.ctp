<?php use Cake\Routing\Router; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Reservation Request
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                     <?= $this->Form->create($reservationRequest) ?>
                                        <fieldset>
                                            <legend><?= __('Request Reservation') ?></legend>
                                            <?php
                                            	$this->Form->templates([
                                        			'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
                                    			]);
                                            	echo "<div class='form-group'>".$this->Form->input('gender', array('type' => 'select',
                                    							        		'options' => array(
                                    							        							"m" => "Male",
                                    							        							"f" => "Female"),'class' => 'form-control'
                                    											))."</div>";

                                                echo "<div class='form-group'>".$this->Form->input('firstname', ['label' => 'Firstname:', 'class' => 'form-control'])."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('lastname', ['label' => 'Lastname:', 'class' => 'form-control'])."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('email', ['label' => 'Email:', 'class' => 'form-control'])."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('phone', ['label' => 'Phone:', 'class' => 'form-control'])."</div>";



                                                ?>

                                                <div class='form-group'>
                                                    <label>Start date and time:</label>
                                                    <?= $this->Form->text('start', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i')]); ?>
                                                </div>

                                                <div class='form-group'>
                                                    <label>End date and time:</label>
                                                     <?= $this->Form->text('end', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i', strtotime('+3 hour'))]) ;?>
                                                 </div>

                                                <script type="text/javascript">
                                                    $.datetimepicker.setLocale('de');
                                                    $('.dtpicker').datetimepicker({
                                                    inline: false,
                                                    step: 15,
                                                    format:'d.m.Y H:i',
                                                    });
                                                </script>

                                        </fieldset>
                                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<?php $this->Html->addCrumb('Reservation Requests', '/reservationRequests'); ?>
<?php $this->Html->addCrumb('Add', ['controller' => 'ReservationRequests', 'action' => 'add']); ?>