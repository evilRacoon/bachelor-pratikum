<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Request Reservation
            </div>
            <div class="panel-body">
                <div class="row reservation-add-submit-form">
                    <?= $this->Form->create($reservationRequest) ?>
                    <fieldset>
                        <div class="col-lg-6">
                            <?php
                                $this->Form->templates([
                                    'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
                                ]);
                                echo "<div class='form-group'>".$this->Form->input('gender', array('type' => 'select',
                                                                'options' => array(
                                                                                    "m" => "Male",
                                                                                    "f" => "Female"),'class' => 'form-control'
                                                                ))."</div>";

                                echo "<div class='form-group'>".$this->Form->input('firstname', ['label' => 'Firstname:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('lastname', ['label' => 'Lastname:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('email', ['label' => 'Email:', 'class' => 'form-control'])."</div>";
                                echo "<div class='form-group'>".$this->Form->input('phone', ['label' => 'Phone:', 'class' => 'form-control'])."</div>";

                            ?>
                            <?= $this->Form->button(__('Submit'),  array('class' => 'btn btn-default reservation-edit-submit')) ?>
                        </div>
                        <div class="col-lg-6">
                            <div class='form-group'>
                                <label>Start date and time:</label>
                                <?= $this->Form->text('start', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i')]); ?>
                            </div>

                            <div class='form-group'>
                                <label>End date and time:</label>
                                <?= $this->Form->text('end', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i', strtotime('+3 hour'))]) ;?>
                            </div>
                        </div>

                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
                <?php if($this->Flash->render('success')):?>
                <div class="alert alert-success">
                    The reservation has been saved and the visitor was notified of any changes to his booking.<br>

                </div>
                <?php endif; ?>
                <?php if($this->Flash->render('error')):?>
                <div class="alert alert-danger">
                    The reservation could not be saved. Please, try again.<br>
                </div>
                <?php endif; ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
        <!-- /.row -->
