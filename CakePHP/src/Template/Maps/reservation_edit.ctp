            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Reservation
                        </div>
                        <div class="panel-body">
                            <div class="row reservation-edit-submit-form">
                                <?= $this->Form->create($reservation) ?>
                                <fieldset>
                                <div class="col-lg-6">
                                            <?php   $this->Form->templates([
                                                    'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
                                            ]); ?>
                                            <div class='form-group'>
                                                <?php echo $this->Form->input('visitor.name', ['label' => 'Visitor Name:', 'class' => 'form-control']); ?>
                                            </div>
                                            <div class='form-group'>
                                                <?php echo $this->Form->input('visitor.mail', ['label' => 'Visitor Mail:', 'class' => 'form-control']); ?>
                                            </div>
                                            <div class='form-group'>
                                                <?php echo $this->Form->input('visitor.mobile', ['label' => 'Visitor Mobile:', 'class' => 'form-control']); ?>
                                            </div>
                                            <div class='form-group'>
                                                <?php echo  $this->Form->input('token', ['readonly' => 'readonly',  'class' => 'form-control']); ?>
                                            </div>
                                    <?= $this->Form->button(__('Submit'),  array('class' => 'btn btn-default reservation-edit-submit')) ?>
                                </div>
                                <div class="col-lg-6">
                                    <div class='form-group'>
                                        <label>Start date and time:</label>
                                        <?= $this->Form->text('start', ['class' => 'dtpicker form-control', ['monthNames' => false]]); ?>
                                    </div>
                                    <div class='form-group'>
                                        <label>End date and time:</label>
                                        <?= $this->Form->text('end', ['class' => 'dtpicker form-control', ['monthNames' => false]]); ?>
                                    </div>
                                    <div class='form-group'>
                                        <?php echo $this->Form->input('parkinglot_description', ['options' => $parkinglots]); ?>
                                    </div>
                                    <div class='form-group'>
                                        <?php echo $this->Form->input('visitedPersonName', ['label' => 'Visited Person Name:', 'class' => 'form-control']); ?>
                                    </div>
                                </div>

                                </fieldset>
                                <?= $this->Form->end() ?>
                            </div>
                            <?php if($this->Flash->render('success')):?>
                                <div class="alert alert-success">
                                    The reservation has been saved and the visitor was notified of any changes to his booking.<br>
                                    <a class="modal-reservation" data-reservation-parking-id="<?php echo $reservation['parkinglot_id']; ?>">Back to reservation list</a>
                                </div>
                            <?php endif; ?>
                            <?php if($this->Flash->render('error')):?>
                                <div class="alert alert-danger">
                                    The reservation could not be saved. Please, try again.<br>
                                    <a class="modal-reservation" data-reservation-parking-id="<?php echo $reservation['parkinglot_id']; ?>">Back to reservation list</a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
