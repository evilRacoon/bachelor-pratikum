<?php
/**
 * @var \App\View\AppView $this
 */
?>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= h($databaseLog->id) ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Type') ?></th>
                        <td><?= h($databaseLog->type) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Ip') ?></th>
                        <td><?= h($databaseLog->ip) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Hostname') ?></th>
                        <td><?= h($databaseLog->hostname) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Uri') ?></th>
                        <td><?= h($databaseLog->uri) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Refer') ?></th>
                        <td><?= h($databaseLog->refer) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('User Agent') ?></th>
                        <td><?= h($databaseLog->user_agent) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Tenant') ?></th>
                        <td><?= h($databaseLog->tenant) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Action') ?></th>
                        <td><?= h($databaseLog->action) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('UserName') ?></th>
                        <td><?= h($databaseLog->userName) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('UserRole') ?></th>
                        <td><?= h($databaseLog->userRole) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('OldValue') ?></th>
                        <td><?= h($databaseLog->oldValue) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('NewValue') ?></th>
                        <td><?= h($databaseLog->newValue) ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('User Id') ?></th>
                        <td><?= $databaseLog->has('user_id') ? $this->Html->link($databaseLog->user_id, ['controller' => 'Users', 'action' => 'view', $databaseLog->user_id]) : '' ?></td>
                    </tr>


                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $databaseLog->id ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Count') ?></th>
                        <td><?= $databaseLog->count ?></td>
                    </tr>

                    <tr role="row" class="gradeA odd">
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($databaseLog->created) ?></td>
                    </tr>

                    <tr role="row" class="gradeA odd">
                        <th><?= __('Message') ?></th>
                        <td><?= $this->Text->autoParagraph(h($databaseLog->message)); ?></td>
                    </tr>
                    <tr role="row" class="gradeA odd">
                        <th><?= __('Context') ?></th>
                        <td><?= $this->Text->autoParagraph(h($databaseLog->context)); ?></td>
                    </tr>
                    </thead>
                </table>


            </div>
        </div>
    </div>
</div>
