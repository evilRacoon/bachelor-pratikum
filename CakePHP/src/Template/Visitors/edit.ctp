<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $visitor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $visitor->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Visitors'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reservation'), ['controller' => 'Reservations', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="visitors form large-10 medium-9 columns">
    <?= $this->Form->create($visitor) ?>
    <fieldset>
        <legend><?= __('Edit Visitor') ?></legend>
        <?php
            echo $this->Form->input('gender');
            echo $this->Form->input('title');
            echo $this->Form->input('name');
            echo $this->Form->input('mail');
            echo $this->Form->input('mobile');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->addCrumb('Visitors', '/visitors'); ?>
<?php $this->Html->addCrumb('edit', ['controller' => 'Visitors', 'action' => 'edit']); ?>