<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Visitor'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reservation'), ['controller' => 'Reservations', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="visitors index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('title') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('mail') ?></th>
            <th><?= $this->Paginator->sort('mobile') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($visitors as $visitor): ?>
        <tr>
            <td><?= $this->Number->format($visitor->id) ?></td>
            <td><?= h($visitor->title) ?></td>
            <td><?= h($visitor->name) ?></td>
            <td><?= h($visitor->mail) ?></td>
            <td><?= h($visitor->mobile) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $visitor->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $visitor->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $visitor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitor->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?php $this->Html->addCrumb('Visitors', ['controller' => 'Visitors', 'action' => 'index']); ?>