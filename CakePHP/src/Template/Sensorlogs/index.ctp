<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('List Parkinglots'), ['controller' => 'Parkinglots', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('New Parkinglot'), ['controller' => 'Parkinglots', 'action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sensors List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('parkinglot_id') ?></th>
                            <th><?= $this->Paginator->sort('x') ?></th>
                            <th><?= $this->Paginator->sort('y') ?></th>
                            <th><?= $this->Paginator->sort('z') ?></th>
                            <th><?= $this->Paginator->sort('logtime') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 1; ?>
                        <?php foreach ($sensorlogs as $sensorlog): ?>
                        <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                        <td class="sorting_1"><?= $this->Number->format($sensorlog->id) ?></td>
                        <td><?= $sensorlog->has('parkinglot') ? $this->Html->link($sensorlog->parkinglot->pid, ['controller' => 'Parkinglots', 'action' => 'view', $sensorlog->parkinglot->id]) : '' ?></td>
                        <td><?= $this->Number->format($sensorlog->x) ?></td>
                        <td><?= $this->Number->format($sensorlog->y) ?></td>
                        <td class="center"><?= $this->Number->format($sensorlog->z) ?></td>
                        <td><?= h($sensorlog->logtime->i18nFormat('dd.MM.YYYY HH:mm:ss')) ?></td>
                        <td class="actions center">
                            <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
                								     'url' => ['controller' => 'Sensorlogs',
                								     		   'action' => 'edit', $sensorlog->id]
                								   ]) ?>

                            <?= $this->Html->link(
                					  $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                					  ['controller' => 'Sensorlogs', 'action' => 'delete', $sensorlog->id],
                					  ['confirm' => __('Are you sure you want to delete # {0}?', $sensorlog->id),
                					   'escapeTitle' => false]
                				     ) ?>
                        </td>
                    </tr>
                    <?php $counter++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
        <!-- /.col-lg-12 -->
</div>


<?php $this->Html->addCrumb('Screenlogs', ['controller' => 'Screenlogs', 'action' => 'index']); ?>
