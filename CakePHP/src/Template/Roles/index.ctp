<?php
/**
  * @var \App\View\AppView $this
  */
?>
<form>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
        <li class="btn btn-default"><?= $this->Html->link(__('New Role'), ['action' => 'add']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Auth Acl'), ['controller' => 'AuthAcl', 'action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Auth Acl'), ['controller' => 'AuthAcl', 'action' => 'add']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li class="btn btn-default"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        </ul>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Roles list
            </div>
<div class="panel-body">
    <!--<h3><?= __('Roles') ?></h3>-->
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($roles as $role): ?>
            <tr>
                <td><?= $this->Number->format($role->id) ?></td>
                <td><?= h($role->name) ?></td>
                <td class="actions center">

                    <?= $this->Html->image('view.gif', [ 'alt' => __('View'), 'url' => ['controller' => 'Roles', 'action' => 'view', $role->id]]) ?>

                    <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'), 'url' => ['controller' => 'Roles', 'action' => 'edit', $role->id]]) ?>

                    <?= $this->Form->postlink($this->Html->image('delete.png', [ 'alt' => __('Delete')]), ['controller' => 'Roles', 'action' => 'delete', $role->id], ['confirm' => __('Are you sure you want to delete {0}?', $role->name), 'escapeTitle' => false]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
        </div>
    </div>
</div>
</div>
</div>
</form>
<?php $this->Html->addCrumb('Roles', ['controller' => 'Roles', 'action' => 'index']); ?>