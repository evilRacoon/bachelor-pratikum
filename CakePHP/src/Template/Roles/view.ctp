<?php
/**
  * @var \App\View\AppView $this
  */
?>
<form>
<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header"><?= __('Actions') ?></h1>
    <ul class="side-nav">
        <li class="btn btn-default"><?= $this->Html->link(__('List Roles'), ['action' => 'index']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Role'), ['action' => 'add']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('Edit Role'), ['action' => 'edit', $role->id]) ?> </li>
        <li class="btn btn-default"><?= $this->Html->Link(__('Delete Role'), ['action' => 'delete', $role->id], ['confirm' => __('Are you sure you want to delete {0}?', $role->name)]) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Auth Acl'), ['controller' => 'AuthAcl', 'action' => 'index']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Auth Acl'), ['controller' => 'AuthAcl', 'action' => 'add']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Role: <?= h($role->name) ?></h3>
            </div>
    <div class="panel-body">        
    <table width="25%%" class="table table-striped table-bordered table-hover" id="dataTables-example" >
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($role->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($role->id) ?></td>
        </tr>
    </table>
    </div>

    <div class="related">
        <div class="panel-body">
        <h4><?= __('Related Auth Acl') ?></h4>
        <?php if (!empty($role->auth_acl)): ?>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('controller') ?></th>
                <th scope="col"><?= $this->Paginator->sort('actions') ?></th>
                <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($role->auth_acl as $authAcl): ?>
            <tr>
                <td><?= h($authAcl->id) ?></td>
                <td><?= h($authAcl->controller) ?></td>
                <td><?= h($authAcl->actions) ?></td>
                <td><?= h($authAcl->role_id) ?></td>
                <td class="actions">

                    <?= $this->Html->image('view.gif', [ 'alt' => __('View'), 'url' => ['controller' => 'AuthAcl', 'action' => 'view', $authAcl->id]]) ?>

                    <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'), 'url' => ['controller' => 'AuthAcl', 'action' => 'edit', $authAcl->id]]) ?>

                    <?= $this->Form->postlink($this->Html->image('delete.png', [ 'alt' => __('Delete')]), ['controller' => 'AuthAcl', 'action' => 'delete', $authAcl->id], ['confirm' => __('Are you sure you want to delete # {0}?', $authAcl->id), 'escapeTitle' => false]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
        <?php endif; ?>
        </div>
    </div>

    <div class="related">
    <div class="panel-body">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($role->users)): ?>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('username') ?></th>
                            <th><?= $this->Paginator->sort('email') ?></th>
                            <th><?= $this->Paginator->sort('created') ?></th>
                            <th><?= $this->Paginator->sort('modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
            <?php foreach ($role->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">

                <?= $this->Html->image('view.gif', [ 'alt' => __('View'), 'url' => ['controller' => 'Users', 'action' => 'view', $users->id]]) ?>

                <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'), 'url' => ['controller' => 'Users', 'action' => 'edit', $users->id]]) ?>

                <?= $this->Form->postlink( $this->Html->image('delete.png', [ 'alt' => __('Delete')]), ['controller' => 'Users', 'action' => 'delete', $users->id],['confirm' => __('Are you sure you want to delete {0}, {1} ({2})?', $users->lastname, $users->firstname, $users->username), 'escapeTitle' => false]) ?>
                
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
                    <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</form>
<?php $this->Html->addCrumb('Roles', '/roles'); ?>
<?php $this->Html->addCrumb('Detail', ['controller' => 'Roles', 'action' => 'view']); ?>