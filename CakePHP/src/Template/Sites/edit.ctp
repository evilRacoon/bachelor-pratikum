<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $site->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $site->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sites'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="sites form large-10 medium-9 columns">
    <?= $this->Form->create($site) ?>
    <fieldset>
        <legend><?= __('Edit Site') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('imagepath');
            echo $this->Form->input('coordinates');
            echo $this->Form->input('prefix');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php $this->Html->addCrumb('Sites', '/sites'); ?>
<?php $this->Html->addCrumb('Edit', ['controller' => 'Sites', 'action' => 'edit']); ?>