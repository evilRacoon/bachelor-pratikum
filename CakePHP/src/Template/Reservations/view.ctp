<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Reservations') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('Edit Reservation'), ['action' => 'edit', $reservation->id]) ?>
            <li class="btn btn-default"><?= $this->Form->postLink(__('Delete Reservation'), ['action' => 'delete', $reservation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reservation->id)]) ?> </li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('New Reservation'), ['action' => 'add']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Reservation: <?= h($reservation->id) ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Id') ?></th>
                            <td><?= $this->Number->format($reservation->id) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('Visitor') ?></th>
                            <td><?= $this->Html->link($reservation->user->username, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Email') ?></th>
                            <td><?= $this->Html->link($reservation->user->email, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Mobile') ?></th>
                            <td><?= $this->Html->link($reservation->user->mobileNumber, ['controller' => 'Users', 'action' => 'view', $reservation->user->id])?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('VisitedPersonName') ?></th>
                            <td><?= h($reservation->visitedPersonId) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Parking Lot') ?></th>
                            <td><?= $reservation->has('parkinglot') ? h($reservation->parkinglot->description) : '' ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                                <th><?= __('Driveway') ?></th>
                                <td>
                                <h4 id = "loader"> Please wait </h4>
                                <button id="openButton<?php echo $reservation->parkinglot->pid ?>" class="btn btn-default" style="display: none;" onClick="sendcmdOpen(<?php echo $reservation->parkinglot->pid . "," . $reservation->parkinglot->gateway_id ?>)" >open</button>
                                <button id="closeButton<?php echo $reservation->parkinglot->pid ?>" class="btn btn-default" style="display: none;" onClick="sendcmdClose(<?php echo $reservation->parkinglot->pid . "," . $reservation->parkinglot->gateway_id ?>)">close</button>
                                </td>
                        </tr><tr role="row" class="gradeA even">
                            <th><?= __('License Plate No') ?></th>
                            <td><?= h($reservation->licencePlate) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Reservation State') ?></th>
                            <td><?= h($reservation->reservationState) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Reservation Message') ?></th>
                            <td><?= h($reservation->messageInfo) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Is Recurring') ?></th>
                            <td><?= h($reservation->isRecurring ) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Recurring ID') ?></th>
                            <td><?= h($reservation->recurringID ) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('Start') ?></th>
                            <td><?= h($reservation->start) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('End') ?></th>
                            <td><?= h($reservation->end) ?></td>
                        </tr>
                    </thead>
            </table>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<script type="text/javascript">
    $(document).ready(function() {
            MQTTconnect();
            // wait for connection to become stable
            // only possible through the use of callback function onConnect
            setTimeout(function() { 
                sendcmdRefresh(<?= h($reservation->parkinglot->pid) ?>);
            },3000); 
        });
    </script>
<?php $this->Html->addCrumb('Reservations', '/reservations'); ?>
<?php $this->Html->addCrumb('Details', ['controller' => 'Reservations', 'action' => 'view']); ?>