<?php use Cake\Routing\Router; ?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Reservations</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>List Reservations</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo Router::url('/', true); ?>Reservations/index">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">12</div>
                                        <div>List Parking Lots</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo Router::url('/', true); ?>Parkinglots/index">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">124</div>
                                        <div>New Parking Lot</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo Router::url('/', true); ?>Parkinglots/add">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Reservation
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                     <?= $this->Form->create($reservation) ?>
                                        <fieldset>
                                            <legend><?= __('Add Reservation') ?></legend>
                                            <?php
                                                $this->Form->templates([
                                                    'dateWidget' => '{{day}}{{month}}{{year}}<br />{{hour}}{{minute}}{{second}}{{meridian}}'
                                                ]);
                                                ?>
                                                <div class='form-group'>
                                                    <label>Start date and time:</label>
                                                    <?= $this->Form->text('start', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i')]); ?>
                                                </div>

                                                <div class='form-group'>
                                                    <label>End date and time:</label>
                                                     <?= $this->Form->text('end', ['class' => 'dtpicker form-control', 'required' => true, 'default' => date('d.m.Y H:i', strtotime('+3 hour'))]) ;?>
                                                 </div>

                                                <script type="text/javascript">
                                                    $.datetimepicker.setLocale('de');
                                                    $('.dtpicker').datetimepicker({
                                                    inline: false,
                                                    step: 15,
                                                    format:'d.m.Y H:i',
                                                    });
                                                </script>
                                                <?php
                                                echo $this->Form->label('Reservation State:');
                                                echo "<div class='form-group'>".$this->Form->select('reservationState', $state, ['class' => 'form-control'])."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('messageInfo', ['label' => 'Message Info:', 'class' => 'form-control'])."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('visitedPersonId', array('type' => 'select',
                                                                'options' => $visitedId,'class' => 'form-control', 'multiple' => false, 
                                                                'required' => true, 'label' => 'Host'
                                                                ))."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('parkinglot_id', array(['options' => $parkinglots], 'class' => 'form-control'))."</div>";
                                                echo "<div class='form-group'>".$this->Form->input('users_id', array('label' => 'Visitor', 'class' => 'form-control'))."</div>";;
                                                echo "<div class='form-group'>".$this->Form->input('licencePlate', ['label' => 'License Plate:', 'class' => 'form-control'])."</div>";
                                            ?>
                                        </fieldset>
                                        <?= $this->Form->button('Submit',  array('class' => 'btn btn-default')); ?>
                                        <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                                            <?= $this->Form->button($this->Form->postlink(
                                                 __('Delete'), 
                                                 ['action' => 'delete', $reservation->id],
                                                 ['confirm' => __('Are you sure you want to delete # {0}?', $reservation->id)]), array('class' => 'btn btn-default'))?>
                                            <?= $this->Form->end() ?>
                                        <?php endif ?>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>


<?php $this->Html->addCrumb('Reservations', '/reservations'); ?>
<?php $this->Html->addCrumb('Edit', ['controller' => 'Reservations', 'action' => 'edit']); ?>