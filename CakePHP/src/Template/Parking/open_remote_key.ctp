<div style="font-weight:bold;">
	<div style="text-align:center;margin-top:10px;padding-top:10px;">
		<?= $this->Html->image('barrier_open.png', [ 'alt' => __('Barrier open')]); ?>
		<div style="text-align:center;">
			Please stand by while parking lock barrier is opening and pull in.<br />
			Don't forget to close your parking lot when leaving - you have full manual control.!<br />
			Your key is valid till <?= h($remoteKey->valid_until) ?>
			<br />
			Have a nice day!<br />
			<?= $this->Html->link('go back',['controller' => 'Parking', 'action' => 'remoteKey', $remoteKey->token]); ?>
		</div>
	</div>
</div>
<?php $this->Html->addCrumb('Parking', '/parking'); ?>
<?php $this->Html->addCrumb('Open Remote Key', ['controller' => 'Parking', 'action' => 'openremotekey']); ?>