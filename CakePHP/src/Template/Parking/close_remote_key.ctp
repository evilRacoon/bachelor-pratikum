<div style="font-weight:bold;">
	<div style="text-align:center;margin-top:10px;padding-top:10px;">
		<div style="text-align:center;">
			Your parkinglot is blocking.<br />
			Your key is valid till <?= h($remoteKey->valid_until) ?>
			<br />
			See you next time.<br />
            <?= $this->Html->link('go back',['controller' => 'Parking', 'action' => 'remoteKey', $remoteKey->token]); ?>
		</div>
	</div>
</div>
<?php $this->Html->addCrumb('Parking', '/parking'); ?>
<?php $this->Html->addCrumb('Close Remote Key', ['controller' => 'Parking', 'action' => 'closeremotekey']); ?>