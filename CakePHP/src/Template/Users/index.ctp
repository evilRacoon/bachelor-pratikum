<form>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Users') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Reservations'), ['controller' => 'Reservations', 'action' => 'index']) ?> </li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Users List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('username') ?></th>
                            <?php if ($userRoles[0] == "Admin"): ?>
                            <th><?= $this->Paginator->sort('Zone') ?></th>
                            <?php endif; ?>
                            <th><?= $this->Paginator->sort('Role') ?></th>
                            <th><?= $this->Paginator->sort('created') ?></th>
                            <th><?= $this->Paginator->sort('modified') ?></th>
                            <th><?= $this->Paginator->sort('userState') ?></th>
                            <th class="actions"><?= __('Users') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 1; ?>
                        <?php $admin = false ?>
                        <?php if ($userRoles[0] == "Admin"): ?>
                            <?php $admin = true ?>
                        <?php endif; ?>


                        <?php foreach ($users as $user): ?>
                        <?php if ((!$admin)
                                &&
                                (
                                        (($user->userState) == "DELETED")
                                ||
                                        (($user->roles[0]->name) == "Admin")
                                )

                            ) continue ?>
                            <tr role="row" class="gradeA <?php echo ($counter%2 == 0 ?  'odd': 'even'); ?>">
                                <td class="sorting_1"><?= $this->Number->format($user->id) ?></td>
                                <td><?= h($user->username) ?></td>
                                <?php if ($userRoles[0] == "Admin"): ?>
                                    <td><?php foreach($user->zones as $zone) ?>
                                        <?= h($zone->name) ?> 
                                        <?php ;?></td>
                                <?php endif; ?>
                                <td><?php for ($i=0;$i<count($user->roles);$i++): ?>
                                        <?= h($user->roles[$i]->name)?><?php if($i < count($user->roles)-1): ?>,<?php endif;?> 
                                    <?php endfor;?></td>
                                <td><?= h($user->created) ?></td>
                                <td class="center"><?= h($user->modified) ?></td>
                                <td class="center"><?= h($user->userState) ?></td>
                                <td class="actions center" style="text-align: center; ">
                                    <?= $this->Html->image('view.gif', [ 'alt' => __('View'),
                                                                 'url' => ['controller' => 'Users',
                                                                           'action' => 'view', $user->id]]) ?>
                                    <?= $this->Html->image('edit.png', [ 'alt' => __('Edit'),
                                                                 'url' => ['controller' => 'Users',
                                                                           'action' => 'edit', $user->id]
                                                               ]) ?>
                                    <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                                        <?= $this->Form->postlink(
                                                      $this->Html->image('delete.png', [ 'alt' => __('Delete')]),
                                                      ['controller' => 'Users', 'action' => 'delete', $user->id],
                                                      ['confirm' => __('Are you sure you want to delete {0}, {1} ({2})?', $user->lastname, $user->firstname, $user->username), 'escapeTitle' => false]
                                                     ) ?>
                                    <?php endif ?> 
                                </td>
                            </tr>
                            <?php $counter++; ?>
                        <?php endforeach; ?>

                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
        <!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Users', ['controller' => 'Users', 'action' => 'index']); ?>
</form>