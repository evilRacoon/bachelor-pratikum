<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Users') ?></h1>
        <ul class="side-nav">
            <li class="btn btn-default"><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?></li>
            <li class="btn btn-default"><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete {0}, {1} ({2})?', $user->lastname, $user->firstname, $user->username)]) ?> </li>
            <li class="btn btn-default"><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
            <li class="btn btn-default"><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                User: <?= h($user->username) ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('Id') ?></th>
                            <td><?= $this->Number->format($user->id) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('User State') ?></th>
                            <td><?= h($user->userState) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('Title') ?></th>
                            <td><?= h($user->title) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('First Name') ?></th>
                            <td><?= h($user->firstname) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th><?= __('Last Name') ?></th>
                            <td><?= h($user->lastname) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Username') ?></th>
                            <td><?= h($user->username) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Role') ?></th>
                            <td>
                            <?php for ($i=0;$i<count($user->roles);$i++): ?>
                            <?= h($user->roles[$i]->name)?><?php if($i < count($user->roles)-1): ?>,<?php endif;?>         
                            <?php endfor;?>
                            </td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Tenant') ?></th>
                            <td>
                            <?php for ($i=0;$i<count($user->tenants);$i++): ?>
                            <?= h($user->tenants[$i]->name)?><?php if($i < count($user->tenants)-1): ?>,<?php endif;?>         
                            <?php endfor;?>
                            </td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th><?= __('Zones') ?></th>
                            <td>
                            <?php for ($i=0;$i<count($user->zones);$i++): ?>
                            <?= h($user->zones[$i]->name)?><?php if($i < count($user->zones)-1): ?>,<?php endif;?>         
                            <?php endfor;?>
                            </td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th>Created</th>
                            <td><?= h($user->created) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th>Modified</th>
                            <td><?= h($user->modified) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th>Email</th>
                            <td><?= h($user->email) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th>Mobile Number</th>
                            <td><?= h($user->mobileNumber) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th>Landline Number</th>
                            <td><?= h($user->landlineNumber) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th>Gender</th>
                            <td><?= h($user->gender) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th>Company</th>
                            <td><?= h($user->company) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th>Department</th>
                            <td><?= h($user->department) ?></td>
                        </tr>
                        <tr role="row" class="gradeA odd">
                            <th>Street</th>
                            <td><?= h($user->street) ?></td>
                        </tr>
                        <tr role="row" class="gradeA even">
                            <th>Zip</th>
                            <td><?= h($user->ZIP) ?></td>
                        </tr>
                    </thead>
            </table>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('Users', '/users'); ?>
<?php $this->Html->addCrumb('Detail', ['controller' => 'Users', 'action' => 'view']); ?>