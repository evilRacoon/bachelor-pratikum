<?php

$cakeDescription = 'SmartParking@KOM';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('custom-style.css') ?>
    <?= $this->Html->css('/themes/vendor/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('/themes/vendor/metisMenu/metisMenu.min.css') ?>
    <?= $this->Html->css('/themes/dist/css/sb-admin-2.css') ?>
    <?= $this->Html->css('/themes/vendor/font-awesome/css/font-awesome.min.css') ?>

    <?= $this->Html->css('jquery-ui.min.css') ?>
    <?= $this->Html->css('jquery.datetimepicker.css') ?>    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    <?= $this->Html->script('jquery-2.1.4.min.js') ?>
    <?= $this->Html->script('jquery-ui.min.js') ?>

    <?= $this->Html->script('maps.js') ?>

    <?= $this->Html->script('/themes/vendor/jquery/jquery.min.js') ?>
    <?= $this->Html->script('/themes/vendor/bootstrap/js/bootstrap.min.js') ?>
    <?= $this->Html->script('/themes/vendor/metisMenu/metisMenu.min.js') ?>
    <?= $this->Html->script('/themes/dist/js/sb-admin-2.js') ?>
    <?= $this->Html->script('/themes/vendor/jquery/jquery.min.js') ?>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <!-- DataTables JavaScript -->
    <?= $this->Html->script('/themes/vendor/datatables/js/jquery.dataTables.min.js') ?>
    <?= $this->Html->script('/themes/vendor/datatables-plugins/dataTables.bootstrap.min.js') ?>
    <?= $this->Html->script('/themes/vendor/datatables-responsive/dataTables.responsive.js') ?>
    <?= $this->Html->script('jquery.datetimepicker.full.min.js') ?>


    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <!--
    <?= $this->Html->css('cake.css') ?>
    -->
    <?= $this->Html->css('jquery-ui.min.css') ?>
    <?= $this->Html->css('dashboard.css'); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    <?= $this->Html->script('jquery-2.1.4.min.js') ?>
    <?= $this->Html->script('jquery-ui.min.js') ?>
    
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('mqttws31.js') ?>
    <?= $this->Html->script('config.js') ?>


    <script type="text/javascript">
        var mqtt;
        var reconnectTimeout = 2000;
        var mqttIsConnected = false;
    
        function MQTTconnect() {
            if (typeof path == "undefined") {
                //path = '/mqtt';
                path = '';
            }
            mqtt = new Paho.MQTT.Client(
                    host,
                    port,
                    path,
                    "webDB_" + parseInt(Math.random() * 1000, 10)
            );
            var options = {
            timeout: 3,
            useSSL: useTLS,
            cleanSession: cleansession,
            onSuccess: onConnect,
            onFailure: function (message) {
                $('#status').text("Connection failed: " + message.errorMessage + "Retrying");
                setTimeout(MQTTconnect, reconnectTimeout);
                }
            };
        
            mqtt.onConnectionLost = onConnectionLost;
            mqtt.onMessageArrived = onMessageArrived;
        
            if (username != null) {
                options.userName = username;
                options.password = password;
            }
            
            console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
            mqtt.connect(options);
        }
    
        function onConnect() {
            $('#status').text('Connected to ' + host + ':' + port + path);
            // Connection succeeded; subscribe to our topic
            mqtt.subscribe(topic, {qos: 0});
            $('#topic').text(topic);
            mqttIsConnected = true;
            initializeBackgrounds();
        }
    
        function onConnectionLost(responseObject) {
            setTimeout(MQTTconnect, reconnectTimeout);
            $('#status').text("connection lost: " + responseObject.errorMessage + ". Reconnecting");
        };
        
        function sendcmdOpen(nodeId, gwid) {
           sendcmd(nodeId,"open", gwid);
        }
        
        function sendcmdClose(nodeId, gwid) {
           sendcmd(nodeId,"close", gwid);
        }
        
        function sendcmdRefresh(nodeId, gwid) {
           sendcmd(nodeId, "getState", gwid);
        }

        function sendcmdReboot(nodeId, gwid) {
               sendcmd(nodeId,"reboot", gwid);
            }
        
        function sendcmdRecalibrate(nodeId, gwid) {
           sendcmd(nodeId,"recalibrate", gwid);
        }
        
        function sendcmd(nodeId, cmd, gwId) {
           try {
               mqtt.send("parking/"+gwId+"/"+nodeId+"/"+cmd, String(cmd), qos=0);
           } catch(err) {
               alert(err.message);
           }
        }
        
        function onMessageArrived(message) {
            var topic = message.destinationName;
            var splitTopic = topic.split("/");
            var payload = message.payloadString;
            
            $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');
            
            if (splitTopic[2] == "state" ) {
               var divId = "#parkinglotItem" + splitTopic[1];
               var stateId = "#state" + splitTopic[1];
               var errorMessageId = "#errorMessage" + splitTopic[1];
               if (splitTopic[3] == "F") {
                      $(stateId).text("free");
                      $(divId).css("background-color","lightgreen");
                      $(errorMessageId).text(payload);
                      $(stateId).show();
                } else if (splitTopic[3] == "B") {
                  $(stateId).text("blocked");
                  $(divId).css("background-color","rgb(255,102,102)");
                  $(errorMessageId).text(payload);
                  $(stateId).show();
                } else if (splitTopic[3] == "E") {
                  $(stateId).text("Error");
                  $(divId).css("background-color","blue");
                  //$(divId).effect("pulsate", {times: 10}, 2000);
                  $(errorMessageId).text(payload);
                } else if (splitTopic[3] == "P") {
                  $(stateId).text("car present");
                  $(divId).css("background-color","orange");
                  //$(divId).effect("pulsate", {times: 10}, 2000);
                  $(errorMessageId).text(payload);
            } else if (splitTopic[3] == "D") {
          $(stateId).text("freeing");
                  $(divId).css("background-color","yellow");
          $(errorMessageId).text(payload);
        } else if (splitTopic[3] == "U") {
          $(stateId).text("blocking");
                  $(divId).css("background-color","rgb(255,204,255)");
                  $(errorMessageId).text(payload);
               } else {
                    $(stateId).text("unknown");
                    $(divId).css("background-color","lightgrey");
                }
            };       
        }
    
        //$(document).ready(function() {
        //    MQTTconnect();
        //});

    </script>

</head>
<body>
    <header>
      <div class="header-title">
        <!--      <?= $this->Html->image('kom_logo.png', ['url' => ['controller' => 'Dashboard', 'action' => 'index']]); ?>
        <span style="padding-left:5px;font-size:24pt;vertical-align:bottom;">Smart Parking System: <?= $this->fetch('title') ?></span> -->
        </div>
  </header>

    <?php  if ($this->request->here != '/'): ?>
        <div id="wrapper">
        <!-- Navigation -->
            <nav class="navbar navbar-default " role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> 
                    
                    <div class="business-logo-align col-md-3" >
                        <img class="img-responsive" src="/img/tud_logo.png" alt="logo"/> 
                    </div> 
                    <div class="col-md-5">
                        <ul class="nav navbar-nav  navbar-left">
                            <li><h1><?= $this->Html->link('Smart Parking System', 'https://www.smartparking.tu-darmstadt.de/projekt/index.html');?></h1></li>
                        </ul>
                    </div> 

                </div>
                
                <div class="navbar-collapse collapse">
                    
                    <div class="nav navbar-nav navbar-right" style="padding-top: 15px; padding-right: 15px">   
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench"></span>
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><?php
                                if ($authUser) {
                                echo $this->Html->link('<p>Logout <span class="glyphicon glyphicon-log-out"></p></span>',['controller' => 'Users', 'action' => 'logout'], ['escape' => false]);
                                        }
                            ?></li>
                        </ul>
                    </div>
                    

                    <div class="col-md-2 pull-right">
                        <p style="padding-top: 10px">Tenant : <?= h($nameTenant) ?></p>
                        <p>User : <?= $this->request->session()->read('Auth.User.username'); ?></p>
                        <?php if($this->request->session()->check('Auth.User.id')): ?>
                        <p>Role : <?= $this->Html->tableHeaders($currentAuthUserRoles); ?></p>

                        <?php endif; ?>
                    </div>
                </div>
            </nav>
            <div class="bread-crumb">
                <?php echo $this->Html->getCrumbs(' > ', [
                    'text' => '<a href="#"><span class="glyphicon glyphicon-home"></span></a>',
                    'url' => ['controller' => 'Maps', 'action' => 'index', 'home'],
                    'escape' => false
                ]); ?>
            </div>

                    <!-- /.navbar-header -->

                    <div class="navbar-default sidebar" role="navigation">
                        <div class="sidebar-nav navbar-collapse">
                            <ul class="nav" id="side-menu">
                                <li class="sidebar-search">
                                    <div class="input-group custom-search-form">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                    </div>
                                    <!-- /input-group -->
                                </li>
                                <?php if($currentAuthUserRoles[0] == "Admin"): ?>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-map-marker"></span> Map'), ['controller' => 'Maps', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-dashboard"></span> Dashboard'), ['controller' => 'Dashboard', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Reservations'), ['controller' => 'Reservations', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Parking Lots'), ['controller' => 'Parkinglots', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Sensorlogs'), ['controller' => 'Sensorlogs', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-book"></span> Reservation Request'), ['controller' => 'ReservationRequests', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Users'), ['controller' => 'Users', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Roles'), ['controller' => 'Roles', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Authorization ACL'), ['controller' => 'AuthAcl', 'action' => 'index'], ['escape' => false]) ?></li>
                                <?php elseif($currentAuthUserRoles[0] == "Reservation Manager"): ?>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-dashboard"></span> Dashboard'), ['controller' => 'Dashboard', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Reservations'), ['controller' => 'Reservations', 'action' => 'index'], ['escape' => false]) ?></li>
                                    <li> <?= $this->Html->link(('<span class="glyphicon glyphicon-th-list"></span> Users'), ['controller' => 'Users', 'action' => 'index'], ['escape' => false]) ?></li>
                                <?php endif?>
                            </ul>
                        </div>
                        <!-- /.sidebar-collapse -->
                    <!-- /.navbar-static-side -->
                    </div>
                
           <div id="page-wrapper">
    <?php endif; ?>

            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>


    <?php if ($this->request->here != '/'): ?>
            </div>
        </div>
    <?php endif; ?>

        <footer style="background-color:#ffffff;margin-top:-20px;">
        <hr />
        <div style="border:0px solid blue;">
        <?= $this->Html->image('kom_logo.png'); ?>
        <div class="smart-logo">SmartParking TU Darmstadt<br /></div>
        </div>
        </footer>
    </div>
</body>
</html>
