<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= __('Actions') ?></h1>
        <ul class="side-nav">
        <li class="btn btn-default"><?= $this->Html->link(__('Edit Auth Acl'), ['action' => 'edit', $authAcl->id]) ?> </li>
        <li class="btn btn-default"><?= $this->Form->postLink(__('Delete Auth Acl'), ['action' => 'delete', $authAcl->id], ['confirm' => __('Are you sure you want to delete Action->{0}, on Controller->{1} for Role->{2}', $authAcl->actions, $authAcl->controller, $authRoles[0])]) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Auth Acl'), ['action' => 'index']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Auth Acl'), ['action' => 'add']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li class="btn btn-default"><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
    </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3><?= __('ID : '), h($authAcl->id) ?></h3>
            </div>
 <div class="panel-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <trole="row" class="gradeA odd">
            <th scope="row"><?= __('Controller') ?></th>
            <td><?= h($authAcl->controller) ?></td>
        </tr>
        <tr ole="row" class="gradeA even">
            <th scope="row"><?= __('Actions') ?></th>
            <td><?= h($authAcl->actions) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $authAcl->has('role') ? $this->Html->link($authAcl->role->name, ['controller' => 'Roles', 'action' => 'view', $authAcl->role->id]) : '' ?></td>
        </tr>
        <tr ole="row" class="gradeA odd">
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($authAcl->id) ?></td>
        </tr>
    </table>
    </div>
        <!-- /.panel-body -->
</div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<?php $this->Html->addCrumb('AuthAcl', '/authAcl'); ?>
<?php $this->Html->addCrumb('Detail', ['controller' => 'AuthAcl', 'action' => 'view']); ?>