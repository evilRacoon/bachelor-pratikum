<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * AuthAcl Model
 *
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\AuthAcl get($primaryKey, $options = [])
 * @method \App\Model\Entity\AuthAcl newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AuthAcl[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AuthAcl|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthAcl patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AuthAcl[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AuthAcl findOrCreate($search, callable $callback = null, $options = [])
 */
class AuthAclTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('auth_acl');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->requirePresence('actions', 'create')
            ->notEmpty('actions');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }
    public function authRoles(int $authid) {
        $conn = ConnectionManager::get('default');
        
        if (empty($authid)) return false;
        $stmt = "SELECT roles.name
                FROM roles
                WHERE roles.id=".$authid."
                ";
        
        $authroles= array_column($conn->query($stmt)->fetchAll('assoc'),'name');
        return $authroles;
    }
}
