<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DatabaseLogs Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DatabaseLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\DatabaseLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DatabaseLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DatabaseLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DatabaseLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DatabaseLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DatabaseLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DatabaseLogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('database_logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->scalar('context')
            ->allowEmpty('context');

        $validator
            ->scalar('ip')
            ->allowEmpty('ip');

        $validator
            ->scalar('hostname')
            ->allowEmpty('hostname');

        $validator
            ->scalar('uri')
            ->allowEmpty('uri');

        $validator
            ->scalar('refer')
            ->allowEmpty('refer');

        $validator
            ->scalar('user_agent')
            ->allowEmpty('user_agent');

        $validator
            ->integer('count')
            ->requirePresence('count', 'create')
            ->notEmpty('count');

        $validator
            ->scalar('tenant')
            ->allowEmpty('tenant');

        $validator
            ->scalar('action')
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        $validator
            ->scalar('userName')
            ->allowEmpty('userName');

        $validator
            ->scalar('userRole')
            ->allowEmpty('userRole');

        $validator
            ->scalar('oldValue')
            ->allowEmpty('oldValue');

        $validator
            ->scalar('newValue')
            ->allowEmpty('newValue');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
