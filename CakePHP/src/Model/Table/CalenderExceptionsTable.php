<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CalenderExceptions Model
 *
 * @property \App\Model\Table\ResTable|\Cake\ORM\Association\BelongsTo $Res
 *
 * @method \App\Model\Entity\CalenderException get($primaryKey, $options = [])
 * @method \App\Model\Entity\CalenderException newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CalenderException[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CalenderException|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CalenderException patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CalenderException[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CalenderException findOrCreate($search, callable $callback = null, $options = [])
 */
class CalenderExceptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('calender_exceptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Res', [
            'foreignKey' => 'res_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('isScheduled', 'create')
            ->notEmpty('isScheduled');

        $validator
            ->requirePresence('isCancelled', 'create')
            ->notEmpty('isCancelled');

        $validator
            ->dateTime('start')
            ->requirePresence('start', 'create')
            ->notEmpty('start');

        $validator
            ->dateTime('end')
            ->requirePresence('end', 'create')
            ->notEmpty('end');

        $validator
            ->requirePresence('isFullDayReservation', 'create')
            ->notEmpty('isFullDayReservation');

        $validator
            ->dateTime('createdDate')
            ->requirePresence('createdDate', 'create')
            ->notEmpty('createdDate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['res_id'], 'Res'));

        return $rules;
    }
}
