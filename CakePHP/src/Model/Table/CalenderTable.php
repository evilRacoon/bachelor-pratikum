<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Calender Model
 *
 * @method \App\Model\Entity\Calender get($primaryKey, $options = [])
 * @method \App\Model\Entity\Calender newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Calender[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Calender|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calender patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Calender[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Calender findOrCreate($search, callable $callback = null, $options = [])
 */
class CalenderTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('calender');
        $this->setDisplayField('recurringId');
        $this->setPrimaryKey(['recurringId', 'recurringTypeId']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('recurringId')
            ->allowEmpty('recurringId', 'create');

        $validator
            ->integer('recurringTypeId')
            ->allowEmpty('recurringTypeId', 'create');

        $validator
            ->integer('separationCount')
            ->allowEmpty('separationCount');

        $validator
            ->integer('maxNumOfOccurences')
            ->allowEmpty('maxNumOfOccurences');

        $validator
            ->integer('dayOfWeek')
            ->allowEmpty('dayOfWeek');

        $validator
            ->integer('WeekOfMonth')
            ->allowEmpty('WeekOfMonth');

        $validator
            ->integer('DayOfMonth')
            ->allowEmpty('DayOfMonth');

        $validator
            ->integer('MonthOfYear')
            ->allowEmpty('MonthOfYear');

        return $validator;
    }
}
