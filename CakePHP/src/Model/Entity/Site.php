<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Site Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $prefix
 * @property int $locationID
 * @property string $isPublic
 * @property \Cake\I18n\Time $publicStart
 * @property \Cake\I18n\Time $publicEnd
 * @property string $isPublicRecurring
 * @property int $recurringId
 *
 * @property \App\Model\Entity\Gateway[] $gateways
 * @property \App\Model\Entity\Parkinglot[] $parkinglots
 * @property \App\Model\Entity\Zone[] $zones
 */
class Site extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
