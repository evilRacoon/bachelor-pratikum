<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property string $description
 * @property string $type
 * @property float $upperLeftLongitude
 * @property float $upperLeftLatitude
 * @property float $upperRightLongitude
 * @property float $upperRightLatitude
 * @property float $lowerLeftLongitude
 * @property float $lowerLeftLatitude
 * @property float $lowerRightLongitude
 * @property float $lowerRightLatitude
 * @property float $width
 * @property float $length
 * @property string $imagePath
 */
class Location extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
