<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reservation Entity
 *
 * @property int $id
 * @property string $token
 * @property \Cake\I18n\Time $start
 * @property \Cake\I18n\Time $end
 * @property int $parkinglot_id
 * @property int $users_id
 * @property int $visitedPersonId
 * @property string $licencePlate
 * @property string $reservationState
 * @property string $messageInfo
 * @property string $isRecurring
 * @property int $recurringID
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Parkinglot $parkinglot
 * @property \App\Model\Entity\User $user
 */
class Reservation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        //'token'
    ];
}
