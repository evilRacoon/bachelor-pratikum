<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Parkinglot Entity
 *
 * @property int $id
 * @property int $pid
 * @property string $description
 * @property string $mqttchannel
 * @property int $gateway_id
 * @property string $status
 * @property string $ip
 * @property string $hwid
 * @property \Cake\I18n\Time $lastStateChange
 * @property string $type
 * @property int $locationId
 *
 * @property \App\Model\Entity\Gateway $gateway
 * @property \App\Model\Entity\RemoteKey[] $remote_keys
 * @property \App\Model\Entity\Reservation[] $reservations
 * @property \App\Model\Entity\Sensorlog[] $sensorlogs
 * @property \App\Model\Entity\Site[] $sites
 * @property \App\Model\Entity\Zone[] $zones
 */
class Parkinglot extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
