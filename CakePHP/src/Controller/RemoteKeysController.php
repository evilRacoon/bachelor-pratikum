<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Database\Type;
use App\Model\Entity\RemoteKeys;
use Cake\I18n\Time;

// require_once 'SAM/php_sam.php';

/**
 * RemoteKeys Controller
 *
 * @property \App\Model\Table\RemoteKeyTable $RemoteKeys
 */
class RemoteKeysController extends AppController
{

	/**
	 * Index method
	 * List all active remoteKeys whose valid_until has not yet passed
	 *
	 * @return void
	 */
	public function index()
	{
		// get now date in valid format
		$nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
		$this->paginate = [
				'contain' => ['Parkinglots'],
				'conditions' => ['RemoteKeys.valid_until >=' => $nowtime ],
				'order' => [
						'RemoteKeys.valid_from' => 'desc']
		];
		$this->set('remoteKeys', $this->paginate(
				$this->RemoteKeys->find('all')->contain(['Parkinglots'])));
		$this->set('_serialize', ['remoteKeys']);
	}

	/**
	 * history method
	 * List all historic remoteKeys whose valid_until-time has passed
	 *
	 * @return void
	 */
	public function history()
	{
		// get now date in valid format
		$nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
		$this->paginate = [
				'contain' => ['Parkinglots'],
				'conditions' => ['RemoteKeys.valid_until <' => $nowtime ],
				'order' => [
						'RemoteKeys.valid_from' => 'desc']
		];
		$this->set('remoteKeys', $this->paginate($this->RemoteKeys));
		$this->set('_serialize', ['remoteKeys']);
	}

	/**
	 * View method
	 *
	 * @param string|null $id RemoteKey id.
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException if record not found.
	 */
	public function view($id = null)
	{
		$remoteKey = $this->RemoteKeys->get($id, [
				'contain' => ['Parkinglots']
		]);
		$this->set('remoteKey', $remoteKey);
		$this->set('_serialize', ['remoteKey']);
	}

	/**
	 * Add method
	 *
	 * @return void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$remoteKey = $this->RemoteKeys->newEntity();
		if ($this->request->is('post')) {
			$remoteKey = $this->RemoteKeys->patchEntity($remoteKey, $this->request->data);
			if ($this->RemoteKeys->save($remoteKey)) {
				$this->Flash->success(__('The remoteKey has been saved and the visitor was sent an email.'));

				$remoteKey = $this->RemoteKeys->get($remoteKey->id, [
						'contain' => ['Parkinglots']
				]);
				// send remoteKey email including open lock link
				$this->sendRemoteKeyMail($remoteKey);

				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The remoteKey could not be saved. Please, try again.'));
			}
		}
		$parkinglots = $this->RemoteKeys->Parkinglots->find('list', ['limit' => 200]);
		// create unique id to use as token to identify booking
		$token = bin2hex(openssl_random_pseudo_bytes(16));
                // hand over values to view
		$this->set(compact('remoteKey', 'parkinglots','token'));
		$this->set('_serialize', ['remoteKey']);
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id RemoteKey id.
	 * @return void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$remoteKey = $this->RemoteKeys->get($id, [
				'contain' => ['Parkinglots']
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$remoteKey = $this->RemoteKeys->patchEntity($remoteKey, $this->request->data);
			if ($this->RemoteKeys->save($remoteKey)) {
				$this->Flash->success(__('The remoteKey has been saved and the visitor was notified of any changes to his booking.'));
				// get full remoteKey object
				$remoteKey = $this->RemoteKeys->get($remoteKey->id, [
						'contain' => ['Parkinglots']
				]);
				// send remoteKey change notification
				$this->sendRemoteKeyMail($remoteKey, true);
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The remoteKey could not be saved. Please, try again.'));
			}
		}
		$parkinglots = $this->RemoteKeys->Parkinglots->find('list', ['limit' => 200]);
		$this->set(compact('remoteKey', 'parkinglots'));
		$this->set('_serialize', ['remoteKey']);
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id RemoteKey id.
	 * @return void Redirects to index.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['get','post','delete']);
		$remoteKey = $this->RemoteKeys->get($id,['contain' => ['Parkinglots']]);
		if ($this->RemoteKeys->delete($remoteKey)) {
			$this->Flash->success(__('The remoteKey has been deleted.'));
			$this->sendDeleteRemoteKeyMail($remoteKey);
		} else {
			$this->Flash->error(__('The remoteKey could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}

	/**
	 * Resend remoteKey email method
	 *
	 * @param string|nill $id RemoteKey id
	 * @return void Redirects to index
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function resend($id = null)
	{
		$this->request->allowMethod(['get','post']);
		$remoteKey = $this->RemoteKeys->get($id,['contain' => ['Parkinglots']]);
		$this->sendRemoteKeyMail($remoteKey);
		 
		return $this->redirect(['action' => 'index']);
	}

	/**
	 * helper get time function
	 * get Time in ISO8106 format
	 *
	 * @param DateTime $ctime
	 * @return DateTime - time in ISO8106 format string
	 */
	private function getTimeAsISO8106($ctime) {
		$temptime = new \DateTime(substr($ctime,0,8));
		$temptime->setTime((int)substr($ctime,9,10), (int)substr($ctime,12,13));
		return $temptime->format('c');
	}

	/**
	 * helper mail function for add and modify
	 * sends an email containing an URL linking the open portal page to the visitor
	 *
	 * @param RemoteKey $remoteKey (RemoteKey Object)
	 * @param bool $modify - initial mail or mail because booking has changed?
	 */
	private function sendRemoteKeyMail($remoteKey = NULL, $modify = false) {
		if ($remoteKey!=NULL) {
			$MyEmail = new Email('default');
			$MyEmail->from(['smartparking@kom.tu-darmstadt.de' => 'TU Darmstadt Smart Parking System'])->to($remoteKey->mail)
			->subject('TU Darmstadt parking lot remoteKey @ KOM institute');
			if (!$modify) {
				$MyEmail->send('Dear ' .$remoteKey->username .",\n\n"
						."you have been assigned a mobile controlled parkinglot from \n"
						.$remoteKey->valid_from. " until ". $remoteKey->valid_until. "\n\n"

						."Please drive to the car park in Rundeturmstrasse 10 and find "
						."parking lot labeled \"" . $remoteKey->parkinglot->description. "\".\n"
						."Press the following link to open the lock for the assigned parking lot: \n\n"
						."https://smartparking.hornjak.de/parking/remoteKey/".$remoteKey->token."\n\n"
						."Best Regards\n\nKOM Institute\nTU Darmstadt\nFachgebiet Multimedia Kommunikation\nRundeturmstrasse 10\n64283 Darmstadt\nGermany");
			} else {
				$MyEmail->send('Dear ' .$remoteKey->username .",\n\n"
						."your parkinglot remoteKey has changed. Please find the updated information attached.\n"
						."You have been assigned a mobile controlled parkinglot from \n"
						.$remoteKey->valid_from. " until ". $remoteKey->valid_until. "\n\n"

						."Please drive to the car park in Rundeturmstrasse 10 and find "
						."parking lot labeled \"" . $remoteKey->parkinglot->description. "\".\n"
						."Press the following link to open the lock for the assigned parking lot: \n\n"
						."https://smartparking.hornjak.de/parking/remoteKey/".$remoteKey->token."\n\n"
						."Best Regards\n\nKOM Institute\nTU Darmstadt\nFachgebiet Multimedia Kommunikation\nRundeturmstrasse 10\n64283 Darmstadt\nGermany");
			}
		}
	}
	/**
	 * helper function to send delete confirmation mail
	 *
	 * @param RemoteKey $remoteKey
	 * @return void
	 */
	private function sendDeleteRemoteKeyMail($remoteKey = NULL) {
		if ($remoteKey!=NULL) {
			 
			$MyEmail = new Email('default');
			$MyEmail->from(['smartparking@kom.tu-darmstadt.de' => 'TU Darmstadt Smart Parking System'])->to($remoteKey->mail)
			->subject('TU Darmstadt parking lot remoteKey deletion confirmation');
			$MyEmail->send('Dear ' .$remoteKey->username .",\n\n"
					."we are sorry to inform you that your parking lot remoteKey valid from \n"
					.$remoteKey->valid_from. " until ". $remoteKey->valid_until. " has been revoked.\n\n"
					."Best Regards\n\nKOM Institute\nTU Darmstadt\nFachgebiet Multimedia Kommunikation\nRundeturmstrasse 10\n64283 Darmstadt\nGermany");
		}
	}

	 
}
