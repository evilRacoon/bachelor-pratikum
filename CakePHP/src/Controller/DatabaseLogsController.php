<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DatabaseLogs Controller
 *
 * @property \App\Model\Table\DatabaseLogsTable $DatabaseLogs
 *
 * @method \App\Model\Entity\DatabaseLog[] paginate($object = null, array $settings = [])
 */
class DatabaseLogsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
            'order' => [
                'id' => 'desc'
            ]
        ];
        $databaseLogs = $this->paginate($this->DatabaseLogs);

        $this->set(compact('databaseLogs'));
        $this->set('_serialize', ['databaseLogs']);
    }

    /**
     * View method
     *
     * @param string|null $id Database Log id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $databaseLog = $this->DatabaseLogs->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('databaseLog', $databaseLog);
        $this->set('_serialize', ['databaseLog']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $databaseLog = $this->DatabaseLogs->newEntity();
        if ($this->request->is('post')) {
            $databaseLog = $this->DatabaseLogs->patchEntity($databaseLog, $this->request->getData());
            if ($this->DatabaseLogs->save($databaseLog)) {
                $this->Flash->success(__('The database log has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The database log could not be saved. Please, try again.'));
        }
        $users = $this->DatabaseLogs->Users->find('list', ['limit' => 200]);
        $this->set(compact('databaseLog', 'users'));
        $this->set('_serialize', ['databaseLog']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Database Log id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $databaseLog = $this->DatabaseLogs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $databaseLog = $this->DatabaseLogs->patchEntity($databaseLog, $this->request->getData());
            if ($this->DatabaseLogs->save($databaseLog)) {
                $this->Flash->success(__('The database log has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The database log could not be saved. Please, try again.'));
        }
        $users = $this->DatabaseLogs->Users->find('list', ['limit' => 200]);
        $this->set(compact('databaseLog', 'users'));
        $this->set('_serialize', ['databaseLog']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Database Log id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $databaseLog = $this->DatabaseLogs->get($id);
        if ($this->DatabaseLogs->delete($databaseLog)) {
            $this->Flash->success(__('The database log has been deleted.'));
        } else {
            $this->Flash->error(__('The database log could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}