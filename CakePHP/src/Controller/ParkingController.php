<?php
namespace App\Controller;
use App\Controller\AppController;
use App\Controller\ReservationsController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

use App\Model\Table\RolesTable;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;


// php_sam should be in CakePHP/webroot/mqtt/SAM/php_sam.php
require_once(__DIR__.'/../../webroot/mqtt/SAM/php_sam.php');

/**
 *  ParkingController
 *  
 *  Handles all direct request for opening the parking post
 *
 */
class ParkingController extends AppController
{

	/**
	 * portal method
	 * displyed if URL of reservation confirmation mail is opened
	 *
	 * @param string token (from GET)
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function index()
	{
		// extract token from GET parmeters
		$token_entered = $this->request->params['pass'][0];

		

		// check token
		if (!is_null($token_entered)) {
			// get associated reservation
			$query = TableRegistry::get('Reservations')->find();
			$query->where(['token' => $token_entered]);
			$query->contain([/*'Visitors',*/'Parkinglots']);
			$reservation = $query->firstOrFail();
			//print_r($reservation);exit;
		
			// make reservation object available in view
			$this->set(compact('reservation'));
			
			// check if reservation is active and open link should 
			// be displayed
			if ($this->reservationPeriodNowActive($reservation)) {
				$this->set('isActive', true);
			} else {
				$this->set('isActive', false);
			}
		} else {
			throw new NotFoundException('Could not find valid token. 
					Please use the link provided in your reservation mail.');
			
		}
		// set mobile layout for smartphones
        $this->viewBuilder()->setLayout('mobile');
	}
	
	/**
	 * remoteKey portal method
	 * displayed if URL of remoteKey confirmation mail is opened
	 *
	 * @param string token (from GET)
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function remoteKey()
	{
		// extract token from GET parmeters
		$token_entered = $this->request->params['pass'][0];
	
		// check token
		if (!is_null($token_entered)) {
			// get associated reservation
			$query = TableRegistry::get('Reservations')->find();
			$query->where(['token' => $token_entered]);
			$query->contain(['Users','Parkinglots']);
			$remoteKey = $query->firstOrFail();
				
			// make remoteKey object available in view
			//$this->set(compact('remoteKey'));
            $start = $remoteKey->start->i18nFormat('dd.MM.YYYY HH:mm');
            $end = $remoteKey->end->i18nFormat('dd.MM.YYYY HH:mm');
            $this->set('start', $start);
            $this->set('end', $end);
			$this->set('remoteKey', $remoteKey);
			$this->set('_serialize', ['remoteKey','start','end']);
			// check if remoteKey is active and open link should
			// be displayed
			if ($this->remoteKeyPeriodNowActive($remoteKey)) {
				$this->set('isActive', true);
			} else {
				$this->set('isActive', false);
			}
		} else {
			throw new NotFoundException('Could not find valid token.
					Please use the link provided in your remoteKey assignment mail.');
		}
		// set mobile layout for smartphones
		$this->viewBuilder()->setLayout('mobile');
//        $this->layout = ;
	}
	
	/**
	 * open method for reservations
	 * Sends MQTT open message
	 * 
	 * @param string token
	 * @return void
	 */
	//public function open($token)
	public function open($token=NULL) {

		$user = $this->Auth->identify();

		//debug('Token: '.$token);
		//if (!is_null($token)) { //rather use isset
		if(isset($token)){
			// retrieve reservation object
			$query = TableRegistry::get('Reservations')->find();
			$query->where(['token' => $token]);
			//CHANGED
			//$query->contain(['Visitors','Parkinglots']);
			//TO
			$query->contain(['Users','Parkinglots']);
			//

			$reservation = $query->firstOrFail();

			if ($this->reservationPeriodNowActive($reservation)) {
				// ok, we are in reservation time period
				// send	open signal via mqtt to raspberry pi
				
				$this->mqttMessage('open',$reservation->parkinglot->mqttchannel."/");

				$this->set('message', "Success");

				//$this->mqttMessage('open','parking/13/open');
			} else {
				throw new NotFoundException("Please be aware that your reservation is only valid between
						". $reservation->start ." and " . $reservation->end);
			}
			$this->set(compact('reservation'));
		} else {
			throw new NotFoundException('Could not find valid token.
					Please use the link provided in your reservation mail.');
		}
		if ($this->request['_ext'] == 'json'){
			//debug("hier");
			$this->set('_serialize', ['message']);
		}else{
			$this->viewBuilder()->setLayout('mobile');
		}
		// set mobile layout for smartphones
		

		
	}

	/**
	 * close method for reservations
	 * Sends MQTT open message
	 * 
	 * @param string token
	 * @return void
	 */
	//public function open($token)
	public function close($token=NULL) {

		$user = $this->Auth->identify();

		//debug('Token: '.$token);
		//if (!is_null($token)) { //rather use isset
		if(isset($token)){
			// retrieve reservation object
			$query = TableRegistry::get('Reservations')->find();
			$query->where(['token' => $token]);
			//CHANGED
			//$query->contain(['Visitors','Parkinglots']);
			//TO
			$query->contain(['Users','Parkinglots']);
			//

			$reservation = $query->firstOrFail();

			if ($this->reservationPeriodNowActive($reservation)) {
				// ok, we are in reservation time period
				// send	open signal via mqtt to raspberry pi
				
				$this->mqttMessage('close',$reservation->parkinglot->mqttchannel."/");

				$this->set('message', "Success");

				//$this->mqttMessage('open','parking/13/open');
			} else {
				throw new NotFoundException("Please be aware that your reservation is only valid between
						". $reservation->start ." and " . $reservation->end);
			}
			$this->set(compact('reservation'));
		} else {
			throw new NotFoundException('Could not find valid token.
					Please use the link provided in your reservation mail.');
		}
		if ($this->request['_ext'] == 'json'){
			//debug("hier");
			$this->set('_serialize', ['message']);
		}else{
			$this->viewBuilder()->setLayout('mobile');
		}
		// set mobile layout for smartphones
		

		
	}
	
	/**
	 * open method for RemoteKeys
	 * Sends MQTT open message
	 *
	 * @param string token
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function openRemoteKey($token) {
		if (!is_null($token)) {
			// retrieve remoteKey object
			$query = TableRegistry::get('RemoteKeys')->find();
			$query->where(['token' => $token]);
			$query->contain(['Parkinglots']);
			$remoteKey = $query->firstOrFail();
				
			if ($this->remoteKeyPeriodNowActive($remoteKey)) {
				// ok, we are in valid time period
				// send	open signal via mqtt to raspberry pi
				$this->mqttMessage('open',$remoteKey->parkinglot->mqttchannel.'/open');
			} else {
				throw new NotFoundException("Please be aware that your remoteKey is only valid between
						". $remoteKey->valid_from ." and " . $remoteKey->valid_until);
			}
			$this->set(compact('remoteKey'));
		} else {
			throw new NotFoundException('Could not find valid token.
					Please use the link provided in your remoteKey assignment mail.');
		}
		// set mobile layout for smartphones
		$this->viewBuilder()->setLayout('mobile');
	}
	
	/**
	 * close method for RemoteKeys
	 * Sends MQTT close message
	 *
	 * @param string token
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function closeRemoteKey($token) {
		if (!is_null($token)) {
			// retrieve remoteKey object
			$query = TableRegistry::get('RemoteKeys')->find();
			$query->where(['token' => $token]);
			$query->contain(['Parkinglots']);
			$remoteKey = $query->firstOrFail();
	
			if ($this->remoteKeyPeriodNowActive($remoteKey)) {
				// ok, we are in valid time period
				// send	open signal via mqtt to raspberry pi
				$this->mqttMessage('close',$remoteKey->parkinglot->mqttchannel.'/close');
			} else {
				throw new NotFoundException("Please be aware that your remoteKey is only valid between
						". $remoteKey->valid_from ." and " . $remoteKey->valid_until);
			}
			$this->set(compact('remoteKey'));
		} else {
			throw new NotFoundException('Could not find valid token.
					Please use the link provided in your remoteKey assignment mail.');
		}
		// set mobile layout for smartphones
		$this->viewBuilder()->setLayout('mobile');
	}
	
	/**
	 * Helper function
	 * checks if now is between start and end reservation time
	 * 
	 * @param Reservation $reservation (Reservation Object)
	 * @return bool
	 *@codeCoverageIgnore
	 */
	private function reservationPeriodNowActive($reservation) {
		// get now date in valid format
		$nowtime = Time::now('Europe/Berlin');
		
		// check if reservation is current
		if ($nowtime->between($reservation->start, $reservation->end, true))
		{
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Helper function
	 * checks if now is between valid_from and valid_until period
	 *
	 * @param RemoteKey $remoteKey (RemoteKey Object)
	 * @codeCoverageIgnore
	 * @return bool
	 */
	private function remoteKeyPeriodNowActive($remoteKey) {
		// get now date in valid format
		$nowtime = Time::now('Europe/Berlin');
	
		// check if reservation is current
		if ($nowtime->between($remoteKey->start, $remoteKey->end, true))
		{
			return true;
		} else {
			return false;
		}
	}
	

}

?>