<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Database\Type;
use App\Model\Entity\Reservation;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Core\Configure;



use Cake\Http\Client;


// require_once 'SAM/php_sam.php';

/**
 * Reservations Controller
 *
 * @property \App\Model\Table\ReservationsTable $Reservations
 */
class ReservationsController extends AppController
{
	
    /**
     * Index method
     * List all active reservations whose end-time has not yet passed
     *
     * @return void
     */
    public function index()
    {
    	// get now date in valid format
    	$nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
        $userRoles = $this->Reservations->Users->getRoles($this->Auth->user('id'));
        if($userRoles[0] == "Admin") {
            $reservations = $this->Reservations->find('all', [ 
                'contain' => [ 'Users' , 'Parkinglots'],
                'conditions' => ['Reservations.end >=' => $nowtime],
                'order' => [
                'Reservations.start' => 'asc']
                ]);
            $reservations = $this->paginate($reservations);
            $this->set('temp');
            $this->set('reservations', $reservations);
        }else {
            // added ->where(['users_id =' => $this->Auth->user('id')]) to filter for a users reservations
            $reservations = $this->Reservations->find('all', [ 
            'contain' => [ 'Users' => ['Zones'], 'Parkinglots' => ['Locations', 'Sites']],
                'conditions' => ['Reservations.end >=' => $nowtime],
                'order' => [
                'Reservations.start' => 'asc']
                ])->where(['users_id =' => $this->Auth->user('id')])->matching('Users.Zones', function ($q) {
                    return $q->where(['Zones.id' => $this->Reservations->Users->zoneId($this->Auth->user('id'))]);
                    });
            $reservations = $this->paginate($reservations);
            $parkinglots = $this->Reservations->ReservationParkinglotZone($this->Auth->user('id'));
            $this->set('parkinglots', $parkinglots);
            $this->set('temp');
            $this->set('reservations', $reservations);
        }
        
        $this->set(compact('reservations','userRoles'));
        $this->set('_serialize', ['reservations']);
    }
    
    /**
     * history method
     * List all historic reservations whose end-time has passed
     *
     * @return void
     */
    public function history()
    {
    	// get now date in valid format
    	$nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
    	$userRoles = $this->Reservations->Users->getRoles($this->Auth->user('id'));
        if($userRoles[0] == "Admin") {
            $reservations = $this->paginate = [
                'contain' => ['Users', 'Parkinglots'],
                'conditions' => ['Reservations.end <' => $nowtime ],
                'order' => [
                'Reservations.start' => 'desc']
            ];
            $this->set('temp');
        $this->set('reservations', $this->paginate($this->Reservations));
        if ($this->request['_ext'] == 'json')
            $this->set('_serialize', ['reservations']);

        }else {
            $reservations = $this->Reservations->find('all', [ 
            'contain' => [ 'Users' => ['Zones'], 'Parkinglots' => ['Sites'=>'Zones']],
                'conditions' => ['Reservations.end <' => $nowtime],
                'order' => [
                'Reservations.start' => 'desc']
                ])->matching('Users.Zones', function ($q) {
                    return $q->where(['Zones.id' => $this->Reservations->Users->zoneId($this->Auth->user('id'))]);
                    });
            $reservations = $this->paginate($reservations);
            $parkinglots = $this->Reservations->ReservationParkinglotZone($this->Auth->user('id'));
            $this->set('parkinglots', $parkinglots);
            $this->set('reservations', $reservations);
            if ($this->request['_ext'] == 'json')
                $this->set('_serialize', ['reservations']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Reservation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

        $this->viewBuilder()->setLayout('Reservation');
    	$reservation = $this->Reservations->get($id, [
            'contain' => ['Users', 'Parkinglots']
        ]);
        $this->set('reservation', $reservation);
        $this->set('_serialize', 'reservation');
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    	$nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
    	$reservation = $this->Reservations->newEntity();
    	if ($this->request->is('post')) {
    	
    		$startDate = Time::parse($this->request->data['start'])->i18nFormat('yyyy-MM-dd HH:mm:ss');
    		$endDate = Time::parse($this->request->data['end'])->i18nFormat('yyyy-MM-dd HH:mm:ss');
    		//var_dump(date('YYYY-MM-dd HH:mm:ss', $this->request->data['end']));exit;
            $reservation->start = $startDate;
            $reservation->end = $endDate;
    		$conditions = array(
    				'conditions' => array(
    						 array(
    								'OR' => array (
    										array( 'Reservations.start  <=' => $startDate,
    												'Reservations.end  >' => $startDate,
    										),
    										array('Reservations.start  <'   => $endDate,
    												'Reservations.end >=' => $endDate,
    										)
    								),
                                    'Reservations.reservationState' => 'RESERVED',
    								'Reservations.end >=' => $nowtime,
    								'Parkinglots.id' => $this->request->data['parkinglot_id']
    						)));
    	
    		
    		$reservations = $this->Reservations->find('all', $conditions)
    		->contain(['Users', 'Parkinglots']);
    		
    		$count = 0;
    		foreach ($reservations as $reservation) {
    			$count = count($reservation);
    		}
            if($this->request->data['reservationState'] == 'SUPERKEY') {
        			$reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
                    $reservation->start = $startDate;
                    $reservation->end = $endDate;
                    $reservation->created = $nowtime;
        			if ($this->Reservations->save($reservation)) {
        				$this->Flash->success(__('The reservation has been saved with Superkey and the visitor was sent an email.'));
        	
                        /*
                        * This code is not going to work if the database contains less entrys   
                        * than the highest id. Eg: Ids in the database: 1,2,5,12,47 -> new id= 48 but there is no 
                        * reservation at the index 48
        				$reservation = $this->Reservations->get(($reservation->id), [
        						'contain' => ['Users', 'Parkinglots']
        				]);
        	            */

                        $the_new_reservation = $this->Reservations->find()
                                                    ->where(['Reservations.id' => $reservation->id])
                                                    ->contain(['Users', 'Parkinglots'])
                                                    ->first();
                        //debug($the_new_reservation);
                        $reservation = $the_new_reservation;

        				// send reservation email including open lock link
        				//$this->sendReservationMail($reservation);
        	
        				return $this->redirect(['action' => 'index']);
        		} else {
        				$this->Flash->error(__('The reservation could not be saved. Please, try again.'));
        		} 	
    	    } else if($this->request->data['reservationState'] == 'RESERVED') {
                if ($count < 1) {
                    $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
                    $reservation->start = $startDate;
                    $reservation->end = $endDate;
                    $reservation->created = $nowtime;
                    if ($this->Reservations->save($reservation)) {

                        $this->Flash->success(__('The reservation has been saved and the visitor was sent an email.'));
                        
                        /*
                        * This code is not going to work if the database contains less entrys   
                        * than the highest id. Eg: Ids in the database: 1,2,5,12,47 -> new id= 48 but there is no 
                        * reservation at the index 48
                        $reservation = $this->Reservations->get(($reservation->id), [
                                'contain' => ['Users', 'Parkinglots']
                        ]);
                        /*=====================================*/
                        $the_new_reservation = $this->Reservations->find()
                                                    ->where(['Reservations.id' => $reservation->id])
                                                    ->contain(['Users', 'Parkinglots'])
                                                    ->first();
                        //debug($the_new_reservation);
                        $reservation = $the_new_reservation;
                        
                        // send reservation email including open lock link
                        //$this->sendReservationMail($reservation);
                        
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
                    }
                } else {
                    $this->Flash->error(__('The reservation time clashes. Hence, it could not be saved. Please, try again with a different time slot'));
                }
            } else {
                $this->Flash->error(__('Your reservation cannot be saved because you have wrong reservationState.'));
                return $this->redirect(['action' => 'add']);
            }
        }
        $usrRoles = $this->Reservations->Users->getRoles($this->Auth->user('id'));
        foreach ($usrRoles as $userRoles) {
             if($userRoles == "Admin") {
                $visitedId = $this->Reservations->Users->adminVisitor();
                $users = $this->Reservations->Users->adminVisitor();
                $parkinglots = $this->Reservations->AdminParkinglotZone($this->Auth->user('id'));
             }elseif ($userRoles == "Reservation Manager") {
                $visitedId = $this->Reservations->Users->reservationVisitor($this->Auth->user('id'));
                $users = $this->Reservations->Users->reservationVisitor($this->Auth->user('id'));
                $parkinglots = $this->Reservations->ReservationParkinglotZone($this->Auth->user('id'));
            }
        }
    	foreach($parkinglots as $parkinglot) {
    		$parkingHistories = $this->Reservations->find('all')
    		->where(['Parkinglots.description' => $parkinglot,
    				'Reservations.end >=' => $nowtime
    		])
    		->contain(['Users', 'Parkinglots']);
    		break;
    	}
    	$state = array("RESERVED" => "RESERVED", "BOOKED" => "BOOKED", "CANCELED" => "CANCELED", "SUPERKEY" => "SUPERKEY");
    	//$futureReservation =
    	// create unique id to use as token to identify booking
    	$token = bin2hex(openssl_random_pseudo_bytes(16));
    	// hand over values to view
    	$this->set(compact('reservation', 'users', 'parkinglots', 'token','parkingHistories', 'nowtime', 'visitedId', 'state'));
    	$this->set('_serialize', ['reservation','parkingHistories']);
    }

    /**
    * Add Method for API-Requests
    *
    */
    public function add_api(){

        $nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
        $reservation = $this->Reservations->newEntity();

        if ($this->request->is('post')) {

            if(!isset($this->request->getData()['start']) ||
                !isset($this->request->getData()['end']) ||
                !isset($this->request->getData()['parkinglot_id']) ||
                !isset($this->request->getData()['reservationState']) ||
                !isset($this->request->getData()['licencePlate']) ){

                $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Die Reservierung konnte nicht gespeichert werden, weil entscheidende Felder nicht gesetzt wurden. Bitte versuchen Sie es erneut.']))
                                         ->withStatus(400);

                return $response;

            }

        
            $startDate = Time::parse($this->request->getData()['start'])->i18nFormat('yyyy-MM-dd HH:mm:ss');
            $endDate = Time::parse($this->request->getData()['end'])->i18nFormat('yyyy-MM-dd HH:mm:ss');
            //var_dump(date('YYYY-MM-dd HH:mm:ss', $this->request->data['end']));exit;
            $reservation->start = $startDate;
            $reservation->end = $endDate;

            $startDateObject = new Date($startDate);
            $nowTimeObject = new Date($nowtime);
            $reducedDate = $startDateObject->modify('-1 hours'); //Tolerance for past reservations
            

            if($startDate > $endDate){ //Error if starttime is greater than endtime
                $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Die Reservierung kann nicht gespeichert werden, weil die Startzeit nach der Endzeit liegt']))
                                         ->withStatus(400);

                        return $response;

            }else if($reducedDate < $nowTimeObject){ // Error if a user wants to make a reservation in the past

                $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Die Reservierung kann nicht gespeichert werden, weil die Endzeit der Reservierung in der Vergangenheit liegt']))
                                         ->withStatus(400);

                        return $response;
            }


            $conditions = array(
                    'conditions' => array(
                             array(
                                    'OR' => array (
                                            array( 'Reservations.start  <=' => $startDate,
                                                    'Reservations.end  >' => $startDate,
                                            ),
                                            array('Reservations.start  <'   => $endDate,
                                                    'Reservations.end >=' => $endDate,
                                            )
                                    ),
                                    'Reservations.reservationState' => 'RESERVED',
                                    'Reservations.end >=' => $nowtime,
                                    'Parkinglots.id' => $this->request->data['parkinglot_id']
                            )));
        
        
            $reservations = $this->Reservations->find('all',$conditions)
            ->contain(['Users', 'Parkinglots'])->all();
            //debug($reservations);
            $count = count($reservations);
            //debug($count);
            if($this->request->getData()['reservationState'] == 'SUPERKEY') {
                //TODO
            } else if($this->request->getData()['reservationState'] == 'RESERVED') {
                
                if ($count < 1) { //If there is no other reservation inside the requested timeframe
                    $reservation = $this->Reservations->patchEntity($reservation, $this->request->getData());
                    $reservation->start = $startDate;
                    $reservation->end = $endDate;
                    $reservation->created = $nowtime;
                    $reservation->users_id = $this->Auth->user('id');
                    $reservation->visitedPersonId= $this->Auth->user('id');
                    $this->sendFirebaseMessage();
                    // create unique id to use as token to identify booking
                    $token = bin2hex(openssl_random_pseudo_bytes(16));

                    $reservation->token = $token;
                    if ($this->Reservations->save($reservation)) {
            
                        /*
                        * This code is not going to work if the database contains less entrys   
                        * than the highest id. Eg: Ids in the database: 1,2,5,12,47 -> new id= 48 but there is no 
                        * reservation at the index 48
                        $reservation = $this->Reservations->get(($reservation->id), [
                                'contain' => ['Users', 'Parkinglots']
                        ]);
                        /*=====================================*/
                        $the_new_reservation = $this->Reservations->find()
                                                    ->where(['Reservations.id' => $reservation->id])
                                                    ->contain(['Users', 'Parkinglots'])
                                                    ->first();
                        //debug($the_new_reservation);
                        $reservation = $the_new_reservation;
                        $this->sendFirebaseMessage();
                        
                        // send reservation email including open lock link
                        //$this->sendReservationMail($reservation);
                        
                        $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Message' => 'Success']))
                                         ->withStatus(200);

                        return $response;
                    } else {
                        $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Die Reservierung konnte nicht gespeichert werden, bitte versuchen Sie es erneut.']))
                                         ->withStatus(400);

                        return $response;
                    }
                } else {
                    $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Reservierungszeiten ueberschneiden sich!']))
                                         ->withStatus(400);

                    return $response;
                }
            } else {
                $response = $this->response->withType('application/json')
                                         ->withStringBody(json_encode(['Error' => 'Falscher Reservierungsstatus']))
                                         ->withStatus(400);

                return $response;
            }
        }else {
            $response = $this->response->withType('application/json')
                                     ->withStringBody(json_encode(['Error' => 'Only POST-Requests are allowed to this location']))
                                     ->withStatus(400);

            return $response;
        }

        
        
        

     }

    /**
     * Edit method
     *
     * @param string|null $id Reservation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reservation = $this->Reservations->get($id, [
            'contain' => ['Users','Parkinglots']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
            $startDate = Time::parse($this->request->data['start'])->i18nFormat('yyyy-MM-dd HH:mm:ss');
            $endDate = Time::parse($this->request->data['end'])->i18nFormat('yyyy-MM-dd HH:mm:ss');

            $reservation->start = $startDate;
            $reservation->end = $endDate;
            $conditions = array(
                    'conditions' => array(
                            'and' => array(
                                    'OR' => array (
                                            array( 'Reservations.start  <=' => $startDate,
                                                    'Reservations.end  >' => $startDate,
                                            ),
                                            array('Reservations.start  <'   => $endDate,
                                                    'Reservations.end >=' => $endDate,
                                            )
                                    ),
                                    'Reservations.reservationState' => 'RESERVED',
                                    'Reservations.end >=' => $nowtime,
                                    'Parkinglots.id' => $this->request->data['parkinglot_id']
                            )));
        
            
            $reservations = $this->Reservations->find('all', $conditions)
            ->contain(['Users', 'Parkinglots']);
            
            $count = 0;
            foreach ($reservations as $reservation) {
                $count = count($reservation);
            }
            if($this->request->data['reservationState'] == 'SUPERKEY') {
                    $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
                    $reservation->start = $startDate;
                    $reservation->end = $endDate;
                    if ($this->Reservations->save($reservation)) {
                        $this->Flash->success(__('The reservation has been saved with Superkey and the visitor was sent an email.'));
            
                        $reservation = $this->Reservations->get($reservation->id, [
                                'contain' => ['Users', 'Parkinglots']
                        ]);
            
                        // send reservation email including open lock link
                        $this->sendReservationMail($reservation);
            
                        return $this->redirect(['action' => 'view', $id]);
                } else {
                        $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
                }   
            } else if($this->request->data['reservationState'] == 'RESERVED') {
                if ($count < 1) {
                    $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
                    $reservation->start = $startDate;
                    $reservation->end = $endDate;
                    if ($this->Reservations->save($reservation)) {
                        $this->Flash->success(__('The reservation has been saved and the visitor was sent an email.'));
            
                        $reservation = $this->Reservations->get($reservation->id, [
                                'contain' => ['Users', 'Parkinglots']
                        ]);
                        // send reservation email including open lock link
                        $this->sendFirebaseMessage();
                        $this->sendReservationMail($reservation);
            
                        return $this->redirect(['action' => 'view', $id]);
                    } else {
                        $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
                    }
                } else {
                    $this->Flash->error(__('The reservation time clashes. Hence, it could not be saved. Please, try again with a different time slot'));
                }
            } else if($this->request->data['reservationState'] == 'CANCELED') {
                $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
                if ($this->Reservations->save($reservation)) {
                    $this->Flash->success(__('The reservation has been cancelled and saved in the database.'));
        
                    $reservation = $this->Reservations->get($reservation->id, [
                            'contain' => ['Users', 'Parkinglots']
                    ]);
                    return $this->redirect(['action' => 'view', $id]);
                } else {
                    $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
                }
            }
            else {
                $this->Flash->error(__('Your reservation cannot be saved because you have wrong reservationState.'));
                return $this->redirect(['action' => 'add']);
            }

        }
        $usrRoles = $this->Reservations->Users->getRoles($this->Auth->user('id'));
        foreach ($usrRoles as $userRoles) {
             if($userRoles == "Admin") {
                $visitedId = $this->Reservations->Users->adminVisitor();
                $users = $this->Reservations->Users->adminVisitor();
                $parkinglots = $this->Reservations->AdminParkinglotZone($this->Auth->user('id'));
             }elseif ($userRoles == "Reservation Manager") {
                $visitedId = $this->Reservations->Users->reservationVisitor($this->Auth->user('id'));
                $users = $this->Reservations->Users->reservationVisitor($this->Auth->user('id'));
                $parkinglots = $this->Reservations->ReservationParkinglotZone($this->Auth->user('id'));
            }
        }
        $state = array("RESERVED" => "RESERVED", "BOOKED" => "BOOKED", "CANCELED" => "CANCELED", "SUPERKEY" => "SUPERKEY");
        $this->set(compact('reservation', 'users', 'parkinglots','visitedId', 'state'));
        $this->set('_serialize', ['reservation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reservation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
    	$this->request->allowMethod(['get','post','delete']);
        $reservation = $this->Reservations->get($id,['contain' => ['Parkinglots', 'Users']]);
        if ($this->Reservations->delete($reservation)) {
            /**	
            $this->mqttMessage(
                		$reservation->id.";".
                		$reservation->parkinglot->pid.";2015-07-03T10:26:00+00:00;2015-07-03T10:26:00+00:00;3"
                		,$reservation->parkinglot->mqttchannel. "/reservation");
            **/
            $this->Flash->success(__('The reservation has been deleted.'));
            $this->sendDeleteReservationMail($reservation);
            $this->sendFirebaseMessage();
        } else {
            $this->Flash->error(__('The reservation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

   /**
    * Resend reservation email method
    * 
    * @param string|nill $id Reservation id
    * @return void Redirects to index
    * @throws \Cake\Network\Exception\NotFoundException When record not found.
    */
    public function resend($id = null)
    {
    	$this->request->allowMethod(['get','post']);
    	$reservation = $this->Reservations->get($id,['contain' => ['Users', 'Parkinglots']]);
    	$this->sendReservationMail($reservation);
        echo $this->Flash->success('email sent');
    	
    	return $this->redirect(['action' => 'index']);
    }
    
    /**
     * helper get time function
     * get Time in ISO8106 format
     * 
     * @param DateTime $ctime
     * @return DateTime - time in ISO8106 format string
     */
    private function getTimeAsISO8106($ctime) {
    	$temptime = new \DateTime(substr($ctime,0,8));
    	$temptime->setTime((int)substr($ctime,9,10), (int)substr($ctime,12,13));
    	return $temptime->format('c');
    }
    
    /**
     * helper mail function for add and modify
     * sends an email containing an URL linking the open portal page to the visitor
     * 
     * @param Reservation $reservation (Reservation Object)
     * @param bool $modify - initial mail or mail because booking has changed?
     */
    private function sendReservationMail($reservation = NULL, $modify = false) {
    	if ($reservation!=NULL) {
     
	//    	 print_r($reservation);
    
    		if ($reservation->user->gender=='female') 
          {
	    		 $salutation = "Frau " .$reservation->user->firstname. " ". $reservation->user->lastname. ",\n\n";
   	      } elseif ($reservation->user->gender=='male') 
            {
	    		    $salutation = "Herr " .$reservation->user->firstname. " ". $reservation->user->lastname. ",\n\n";
   	        } else 
              {
               $salutation = $reservation->user->username. ",\n\n";
              }
	    	
	    	$MyEmail = new Email('default');
	    	$MyEmail->from(['smartparking@kom.tu-darmstadt.de' => 'TU Darmstadt Smart Parking System'])->to($reservation->user->email)
	    	->subject('SmartParking TU Darmstadt: Ihre Reservierung fuer ' . $reservation->start. ', Parkplatz: ' .  $reservation->parkinglot->description );
	    	if (!$modify) {	    	
	    		$MyEmail->send('Hallo ' .$salutation
	    			."\n" 
            ."fuer Sie wurde ueber das Intelligente Parkplatz-Management der TU Darmstadt der Parkplatz " 
            .$reservation->parkinglot->description. " reserviert\n" 
	    			."\n" 
            ."Ihre Parkzeit beginnt\n"
            ."   am ". $reservation->start  . " und endet \n" 
            ."   am ". $reservation->end. "\n"
            ."\n"
            ."Zum Freigeben des Parkplatzes rufen Sie den folgenden Link auf und klicken danach auf PARKPLATZ FREIGEBEN\n"
            ."\n"
	    			."https://www.smartparking.tu-darmstadt.de/parking/remoteKey/".$reservation->token."\n\n"
	    			."Mit freundlichen Gruessen\n"
            ."Ihr Intelligentes Parkplatz-Management-System der \n"
            ."TU Darmstadt\n"
            ."Fachgebiet Multimedia Kommunikation\n"
            ."Rundeturmstrasse 10\n"
            ."64283 Darmstadt\n"
            ."E-Mail :smartparking@kom.tu-darmstadt.de\n"
            );

	    	} else {
	    		$MyEmail->send('Hallo ' .$salutation
   				  ."Ihre Reservierung wurde geaendert. Hier ihre neuen Reservierungsdaten: \n\n"
	    		  ."\n" 
            ."fuer Sie wurde ueber das Intelligente Parkplatz-Management der TU Darmstadt der Parkplatz " 
            .$reservation->parkinglot->description. " reserviert\n" 
	    			."\n" 
            ."Ihre Parkzeit beginnt\n"
            ."   am ". $reservation->start  . " und endet \n" 
            ."   am ". $reservation->end. "\n"
            ."\n"
            ."Zum Freigeben des Parkplatzes rufen Sie den folgenden Link auf und klicken danach auf PARKPLATZ FREIGEBEN\n"
            ."\n"
	    			."https://www.smartparking.tu-darmstadt.de/parking/remoteKey/".$reservation->token."\n\n"
	    			."Mit freundlichen Gruessen\n"
            ."Ihr Intelligentes Parkplatz-Management-System der \n"
            ."TU Darmstadt\n"
            ."Fachgebiet Multimedia Kommunikation\n"
            ."Rundeturmstrasse 10\n"
            ."64283 Darmstadt\n"
            ."E-Mail: smartparking@kom.tu-darmstadt.de\n"
          );
	    	}
    	}
    }
    /**
     * helper function to send delete confirmation mail
     * 
     * @param Reservation $reservation
     * @return void
     */
    private function sendDeleteReservationMail($reservation = NULL) {
    	if ($reservation!=NULL) {
    	
    		if ($reservation->user->gender=='female') 
          {
	    		 $salutation = "Frau " .$reservation->user->firstname. " ". $reservation->user->lastname. ",\n\n";
   	      } elseif ($reservation->user->gender=='male') 
            {
	    		    $salutation = "Herr " .$reservation->user->firstname. " ". $reservation->user->lastname. ",\n\n";
   	        } else 
              {
               $salutation = $reservation->user->username. ",\n\n";
              }
    	
    		$MyEmail = new Email('default');
    		$MyEmail->from(['smartparking@kom.tu-darmstadt.de' => 'TU Darmstadt Smart Parking System'])->to($reservation->user->email)
    		->subject('SmartParking TU Darmstadt: Absage Ihrer Reservierung fuer ' . $reservation->start. ', Parkplatz: ' .  $reservation->parkinglot->description );
    			$MyEmail->send('Hallo ' .$salutation
		        ."Ihre Reservierung wurde storniert.\n"
	    		  ."\n" 
            ."Ihre Reservierung fuer den Parkplatz" 
            .$reservation->parkinglot->description. "\n" 
	    			."\n" 
            ."   von ". $reservation->start  . "\n" 
            ."   bis ". $reservation->end. "\n"
            ."\n"
            ."wurde leider storniert.\n"
            ."\n"
	    			."Mit freundlichen Gruessen\n"
            ."Ihr Intelligentes Parkplatz-Management-System der \n"
            ."TU Darmstadt\n"
            ."Fachgebiet Multimedia Kommunikation\n"
            ."Rundeturmstrasse 10\n"
            ."64283 Darmstadt\n"
            ."E-Mail: smartparking@kom.tu-darmstadt.de\n"
         );
    	}
    }
    

    /**
    * Responsible for sending Firebase Requests on an update
    *
    */
    public function sendFirebaseMessage(){

        $http = new Client();
        $firebase = TableRegistry::get('firebase');
        $id =$this->Auth->user('id');
        //debug($id);
        $firebase = $firebase->find()->where(['users_id' => $id])->all();
        //debug($firebase);
        foreach ($firebase as $entry) {
            //debug($entry);
            $post_data = [ 'to' => $entry['token'],  'data'=>['user_id' => $id] ];

            //The Auth Key should not be in clear text!
            $post_header = ['headers'=>['Authorization' => Configure::read('Security.firebase_auth_key')]];
            //debug($post_header);
            //debug($post_data);
            // Simple post
            try{
                $response = $http->post('https://fcm.googleapis.com/fcm/send', $post_data, $post_header);
            }catch(Exception $e){
                return;
            }
            //debug($response);
        }
            

            return;
    }
}
