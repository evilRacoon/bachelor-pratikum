<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Sensorlogs Controller
 *
 * @property \App\Model\Table\SensorlogsTable $Sensorlogs
 */
class SensorlogsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Parkinglots'],
            'order' => [
            'Sensorlogs.logtime' => 'desc']
        ];
        $this->set('sensorlogs', $this->paginate($this->Sensorlogs));
        $this->set('_serialize', ['sensorlogs']);
    }
    
    /**
     * View method
     * Lists all statistical sensor data available for a certain parkinglot
     * 
     * @param $parkinglot_id Parkinglot id
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($parkinglot_pid = null) {
    	$query = $this->Sensorlogs->find('all',
    			['order' => ['Sensorlogs.logtime' => 'desc']])
    		->where(['parkinglot_id' => $parkinglot_pid]);
		$allSensorlogsForLot = $query->all();
		
		$this->set('allSensorlogsForLot', $this->paginate($query));
		$this->set('parkinglot_id', $parkinglot_pid);
		$this->set('_serialize', ['allSensorlogsForLot']);
		
    }

    /**
     * Edit method
     *
     * @param string|null $id Sensorlog id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sensorlog = $this->Sensorlogs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sensorlog = $this->Sensorlogs->patchEntity($sensorlog, $this->request->data);
            if ($this->Sensorlogs->save($sensorlog)) {
                $this->Flash->success(__('The sensorlog has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sensorlog could not be saved. Please, try again.'));
            }
        }
        $parkinglots = $this->Sensorlogs->Parkinglots->find('list', ['limit' => 200]);
        $this->set(compact('sensorlog', 'parkinglots'));
        $this->set('_serialize', ['sensorlog']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sensorlog id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sensorlog = $this->Sensorlogs->get($id);
        if ($this->Sensorlogs->delete($sensorlog)) {
            $this->Flash->success(__('The sensorlog has been deleted.'));
        } else {
            $this->Flash->error(__('The sensorlog could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
