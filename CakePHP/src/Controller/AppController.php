<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\App;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
	
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     * with authenitaction plugged in
     *
     * @return void
     */
	public function initialize()
    {
	    $this->loadComponent('Flash');
	    if ($this->request['_ext'] == 'json') {
			$this->loadComponent ( 'Auth', [
					'authenticate' => [
							'Basic' => [
									'fields' => [
											'username' => 'username',
											'password' => 'password'
									],
									'userModel' => 'Users'
							]
					],
					'storage' => 'Memory',
					'unauthorizedRedirect' => false,
			    'authorize' => 'Controller'
			] );
		} else {
			$this->loadComponent('Auth', [
					'loginAction' => [
							'controller' => 'Users',
							'action' => 'login'
					],
					'loginRedirect' => [
							'controller' => 'Users',
							'action' => 'index'
					],
					'logoutRedirect' => [
							'controller' => 'Users',
							'action' => 'login'
					],
					'authorize' => 'Controller'
			]);

		}
        $this->loadComponent('RequestHandler');
    }
       

    /**
    * Method to prevent Brute-Forcing on the Server
    */ 
    public function handleLoginAttempt(){
      //Brute Force Protection
        $username = $this->request->env('PHP_AUTH_USER');
        $allowed_attempts = Configure::read('Security.BFProtectionUnit.allowed_attempts');
        $mins_to_block = Configure::read('Security.BFProtectionUnit.mins_to_block');
        //debug($this->Auth->identify());
        $loginAttempts = TableRegistry::get('failed_login_attempts');

        if($this->request['_ext'] == 'json' && !is_null($username)){

          $nowtime = Time::now();
          //debug('-'.$mins_to_block.' minutes');
          $barrierTime = $nowtime->modify('-'.$mins_to_block.' minutes')->i18nFormat('YYYY-MM-dd HH:mm:ss'); //Tolerance

          $loginAttempts->deleteAll([ 'timestamp <='=> $barrierTime ]);
          $result = $loginAttempts->find()->where(['username' => $username])->all();
          debug(count($result));
          if(count($result) >= $allowed_attempts){
            return false;
          }
        }

        if($this->request['_ext'] == 'json' && !$this->Auth->identify() && !is_null($username)){
          $attempt = $loginAttempts->newEntity();
          

          $attempt->username = $username;
          $attempt->timestamp = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');

          $loginAttempts->save($attempt);

        }

        return true;
        
         ////////////////////////////////////////////// 
    }

    /**
     * Filter event hook method
     */  
      public function beforeFilter(Event $event)
      {
        // Helps to prevent brute forcing on the basic auth login
        if($this->request['_ext'] == 'json' && Configure::read('Security.BFProtectionUnit.activated')){
          if(!$this->handleLoginAttempt()){
            $response = $this->response->withType('application/json')
                                             ->withStringBody(json_encode(['Error' => Configure::read('Security.BFProtectionUnit.error_text')]))
                                             ->withStatus(401);
            return $response;
          }
        }
        

      	// Helps set logout or login link in view
      	$this->set('authUser', $this->Auth->user());
      	// Allows necessary because default deny policy
      	$this->Auth->allow(['display']);
      	switch ($this->name) {
      		case 'Parking':
      			$this->Auth->allow(['index', 'open', 'remoteKey', 
      				'openRemoteKey', 'closeRemoteKey', 'close']);
      		break;
      	}
        if($this->Auth->user('id')) {
        $userTable = TableRegistry::get('Users');
        $currentAuthUserRoles = $userTable->getRoles($this->Auth->user('id'));
        $this->set( compact('currentAuthUserRoles'));
        $tenentUsersTable = TableRegistry::get('Users');
        $queryTenants = $tenentUsersTable->tenantName($this->Auth->user('id'));
        $nameTenant = implode(" ", $queryTenants);
        $this->set('nameTenant', $nameTenant);
        $this->set('_serialize', ['nameTenant']);
        }
      }
      
      public function isAuthorized($user) {
      	$requestedController = $this->request->getParam('controller');
      	$requestedAction = $this->request->getParam('action');
//       	debug("Requested: " . $requestedController ." " . $requestedAction);
      	// Get user object
      	$userTable = TableRegistry::get('Users');
      	$currentAuthUserRoles = $userTable->getRoles($this->Auth->user('id'));
//       	debug($currentAuthUserRoles);
      	
      	// Lookup roles required for requested action
      	// TODO
      	$conn = ConnectionManager::get('default');
      	$stmt = "SELECT roles.name FROM auth_acl LEFT JOIN roles ON roles.id = auth_acl.role_id
				WHERE auth_acl.controller='".$requestedController."'
					AND (auth_acl.actions LIKE '%".$requestedAction."%' OR auth_acl.actions LIKE '*')";
      	$allowedRoles = array_column($conn->query($stmt)->fetchAll('assoc'),'name');
//       	debug($allowedRoles);
      	
      	// Check if user is allowed to access this Controller and this action
      	$rolesSufficientForAccess = array_intersect($currentAuthUserRoles, $allowedRoles);
//       	debug($rolesSufficientForAccess);
      	
      	if (count($rolesSufficientForAccess) > 0) {
//       		debug("Access Granted");
      		return true;
      	}
      	return false;
        $this->set( compact('currentAuthUserRoles'));
      }
      
      /**
      * @codeCoverageIgnore
      */
      public function createLink()
      {
      	if($this->Auth->user('id')!=NULL) {
      		// A user is logged in, display logout link
      		$this->set($logout, true);
      	} else {
      		$this->set($logout, false);
      	}
      }
      
      /**
       * Method stub to send messages to MQTT-broker
       * !!! Configure PORT and HOST here !!!
       * 
       * @param string $message
       * @param string $topic
       * @return void
       */
      public function mqttMessage($message = NULL, $topic) {
      	if (isset($message)) {
      		if(!empty($message)) {
      			$conn = new \SAMConnection();
      			$conn->Connect('mqtt', array(
      					'SAM_HOST' => 'localhost',
      					'SAM_PORT' => 1883));
      			if ($conn) {
      				$msg = new \SAMMessage($message);
      				$conn->send('topic://'. $topic, $msg);
      			}
      		}
      	}
      }
	    
}
