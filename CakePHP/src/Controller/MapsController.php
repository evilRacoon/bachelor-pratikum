<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Email\Email;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;


/**
 * Static content controller
 *
 * This controller will render views from Template/Maps/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class MapsController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function index()
    {
        $this->loadModel('Sites');
        $query = $this->Sites->find('all');
        $allSites = $query->all();

        $this->set('allSites', $allSites);
        $this->set('_serialize', ['allSites']);

    }

    public function getSiteLatLon()
    {
        $siteId = $this->request->query["siteId"];
        $this->loadModel('Sites');
        $querySite = $this->Sites->find('all',
            ['order' => ['Sites.id' => 'desc']])
            ->where(['Sites.id' => $siteId]);
        $site = $querySite->first();

        $this->loadModel('Parkinglots');
        $queryParkinglots = $this->Parkinglots->find('all',
            ['order' => ['Parkinglots.pid' => 'desc']])
            ->where(['Parkinglots.site_id' => $siteId]);
        $parkinglots = $queryParkinglots->all();

        $latLon = array();

        foreach($parkinglots as $parkinglot)
        {
            $latLon['parkingLatLon'][] = [
                ['lat' => $parkinglot->lat1, 'lng' => $parkinglot->lon1],
                ['lat' => $parkinglot->lat2, 'lng' => $parkinglot->lon2],
                ['lat' => $parkinglot->lat3, 'lng' => $parkinglot->lon3],
                ['lat' => $parkinglot->lat4, 'lng' => $parkinglot->lon4]
            ];

            $color = "#FFFFFF";
            switch ($parkinglot->status) {
                case "blocked":
                    $color = "#D2691E";
                    break;
		case "blocking":
		    $color = "#FF00FF";
		    break;
                case "occupied":
                    //$color = "#8B008B";
		    $color = "orange";
                    break;
                case "free":
                    $color = "#008000";
                    break;
		case "freeing":
		    $color = "#FFFF00";
		    break;
            }

            $latLon['details'][] = [
                ['description' => $parkinglot->description, 'status' => $parkinglot->status, 'color' => $color, 'id' => $parkinglot->id]
            ];
        }

        $latLon['siteLatLon'] =
            ['lat' => $site->lat, 'lng' =>  $site->lon]
        ;

        echo json_encode($latLon); exit;
    }

    public function getParkingLot()
    {
        if (isset($this->request->query["parkingId"]) || isset($this->request->query["freeParkingId"]))
        {
            $parkingId = isset($this->request->query["parkingId"])? $this->request->query["parkingId"]: $this->request->query["freeParkingId"];
        }

        $block = isset($this->request->query["block"])? $this->request->query["block"]: NULL;
        $blockParkingId = isset($this->request->query["blockParkingId"])? $this->request->query["blockParkingId"]: NULL;

        $this->loadModel('Parkinglots');

        if (isset($this->request->query["freeParkingId"]))
        {
            $freeparkingId = $this->request->query["freeParkingId"];
            $this->Parkinglots->updateAll(
                array('parkinglots.status' => "free"),
                array(
                    'parkinglots.id' => $freeparkingId
                )
            );
        }

        if ($block && $blockParkingId)
        {
            $blockParkingId = $blockParkingId;
            $this->Parkinglots->updateAll(
                array('parkinglots.status' => $block),
                array(
                    'parkinglots.id' => $blockParkingId
                )
            );
        }

        $queryParkinglot = $this->Parkinglots->find('all')
            ->where(['Parkinglots.id' => $parkingId]);
        $parkinglot = $queryParkinglot->first();
       // print_r($parkinglot);exit;
        $this->set('parkinglot', $parkinglot);
        $this->set('_serialize', ['parkinglot']);
        $this->layout = false;

    }

    public function getReservationsList()
    {
        $reservationParkingId = isset($this->request->query["reservationParkingId"])? $this->request->query["reservationParkingId"]: null;
        $nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
        if($reservationParkingId)
        {
            $this->loadModel('Reservations');
            $queryReservation = $this->Reservations->find('all', [
                'conditions' => ['parkinglot_id' => $reservationParkingId,
                    'Reservations.end >=' => $nowtime,],
                'contain' => ['visitors']
            ]);
            $reservationList = $queryReservation->all();
            $this->set(array('reservationList'=> $reservationList, 'parkingId' => $reservationParkingId));
            $this->set('_serialize', array('reservationList', 'parkingId'));

        }

       // $this->set('reservationList', $reservationParkingId);
        //$this->set('_serialize', ['reservationList']);
        $this->layout = false;
    }


    /**
     * Delete method
     *
     * @param string|null $id Reservation id and $parking id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function reservationDelete()
    {
        $this->request->allowMethod(['get','post','delete']);
        $parkingId = isset($this->request->query["parkingId"]) ? $this->request->query["parkingId"] : null;
        $id = isset($this->request->query["id"]) ? $this->request->query["id"] : null;

        $this->loadModel('Reservations');

        if ($id)
        {
            $reservation = $this->Reservations->get($id,['contain' => ['Parkinglots', 'Visitors']]);

            if ($this->Reservations->delete($reservation)) {
                $this->mqttMessage(
                    $reservation->id.";".
                    $reservation->parkinglot->pid.";2015-07-03T10:26:00+00:00;2015-07-03T10:26:00+00:00;3"
                    ,$reservation->parkinglot->mqttchannel. "/reservation");
                $this->Flash->success('The reservation has been deleted.', array(
                    'key' => 'success'
                ));
                $this->sendDeleteReservationMail($reservation);
            } else {
                $this->Flash->error('The reservation could not be deleted. Please, try again.', array(
                    'key' => 'error'
                ));
            }

        }
        $queryReservation = $this->Reservations->find('all', [
            'conditions' => ['parkinglot_id' => $parkingId],
            'contain' => ['visitors']
        ]);
        $reservationList = $queryReservation->all();
        $this->set(array('reservationList'=> $reservationList, 'parkingId' => $parkingId));
        $this->set('_serialize', array('reservationList', 'parkingId'));

        $this->layout = false;
    }


    /**
     * View method
     *
     * @param string|null $id Reservation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function reservationEdit()
    {
        $parkingId = isset($this->request->query["parkingId"]) ? $this->request->query["id"] : null;
        $id = isset($this->request->query["id"]) ? $this->request->query["id"] : null;
        $this->loadModel('Reservations');

        $reservation = $this->Reservations->get($id, [
            'contain' => ['Visitors','Parkinglots']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
            if ($this->Reservations->save($reservation)) {
               // $this->Flash->success(__('The reservation has been saved and the visitor was notified of any changes to his booking.'));
                $this->Flash->success('The reservation has been saved and the visitor was notified of any changes to his booking.', array(
                    'key' => 'success'
                ));

                $reservation = $this->Reservations->get($reservation->id, [
                    'contain' => ['Visitors', 'Parkinglots']
                ]);
                // send reservation change notification
                $this->sendReservationMail($reservation, true);
                // update message for client
                $this->mqttMessage(
                    $reservation->id.";".
                    $reservation->parkinglot->pid.";".
                    $this->getTimeAsISO8106($reservation->start).";".
                    $this->getTimeAsISO8106($reservation->end).";2"
                    ,$reservation->parkinglot->mqttchannel. "/reservation");
                //return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The reservation could not be saved. Please, try again.', array(
                    'key' => 'error'
                ));
               // $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
            }
        }
        $visitors = $this->Reservations->Visitors->find('list', ['limit' => 200]);
        $parkinglots = $this->Reservations->Parkinglots->find('list', ['limit' => 200]);

        $this->set(compact('reservation', 'visitors', 'parkinglots','lala'));
        $this->set('_serialize', ['reservation']);

        $this->layout = false;
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function reservationAdd()
    {
        $this->loadModel('Reservations');
        $reservation = $this->Reservations->newEntity();
        if ($this->request->is('post')) {
            $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
            if ($this->Reservations->save($reservation)) {
                $this->Flash->success('The reservation has been saved and the visitor was notified of any changes to his booking.', array(
                    'key' => 'success'
                ));

                $reservation = $this->Reservations->get($reservation->id, [
                    'contain' => ['Visitors', 'Parkinglots']
                ]);
                // MQTT- command types to be included as last portion of message
                // 1 - add
                // 2 - modify
                // 3 - delete
                $this->mqttMessage(
                    $reservation->id.";".
                    $reservation->parkinglot->pid.";".
                    $this->getTimeAsISO8106($reservation->start).";".
                    $this->getTimeAsISO8106($reservation->end).";1"
                    ,$reservation->parkinglot->mqttchannel. "/reservation");

                // send reservation email including open lock link
                $this->sendReservationMail($reservation);
            } else {
                $this->Flash->error('The reservation could not be saved. Please, try again.', array(
                    'key' => 'error'
                ));
            }

        }
        $visitors = $this->Reservations->Visitors->find('list', ['limit' => 200]);
        $parkinglots = $this->Reservations->Parkinglots->find('list', ['limit' => 200]);
        // create unique id to use as token to identify booking
        $token = uniqid(); //uuid_create();
        // hand over values to view
        $this->set(compact('reservation', 'visitors', 'parkinglots','token'));
        $this->set('_serialize', ['reservation']);
        $this->layout = false;
    }

    public function getReservationRequestsList()
    {
        $reservationParkingId = isset($this->request->query["reservationParkingId"])? $this->request->query["reservationParkingId"]: null;
        $this->loadModel('ReservationRequests');
        $nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
        $this->paginate = [
            'conditions' => ['ReservationRequests.end >=' => $nowtime,
                'ReservationRequests.status !=' => 'accept'],
            'order' => [
                'ReservationRequests.start' => 'desc']
        ];

        $this->set(array('reservationRequests' => $this->paginate($this->ReservationRequests), 'parkingId' => $reservationParkingId) );
        $this->set('_serialize', ['reservationRequests']);
        $this->layout = false;
    }

    public function reservationRequestAdd()
    {
        $this->loadModel('ReservationRequests');
        $reservationRequest = $this->ReservationRequests->newEntity();
        if ($this->request->is('post')) {
            $reservationRequest = $this->ReservationRequests->patchEntity($reservationRequest, $this->request->data);
            if ($this->ReservationRequests->save($reservationRequest)) {
                $this->Flash->success('The reservation is requested.', array(
                    'key' => 'success'
                ));

              //  print_r($this->Flash->render('success'));exit;
                //return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The reservation could not be requested. Please, try again.', array(
                    'key' => 'error'
                ));
            }
        }

        // hand over values to view
        $this->set(compact('reservationRequest'));
        $this->set('_serialize', ['reservationRequest']);
        $this->layout = false;
    }

    public function reservationRequestDelete()
    {
        $this->request->allowMethod(['get','post','delete']);
        $reservationRequestId = isset($this->request->query["reservationRequestId"])? $this->request->query["reservationRequestId"]: null;
        $parkingId = isset($this->request->query["reservationParkingId"])? $this->request->query["reservationParkingId"]: null;
        $this->loadModel('ReservationRequests');
        if($reservationRequestId)
        {
            $reservationRequest = $this->ReservationRequests->get($reservationRequestId);
            if ($this->ReservationRequests->delete($reservationRequest)) {
                $this->Flash->success('The reservation request has been deleted', array(
                    'key' => 'success'
                ));
            } else {
                $this->Flash->error('The reservation request can not be deleted', array(
                    'key' => 'error'
                ));
            }
        }

        $nowtime = Time::now()->i18nFormat('YYYY-MM-dd HH:mm:ss');
        $this->paginate = [
            'conditions' => ['ReservationRequests.end >=' => $nowtime,
                'ReservationRequests.status !=' => 'accept'],
            'order' => [
                'ReservationRequests.start' => 'desc']
        ];

        $this->set(array('reservationRequests' => $this->paginate($this->ReservationRequests), 'parkingId' => $parkingId) );
        $this->set('_serialize', ['reservationRequests']);
        $this->layout = false;
    }

    /**
     * Edit method
     *
     * @param string|null $id Reservation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function reservationRequestAccept()
    {
        $reservationRequestId = isset($this->request->query["reservationRequestId"])? $this->request->query["reservationRequestId"]: null;
        $parkingId = isset($this->request->query["reservationParkingId"])? $this->request->query["reservationParkingId"]: null;
        if($reservationRequestId) {
            $this->loadModel('ReservationRequests');
            $reservationRequest = $this->ReservationRequests->get($reservationRequestId);
            $loggedInUser = $this->Auth->user();
            $this->loadModel('Reservations');
            $reservation = $this->Reservations->newEntity();
            if ($this->request->is(['patch', 'post', 'put'])) {
                $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);

                $check = $this->ReservationRequests->updateAll(
                    array('status' => "accept"),
                    array('id' => $reservationRequestId)
                );

                if (true) {
                    if ($this->Reservations->save($reservation)) {
                        $this->Flash->success('The reservation request has been saved and the visitor was notified of any changes to his booking.', array(
                            'key' => 'success'
                        ));
                        // get full reservation object
                        $reservation = $this->Reservations->get($reservation->id, [
                            'contain' => ['Visitors', 'Parkinglots']
                        ]);
                        // send reservation change notification
                        $this->sendReservationMail($reservation, true);
                        // update message for client
                        $this->mqttMessage(
                            $reservation->id.";".
                            $reservation->parkinglot->pid.";".
                            $this->getTimeAsISO8106($reservation->start).";".
                            $this->getTimeAsISO8106($reservation->end).";2"
                            ,$reservation->parkinglot->mqttchannel. "/reservation");
                    } else {
                        $this->Flash->error('The reservation could not be accepted. Please, try again.', array(
                            'key' => 'error'
                        ));
                    }
                } else {
                    $this->Flash->error('The reservation could not be accepted. Please, try again.', array(
                        'key' => 'error'
                    ));
                }
            }

            $visitors = $this->Reservations->Visitors->find('list', ['limit' => 200]);
            $parkinglots = $this->Reservations->Parkinglots->find('list', ['limit' => 200]);
            $token = uniqid(); //uuid_create();
            // hand over values to view
            $this->set(compact('reservationRequest', 'loggedInUser', 'parkinglots','token', 'visitors', 'reservation','parkingId'));
            $this->set('_serialize', ['reservation']);
            $this->layout = false;
        }
        //$this->set(array('reservationRequest'=> $reservationRequest, 'loggedInUser' => $loggedInUser, ));
        //$this->set('_serialize', array('reservationRequest', 'loggedInUser'));
    }

    /**
     * helper get time function
     * get Time in ISO8106 format
     *
     * @param DateTime $ctime
     * @return DateTime - time in ISO8106 format string
     */
    private function getTimeAsISO8106($ctime) {
        $temptime = new \DateTime(substr($ctime,0,8));
        $temptime->setTime((int)substr($ctime,9,10), (int)substr($ctime,12,13));
        return $temptime->format('c');
    }

    /**
     * helper mail function for add and modify
     * sends an email containing an URL linking the open portal page to the visitor
     *
     * @param Reservation $reservation (Reservation Object)
     * @param bool $modify - initial mail or mail because booking has changed?
     */
    private function sendReservationMail($reservation = NULL, $modify = false) {
        if ($reservation!=NULL) {

            if ($reservation->visitor->gender=='f') {
                $salutation = "Mrs. " .$reservation->visitor->name. ",\n\n";
            } else {
                $salutation = "Mr. " .$reservation->visitor->name. ",\n\n";
            }

            $MyEmail = new Email('default');
            $MyEmail->from(['smartparking@kom.tu-darmstadt.de' => 'TU Darmstadt Smart Parking System'])->to($reservation->visitor->mail)
                ->subject('TU Darmstadt parking lot reservation @ KOM institute');
            if (!$modify) {
                $MyEmail->send('Dear ' .$salutation
                    ."we have booked a parking lot for your appointment from \n"
                    .$reservation->start. " until ". $reservation->end. "\n\n"

                    ."Please drive to the car park in Rundeturmstrasse 10 and find "
                    ."parking lot labeled \"" . $reservation->parkinglot->description. "\".\n"
                    ."Press the following link to open the lock for the assigned parking lot: \n\n"
                    ."https://smartparking.hornjak.de/parking/index/".$reservation->token."\n\n"
                    ."We're looking forward to welcome you.\n\n"
                    ."Best Regards\n\nKOM Institute\nTU Darmstadt\nFachgebiet Multimedia Kommunikation\nRundeturmstrasse 10\n64283 Darmstadt\nGermany");
            } else {
                $MyEmail->send('Dear ' .$salutation
                    ."your booking has changed. Please find the updated information attached.\n"
                    ."We have booked a parking lot for your appointment from \n"
                    .$reservation->start. " until ". $reservation->end. "\n\n"

                    ."Please drive to the car park in Rundeturmstrasse 10 and find "
                    ."parking lot labeled \"" . $reservation->parkinglot->description. "\".\n"
                    ."Press the following link to open the lock for the assigned parking lot: \n\n"
                    ."https://smartparking.hornjak.de/parking/index/".$reservation->token."\n\n"
                    ."We're looking forward to welcome you.\n\n"
                    ."Best Regards\n\nKOM Institute\nTU Darmstadt\nFachgebiet Multimedia Kommunikation\nRundeturmstrasse 10\n64283 Darmstadt\nGermany");
            }
        }
    }
    /**
     * helper function to send delete confirmation mail
     *
     * @param Reservation $reservation
     * @return void
     */
    private function sendDeleteReservationMail($reservation = NULL) {
        if ($reservation!=NULL) {

            if ($reservation->visitor->gender=='f') {
                $salutation = "Mrs. " .$reservation->visitor->name. ",\n\n";
            } else {
                $salutation = "Mr. " .$reservation->visitor->name. ",\n\n";
            }

            $MyEmail = new Email('default');
            $MyEmail->from(['smartparking@kom.tu-darmstadt.de' => 'TU Darmstadt Smart Parking System'])->to($reservation->visitor->mail)
                ->subject('TU Darmstadt parking lot reservation deletion confirmation');
            $MyEmail->send('Dear ' .$salutation
                ."we are sorry to inform you that your parking lot reservation from \n"
                .$reservation->start. " until ". $reservation->end. " has been cancelled.\n\n"
                ."Please don't hesitate to contact us for a new reservation.\n\n"
                ."Best Regards\n\nKOM Institute\nTU Darmstadt\nFachgebiet Multimedia Kommunikation\nRundeturmstrasse 10\n64283 Darmstadt\nGermany");
        }
    }

}
