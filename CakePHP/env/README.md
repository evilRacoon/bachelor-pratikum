# set environment for smartparking web app

## get files from git
git clone &lt;git&gt;


## configure files, email, etc. for your environment

Just run script sp_install.sh 
- copy configuration file app.php with db credentials, email values,..
- makes composer update
- change permissions for some subdirs
- set links to doku and project homepage

When you execute the file then be aware of the current user: be user smartparking  
 
To run the script:

cd smartparking/env

./sp_install &lt;environment&gt;

example: ./sp_install prod 
 
