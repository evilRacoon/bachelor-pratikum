#!/bin/sh
### BEGIN INIT INFO
# Provides:          smartparking configuration script
# 
# Short-Description: Miscellaneous things to be done after git clone.
# Description:       Some file copy and chage permission rights.
### END INIT INFO

PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin
SP_ROOT=/var/www/smartparking/CakePHP
SRC_ROOT=$SP_ROOT/env
DEST_WEBROOT=$SP_ROOT/webroot
#
# source path depends on envrionment
#
SRC=$SRC_ROOT/$1

do_env () {
        CFG_FILE=config/app.php
	echo "install '$1' environment..."
	#
	# go to application root directory
	#
	echo "change to destination directory: '$SP_ROOT'"
        cd $SP_ROOT

        #
        # copy config file with setings for email, db, error handling 
        #
	echo "copy config file from $SRC to $SP_ROOT/$CFG_FILE..."
        cp $SRC/files/app.php $SP_ROOT/$CFG_FILE

        #
        # update composer
        #
	echo "do composer update..."
        composer update

        #
        # change permissions for cakePHP tmp and log dir
        #
	echo "change permissions for subdirs logs and tmp in $SP_ROOT..."
        chmod -R 777 tmp/ logs/

	#
	# go to webroot directory
	#
	echo "change to webroot directory: '$DEST_WEBROOT'"
        cd $DEST_WEBROOT

        #
        # set links for Dokuwiki and project homepage
        #
	echo "set softlinks for tmp and logs dir for tmp and logs dir..."
        cd $SP_ROOT/webroot
        ln -s /var/www/html/project/ projekt
        ln -s /var/www/html/doku/ doku

	echo "finish setings for '$1' environment..."

}


case "$1" in
  prod)
	do_env $1
	;;
  staging)
	do_env $1
	exit 3
	;;
  test)
	echo "test successfull"
	exit 3
	;;
  stop|status)
	echo "Error: argument '$1' not supported" >&2
	# No-op
	;;
  *)
	echo "Usage: sp_install.sh [prod|staging|test]" >&2
	exit 3
	;;
esac

