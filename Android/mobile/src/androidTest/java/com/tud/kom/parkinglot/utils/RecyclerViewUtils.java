package com.tud.kom.parkinglot.utils;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewAssertion;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

import com.tud.kom.parkinglot.R;

import org.hamcrest.Matcher;

import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class RecyclerViewUtils {

    public static <VH extends ViewHolder> ViewAssertion checkOnItemAtPositionWithId(final int position, final int id, final Matcher<View> matcher) {
        return (view, noViewFoundException) -> {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }
            RecyclerView recyclerView = (RecyclerView) view;

            @SuppressWarnings("unchecked") VH viewHolderForPosition = (VH) recyclerView.findViewHolderForPosition(position);
            View viewAtPosition = viewHolderForPosition.itemView;
            View withId = viewAtPosition.findViewById(R.id.button_delete);
            matcher.matches(withId);
        };
    }

    public static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on a child view with specified id.";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(id);
                v.performClick();
            }
        };
    }

    //Code from https://stackoverflow.com/questions/36399787/how-to-count-recyclerview-items-with-espresso/39446889
    public static ViewAssertion hasNumberOfItems(int numberOfItems) {
        return (view, noViewFoundException) -> {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertThat(adapter.getItemCount(), is(numberOfItems));
        };
    }
}
