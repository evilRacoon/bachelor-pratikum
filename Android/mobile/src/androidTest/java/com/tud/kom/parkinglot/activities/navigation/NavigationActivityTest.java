package com.tud.kom.parkinglot.activities.navigation;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.NavigationViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.SavedPreferences.Filename;
import com.tud.kom.parkinglot.activities.history.HistoryFragment;
import com.tud.kom.parkinglot.activities.overview.OverviewFragment;
import com.tud.kom.parkinglot.network.NetworkTask;
import com.tud.kom.parkinglot.utils.JsonUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

@LargeTest
public class NavigationActivityTest {

    @Rule
    public ActivityTestRule<NavigationActivity> mActivityTestRule = new ActivityTestRule<NavigationActivity>(NavigationActivity.class, true, false) {
        @Override
        protected void beforeActivityLaunched() {
            SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.USERNAME, "smart");
            SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.PASSWORD, "parking");
            super.beforeActivityLaunched();
        }
    };

    private Date mEndTime;

    private MockWebServer mServer;

    private Date mStartTime;

    @Before
    public void setUp() throws Exception {
        mServer = new MockWebServer();
        mServer.start();
        NetworkTask.baseUrl = mServer.url("/").toString();

        setStartEndTimes();

        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservation(getContext(), mStartTime, mEndTime)));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservation(getContext(), mStartTime, mEndTime)));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservation(getContext(), mStartTime, mEndTime)));

        mActivityTestRule.launchActivity(new Intent());
    }

    @After
    public void tearDown() throws Exception {
        mServer.shutdown();
    }

    @Test
    public void testCloseNavigationDrawerOnBackPressed() throws Exception {
        onView(withId(R.id.drawer_layout)).perform(open());

        Thread.sleep(100);
        Espresso.pressBack();
        onView(withId(R.id.drawer_layout)).check(matches(isClosed()));
    }

    @Test
    public void testHistory() throws Exception {
        onView(withId(R.id.drawer_layout)).perform(open());
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_history));

        Thread.sleep(100);
        List<Fragment> fragments = mActivityTestRule.getActivity().getSupportFragmentManager().getFragments();
        Fragment fragment = fragments.get(fragments.size() - 1);
        assertNotNull(fragment);
        assertTrue(fragment instanceof HistoryFragment);
    }

    @Test
    public void testLogout() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200));
        onView(withId(R.id.drawer_layout)).perform(open());
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_logout));

        Thread.sleep(1000);

        assertNull(SavedPreferences.getPreference(InstrumentationRegistry.getTargetContext(), Filename.USERNAME));
        assertNull(SavedPreferences.getPreference(InstrumentationRegistry.getTargetContext(), Filename.PASSWORD));
        assertNull(SavedPreferences.getPreference(InstrumentationRegistry.getTargetContext(), Filename.UID));
    }

    @Test
    public void testOverview() throws Exception {
        onView(withId(R.id.drawer_layout)).perform(open());
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_overview));

        Thread.sleep(100);
        List<Fragment> fragments = mActivityTestRule.getActivity().getSupportFragmentManager().getFragments();
        Fragment fragment = fragments.get(fragments.size() - 1);
        assertNotNull(fragment);
        assertTrue(fragment instanceof OverviewFragment);
    }

    @Test
    public void testOverviewOnStart() {
        List<Fragment> fragments = mActivityTestRule.getActivity().getSupportFragmentManager().getFragments();
        Fragment fragment = fragments.get(fragments.size() - 1);
        assertNotNull(fragment);
        assertTrue(fragment instanceof OverviewFragment);
    }

    private void setStartEndTimes() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        mStartTime = calendar.getTime();
        calendar.add(Calendar.DATE, 2);
        mEndTime = calendar.getTime();
    }

}