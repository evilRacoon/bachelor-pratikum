package com.tud.kom.parkinglot.activities.overview;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.activities.navigation.NavigationActivity;
import com.tud.kom.parkinglot.network.NetworkTask;
import com.tud.kom.parkinglot.utils.JsonUtils;

import org.bouncycastle.util.encoders.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.tud.kom.parkinglot.utils.RecyclerViewMatcher.withRecyclerView;
import static com.tud.kom.parkinglot.utils.RecyclerViewUtils.checkOnItemAtPositionWithId;
import static com.tud.kom.parkinglot.utils.RecyclerViewUtils.clickChildViewWithId;
import static com.tud.kom.parkinglot.utils.RecyclerViewUtils.hasNumberOfItems;
import static com.tud.kom.parkinglot.utils.WithBackgroundColorMatcher.withBackgroundColor;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class OverviewFragmentTest {

    @Rule
    public ActivityTestRule<NavigationActivity> mActivityTestRule = new ActivityTestRule<NavigationActivity>(NavigationActivity.class, true, false) {
        @Override
        protected void beforeActivityLaunched() {

            SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), SavedPreferences.Filename.USERNAME, "smart");
            SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), SavedPreferences.Filename.PASSWORD, "parking");
            super.beforeActivityLaunched();
        }
    };

    private Date mEndTime;

    private MockWebServer mServer;

    private Date mStartTime;

    private String password = "parking";

    private String username = "smart";

    @Before
    public void setUp() throws Exception {
        mServer = new MockWebServer();
        mServer.start();
        NetworkTask.baseUrl = mServer.url("/").toString();

        setStartEndTimes();

        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservations(getContext(), 2, mStartTime, mEndTime, mEndTime, mEndTime)));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservations(getContext(), 2, mStartTime, mEndTime, mEndTime, mEndTime)));
    }

    @After
    public void tearDown() throws Exception {
        mServer.shutdown();
    }

    @Test
    public void testButtonDeleteNotVisibleIfActive() {
        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.list)).check(checkOnItemAtPositionWithId(0, R.id.button_delete, not(isDisplayed())));
    }

    @Test
    public void testButtonNavigate() {
        mActivityTestRule.launchActivity(new Intent());

        //extend
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.button_navigate)));
    }

    @Test
    public void testCloseParkingLot_failure() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200));//open parkinglot
        mServer.enqueue(new MockResponse().setResponseCode(404));//close parkinglot

        mActivityTestRule.launchActivity(new Intent());

        //open parkinglot
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.card)));
        //close parkinglot
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.card)));

        Thread.sleep(1000);//Needed because response from the server needs to be processed before card color changes
        onView(withRecyclerView(R.id.list).atPositionOnView(0, R.id.card)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_open)))));
    }

    @Test
    public void testCloseParkingLot_success() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200));//open parkinglot
        mServer.enqueue(new MockResponse().setResponseCode(200));//close parkinglot

        mActivityTestRule.launchActivity(new Intent());

        //open parkinglot
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.card)));
        //close parkinglot
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.card)));

        Thread.sleep(1000);//Needed because response from the server needs to be processed before card color changes
        onView(withRecyclerView(R.id.list).atPositionOnView(0, R.id.card)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_active)))));
    }

    @Test
    public void testDeleteReservationAbort() {
        mServer.enqueue(new MockResponse().setResponseCode(200));//delte reservation

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.list)).check(hasNumberOfItems(2));

        onView(withId(R.id.list)).perform(actionOnItemAtPosition(1, clickChildViewWithId(R.id.button_delete)));
        onView(withText(R.string.delete_confirmation)).inRoot(isDialog()).check(matches(isDisplayed()));

        onView(withText(R.string.no_string)).perform(click());

        onView(withId(R.id.list)).check(hasNumberOfItems(2));
    }

    @Test
    public void testDeleteReservationConfirm() {
        mServer.enqueue(new MockResponse().setResponseCode(200));//delte reservation

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.list)).check(hasNumberOfItems(2));

        onView(withId(R.id.list)).perform(actionOnItemAtPosition(1, clickChildViewWithId(R.id.button_delete)));
        onView(withText(R.string.delete_confirmation)).inRoot(isDialog()).check(matches(isDisplayed()));

        onView(withText(R.string.yes_string)).perform(click());

        onView(withId(R.id.list)).check(hasNumberOfItems(1));
    }

    @Test
    public void testEmptyList_hintNotShown() {
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody("[]"));
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservations(getContext(), 2, mStartTime, mStartTime, mEndTime, mEndTime)));

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.swipe_layout)).perform(swipeDown());

        onView(withId(R.id.swipe_layout)).perform(swipeDown());

        onView(withId(R.id.empty_view)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testEmptyList_hintShow() {
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody("[]"));

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.swipe_layout)).perform(swipeDown());

        onView(withId(R.id.empty_view)).check(matches(isDisplayed()));
    }

    @Test
    public void testOpenParkingLot_failure() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(404));//open parkinglot

        mActivityTestRule.launchActivity(new Intent());

        //open parkinglot
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.card)));

        Thread.sleep(1000);//Needed because response from the server needs to be processed before card color changes
        onView(withRecyclerView(R.id.list).atPositionOnView(0, R.id.card)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_active)))));
    }

    @Test
    public void testOpenParkingLot_success() throws Exception {
        mServer.enqueue(new MockResponse().setResponseCode(200));//open parkinglot

        mActivityTestRule.launchActivity(new Intent());

        //open parkinglot
        onView(withId(R.id.list)).perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.card)));

        Thread.sleep(1000);//Needed because response from the server needs to be processed before card color changes
        onView(withRecyclerView(R.id.list).atPositionOnView(0, R.id.card)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_open)))));
    }

    @Test
    public void testReservationActive() {
        mActivityTestRule.launchActivity(new Intent());

        onView(withRecyclerView(R.id.list).atPositionOnView(0, R.id.card)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_active)))));
    }

    @Test
    public void testReservationRequestSend() throws Exception {
        mActivityTestRule.launchActivity(new Intent());

        mServer.takeRequest();
        RecordedRequest request = mServer.takeRequest();
        assertEquals("/reservations.json", request.getPath());
        String header = request.getHeader("Authorization").substring(6);
        assertEquals(username + ":" + password, new String(Base64.decode(header), Charset.defaultCharset()));
    }

    @Test
    public void testSwipeToRefresh_failure() {
        mServer.enqueue(new MockResponse().setResponseCode(401));

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.swipe_layout)).perform(swipeDown());

        onView(withText(containsString(getTargetContext().getString(R.string.internal_server_error)))).inRoot(withDecorView(not(mActivityTestRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void testSwipeToRefresh_success() {
        mServer.enqueue(new MockResponse().setResponseCode(200).setBody(JsonUtils.getJsonReservations(getContext(), 2, mStartTime, mStartTime, mEndTime, mEndTime)));

        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.swipe_layout)).perform(swipeDown());

        onView(withRecyclerView(R.id.list).atPosition(0)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_normal)))));
        onView(withRecyclerView(R.id.list).atPosition(1)).check(matches(withBackgroundColor(is(getTargetContext().getResources().getColor(R.color.reservation_normal)))));
    }

    private void setStartEndTimes() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        mStartTime = calendar.getTime();
        calendar.add(Calendar.DATE, 2);
        mEndTime = calendar.getTime();

        System.out.println(mStartTime);
        System.out.println(mEndTime);
    }
}