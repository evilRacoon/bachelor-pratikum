package com.tud.kom.parkinglot.activities.main;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.SavedPreferences.Filename;
import com.tud.kom.parkinglot.activities.login.LoginActivity;
import com.tud.kom.parkinglot.activities.navigation.NavigationActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mMainActivityIntentsTestRule = new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testStartLoginActivity() {
        SavedPreferences.deleteAllPreferences(InstrumentationRegistry.getTargetContext());

        mMainActivityIntentsTestRule.launchActivity(new Intent());

        String loginActivityClassName = LoginActivity.class.getName();
        intended(hasComponent(hasClassName(loginActivityClassName)));
    }

    @Test
    public void testStartOverviewActivity() {
        SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.USERNAME, "smart");
        SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.PASSWORD, "parking");
        SavedPreferences.savePreference(InstrumentationRegistry.getTargetContext(), Filename.UID, "123");

        mMainActivityIntentsTestRule.launchActivity(new Intent());

        String overviewActivityClassName = NavigationActivity.class.getName();
        intended(hasComponent(hasClassName(overviewActivityClassName)));
    }
}