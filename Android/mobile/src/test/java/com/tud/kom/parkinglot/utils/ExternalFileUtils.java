package com.tud.kom.parkinglot.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class ExternalFileUtils {

    public static String getFileFromPath(Object object, String fileName) {
        ClassLoader classLoader = object.getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = new File(resource.getPath());
        String json = "";
        try {
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            StringBuilder builder = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                builder.append(line.trim());
                line = br.readLine();
            }
            json = builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }


}
