package com.tud.kom.parkinglot;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.tud.kom.parkinglot.activities.reservation.ReservationViewModel;
import com.tud.kom.parkinglot.network.Response;
import com.tud.kom.parkinglot.utils.ExternalFileUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Calendar;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ReservationViewModelTest {
    @Rule
    public InstantTaskExecutorRule mRule = new InstantTaskExecutorRule();

    @Mock
    private Observer<String> errorObserver;

    @Mock
    private Observer<Calendar> mEndCalendarObserver;

    private ReservationViewModel mReservationViewModel;

    @Mock
    private Observer<Calendar> mStartCalendarObserver;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mReservationViewModel = new ReservationViewModel("username", "password");
        mReservationViewModel.getError().observeForever(errorObserver);

    }

    @Test
    public void testGetEndCalendar() {
        assertTrue(mReservationViewModel.getEndCalendar() != null);
        assertTrue(mReservationViewModel.getEndCalendar() != null);
    }

    @Test
    public void testGetError() {
        assertNotNull(mReservationViewModel.getError());
    }

    @Test
    public void testGetParkingLots() {
        assertTrue(mReservationViewModel.getParkingLots() != null);
        assertTrue(mReservationViewModel.getParkingLots() != null);
    }

    @Test
    public void testGetReservation() {
        assertTrue(mReservationViewModel.getReservation() != null);
        assertTrue(mReservationViewModel.getReservation() != null);
    }

    @Test
    public void testGetSites() {
        assertTrue(mReservationViewModel.getSites() != null);
        assertTrue(mReservationViewModel.getSites() != null);
    }

    @Test
    public void testGetStartCalendar() {
        assertTrue(mReservationViewModel.getStartCalendar() != null);
        assertTrue(mReservationViewModel.getStartCalendar() != null);
    }

    @Test
    public void testGetZones() {
        assertTrue(mReservationViewModel.getZones() != null);
        assertTrue(mReservationViewModel.getZones() != null);
    }

    @Test
    public void testHandleWebsiteResponse_error401() {
        Response r = new Response(401, "OK", ExternalFileUtils.getFileFromPath(this, "error401.json"));
        mReservationViewModel.handleWebsiteResponse(r);
        verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: " + r.getRespCode());
    }

    @Test
    public void testHandleWebsiteResponse_error403() {
        Response r = new Response(403, "OK", ExternalFileUtils.getFileFromPath(this, "error403.json"));
        mReservationViewModel.handleWebsiteResponse(r);
        verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: " + r.getRespCode());
    }

    @Test
    public void testHandleWebsiteResponse_success() {
        Response r = new Response(200, "OK", ExternalFileUtils.getFileFromPath(this, "reservation.json"));
        mReservationViewModel.handleWebsiteResponse(r);
        verify(errorObserver).onChanged("Reservation saved");
    }

    @Test
    public void testStartCalendar() {
        mReservationViewModel.getStartCalendar().observeForever(mStartCalendarObserver);
        mReservationViewModel.getEndCalendar().observeForever(mEndCalendarObserver);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 1);

        mReservationViewModel.setEndCalendar(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        mReservationViewModel.setStartCalendar(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));

        verify(mStartCalendarObserver, times(2)).onChanged(any());
        verify(mEndCalendarObserver, times(3)).onChanged(any());
    }

    @Test
    public void testHandleWebsiteResponses_Not200S() {
        Response r = new Response(400, "not ok", "");
        mReservationViewModel.onSiteResponse(r);
        verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: 400");
    }

    @Test
    public void testHandleWebsiteResponses_Not200P() {
        Response r = new Response(400, "not ok", "");
        mReservationViewModel.onParkingLotResponse(r);
        verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: 400");
    }

    @Test
    public void testHandleWebsiteResponses_Not200Z() {
        Response r = new Response(400, "not ok", "");
        mReservationViewModel.onZoneResponse(r);
        verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: 400");
    }
}