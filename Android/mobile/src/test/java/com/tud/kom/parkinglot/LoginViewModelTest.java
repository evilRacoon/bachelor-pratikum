package com.tud.kom.parkinglot;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.tud.kom.parkinglot.activities.login.LoginViewModel;
import com.tud.kom.parkinglot.network.Response;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class LoginViewModelTest {
    @Rule
    public InstantTaskExecutorRule mRule = new InstantTaskExecutorRule();
    private LoginViewModel mLoginViewModel;
    @Mock
    private Observer<String> errorObserver;
    @Mock
    private Observer<Boolean> buttonObserver;
    @Mock
    private Observer<Boolean> waitForNetworkObserver;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        mLoginViewModel = new LoginViewModel();
        mLoginViewModel.getError().observeForever(errorObserver);
        mLoginViewModel.getWaitingForNetwork().observeForever(waitForNetworkObserver);
    }

    @Test
    public void testLogin_error() {
        Response r = new Response(-1, null, null);
        mLoginViewModel.handleWebsiteResponse(r);
        Mockito.verify(errorObserver).onChanged("ErrorRespCodeNotAsExpected: " + r);
    }

    @Test
    public void testLogin_authorized() {
        mLoginViewModel.handleWebsiteResponse(new Response(200, "OK", "{\"id\":57}"));
        Mockito.verify(errorObserver).onChanged("57");
    }

    @Test
    public void testLogin_unauthorized() {
        mLoginViewModel.handleWebsiteResponse(new Response(401, "OK", null));
        Mockito.verify(errorObserver).onChanged(LoginViewModel.UNAUTHORIZED);
    }

    @Test
    public void testLogin_valid() {
       /* Request request = mLoginViewModel.login( "username", "password");
        Assert.assertNotNull(request);
        Assert.assertEquals("Users/getUser.json", request.formattedUrlExtWithParams());
        Assert.assertEquals(Request.RequestType.GET, request.getRequestType());
        Assert.assertNull(request.formattedBodyParams());
        Assert.assertNotNull(request.getHeaderParams());
        Assert.assertEquals(1, request.getHeaderParams().size());
        Assert.assertEquals("Authorization", request.getHeaderParams().get(0).getFirst());
        Assert.assertEquals("Basic dXNlcm5hbWU6cGFzc3dvcmQ=", request.getHeaderParams().get(0).getSecond());*/
    }

    @Test
    public void testLogin_invalid() {
    /*    Assert.assertNull(mLoginViewModel.login( "", "11111"));
        Mockito.verify(errorObserver).onChanged(LoginViewModel.EMPTY_OR_INVALID_USERNAME);
        Assert.assertNull(mLoginViewModel.login( "", ""));
        Mockito.verify(errorObserver, times(2)).onChanged(LoginViewModel.EMPTY_OR_INVALID_USERNAME);
        Assert.assertNull(mLoginViewModel.login( "11111", ""));
        Mockito.verify(errorObserver).onChanged(LoginViewModel.EMPTY_OR_INVALID_PWD);*/
    }
}