package com.tud.kom.parkinglot;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * save a preference in the private android storage
 */
public abstract class SavedPreferences {
    /**
     * save the preference the a file
     *
     * @param c   context
     * @param key key = filename
     */
    public static void savePreference(Context c, Filename key, String value) {
        try {
            FileOutputStream fos = c.openFileOutput(key.name(), Context.MODE_PRIVATE);
            fos.write(value.getBytes(StandardCharsets.UTF_8));
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * get the preference out of the file
     *
     * @param c   context
     * @param key key = filename
     * @return preference. null, if it does not exist or on io exception
     */
    public static String getPreference(Context c, Filename key) {
        try {
            FileInputStream fis = c.openFileInput(key.name());
            byte[] reader = new byte[fis.available()];
            int bytesRead = fis.read(reader);
            fis.close();
            if (bytesRead != -1)
                return new String(reader, StandardCharsets.UTF_8);
        } catch (IOException ignored) {
        }
        return null;
    }

    /**
     * deletes the preference file
     *
     * @param c   context
     * @param key key = filename
     */
    private static void deletePreference(Context c, Filename key) {
        File dir = c.getFilesDir();
        File file = new File(dir, key.name());
        if (file.exists())
            file.delete();
    }

    /**
     * local logout
     *
     * @param c context
     */
    public static void deleteAllPreferences(Context c) {
        for (Filename file : Filename.values())
            deletePreference(c, file);
    }

    public enum Filename {
        USERNAME, PASSWORD, UID
    }
}
