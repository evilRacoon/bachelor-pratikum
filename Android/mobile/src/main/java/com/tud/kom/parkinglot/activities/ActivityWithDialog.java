package com.tud.kom.parkinglot.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * provides a method for showing a info dialog
 */
public abstract class ActivityWithDialog extends AppCompatActivity {
    private static final String TAG = ActivityWithDialog.class.getSimpleName();

    /**
     * shows an info dialog
     *
     * @param message         the info message
     * @param onClickListener what happens after the user clicks on "ok"
     */
    public void showError(final String message, final DialogInterface.OnClickListener onClickListener) {
        Log.d(TAG, "Msg: " + message);
        runOnUiThread(() -> new AlertDialog.Builder(this).setTitle("Info").setMessage(message).setIcon(android.R.drawable.ic_dialog_alert).setCancelable(false).setNeutralButton(android.R.string.ok, onClickListener).show());
    }
}
