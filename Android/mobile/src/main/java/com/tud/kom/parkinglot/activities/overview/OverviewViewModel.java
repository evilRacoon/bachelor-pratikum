package com.tud.kom.parkinglot.activities.overview;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.tud.kom.parkinglot.base.BaseViewModel;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.parser.ReservationParser;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.network.Response;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

/**
 * The model for the Overview-View.
 * Key tasks are to store username and password,
 * to handle requests and responses to open and close parkinglots
 * and to make space to store a list of reservations to be displayed
 */
public class OverviewViewModel extends BaseViewModel<Reservation> {

    public static final String ERROR_CLOSE_PARKING_LOT = "Could not close parking lot!";
    public static final String ERROR_OPEN_PARKING_LOT = "Could not open parking lot!";
    public static final String ERROR_DELETING_RESERVATION = "Could not delete parking lot!";

    private static final String TAG = OverviewViewModel.class.getSimpleName();


    private MutableLiveData<Reservation> mDeleted;


    private MutableLiveData<Reservation> mReservation;

    /**
     * Constructor for an OverviewViewModel
     *
     * @param username The username used for network connections to the backend
     * @param password The password used for network connections to the backend
     */
    public OverviewViewModel(String username, String password) {
        super(username, password);
    }

    public LiveData<Reservation> getDeleted() {
        if (mDeleted == null) {
            mDeleted = new MutableLiveData<>();
        }
        return mDeleted;
    }

    public MutableLiveData<Reservation> getReservation() {
        if (mReservation == null) {
            mReservation = new MutableLiveData<>();
        }
        return mReservation;
    }

    public void listItemClicked(final Reservation reservation) {
        if (reservation.getParkingLot().isOpen()) {
            closeParkingLot(reservation);
            return;
        }

        if (reservation.isActive()) {
            openParkingLot(reservation);
        }
    }

    protected void deleteReservation(Reservation reservation) {
        RequestAbstraction.deleteReservations(response -> handleDeleteReservationResponse(response, reservation), super.mUsername, super.mPassword, reservation.getId());
    }

    @Override
    protected void makeNetworkCall(final String username, final String password, final NetworkCallback callback) {
        RequestAbstraction.getReservations(callback, username, password);
    }

    @NonNull
    @Override
    protected List<Reservation> parseNetworkResponse(final Response response) throws JSONException, ParseException {
        return ReservationParser.parseReservationsArray(response.getResp());
    }

    /**
     * Sends a Request to close a parking lot
     */
    private void closeParkingLot(final Reservation reservation) {
        RequestAbstraction.closeParkingLot(reservation, response -> handleCloseParkingLotResponse(response, reservation));
    }

    /**
     * Method used to handle the response to a request to close the parking lot.
     * Especially responsible to set an error if there was one (HTTPCODE != 200)
     *
     * @param response The response to be handled
     */
    private void handleCloseParkingLotResponse(final Response response, final Reservation reservation) {
        if (response.getRespCode() != 200) {
            mError.setValue(ERROR_CLOSE_PARKING_LOT);
            return;
        }

        reservation.getParkingLot().setOpen(false);
        mReservation.setValue(reservation);
    }

    private void handleDeleteReservationResponse(final Response response, final Reservation reservation) {
        if (response.getRespCode() != 200) {
            mError.setValue(ERROR_DELETING_RESERVATION);
            return;
        }

        mDeleted.setValue(reservation);
    }

    /**
     * Method used to handle the response to a request to close the parking lot.
     * Especially responsible to set an error if there was one (HTTPCODE != 200)
     *
     * @param response The response to be handled
     */
    private void handleOpenParkingLotResponse(final Response response, final Reservation reservation) {
        if (response.getRespCode() != 200) {
            mError.setValue(ERROR_OPEN_PARKING_LOT);
            return;
        }

        reservation.getParkingLot().setOpen(true);
        mReservation.setValue(reservation);
    }

    /**
     * Sends a Request to open a parking lot
     */
    private void openParkingLot(Reservation reservation) {
        RequestAbstraction.openParkingLot(reservation, response -> handleOpenParkingLotResponse(response, reservation));
    }
}