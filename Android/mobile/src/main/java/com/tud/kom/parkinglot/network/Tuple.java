package com.tud.kom.parkinglot.network;

/**
 * a tuple
 *
 * @param <T> type of first element
 * @param <U> type of second element
 */
public class Tuple<T, U> {
    private T first;
    private U second;

    /**
     * @param first  first element
     * @param second second element
     */
    public Tuple(T first, U second) {
        this.first = first;
        this.second = second;
    }

    /**
     * @return first element
     */
    public T getFirst() {
        return this.first;
    }

    /**
     * @return second element
     */
    public U getSecond() {
        return this.second;
    }

    @Override
    public String toString() {
        return "Tuple{" + "first=" + this.first + ", second=" + this.second + '}';
    }
}