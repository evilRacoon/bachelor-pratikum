package com.tud.kom.parkinglot.network;

/**
 * A network response containing response code and message and the json string
 */
public class Response {
    private int respCode;
    private String respMess;
    private String resp;

    /**
     * instantiate a response
     *
     * @param respCode http code
     * @param respMess response message
     * @param resp     the actual (json) reply
     */
    public Response(int respCode, String respMess, String resp) {
        this.respCode = respCode;
        this.respMess = respMess;
        this.resp = resp;
    }

    /**
     * @return http code
     */
    public int getRespCode() {
        return respCode;
    }

    /**
     * @return response message
     */
    public String getRespMess() {
        return respMess;
    }

    /**
     * @return (json) reply
     */
    public String getResp() {
        return resp;
    }

    @Override
    public String toString() {
        return "Response{" + "respCode=" + respCode + ", respMess='" + respMess + '\'' + ", resp='" + resp + '\'' + '}';
    }
}
