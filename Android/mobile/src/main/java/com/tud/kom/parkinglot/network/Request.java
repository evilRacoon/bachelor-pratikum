package com.tud.kom.parkinglot.network;

import android.util.Base64;
import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Models a request to a server
 */
public class Request {
    private static final String TAG = Request.class.getSimpleName();
    private String urlBaseExtension;
    private RequestType requestType;
    private ArrayList<Tuple<String, String>> bodyParams;
    private ArrayList<Tuple<String, String>> urlParams;
    private ArrayList<Tuple<String, String>> headerParams;

    /**
     * a new request with a urlbaseextension and a requesttype
     *
     * @param urlBaseExtension extension to url (without url parameters)
     * @param requestType      type of request
     */
    public Request(String urlBaseExtension, RequestType requestType) {
        this.urlBaseExtension = urlBaseExtension;
        this.requestType = requestType;
        this.bodyParams = new ArrayList<>();
        this.urlParams = new ArrayList<>();
        this.headerParams = new ArrayList<>();
    }

    /**
     * get the base64 encoded value of username:password
     *
     * @return the base64 encoded value of username:password
     */
    public static String getBase64EncodedAuth(String username, String password) {
        return "Basic " + new String(Base64.encode((username + ":" + password).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP), StandardCharsets.UTF_8);
    }

    /**
     * add body parameters. only if post request
     *
     * @param key   key
     * @param value value
     * @return this
     */
    public Request addBodyParam(String key, String value) {
        if (this.requestType == RequestType.POST)
            this.bodyParams.add(new Tuple(key, value));
        else
            Log.e(TAG, "Tried to add BodyParam to RequestType " + this.requestType);
        return this;
    }

    /**
     * add url parameters
     *
     * @param key   key
     * @param value value
     * @return this
     */
    public Request addUrlParam(String key, String value) {
        this.urlParams.add(new Tuple(key, value));
        return this;
    }

    /**
     * add header parameters
     *
     * @param key   key
     * @param value value
     * @return this
     */
    public Request addHeaderParam(String key, String value) {
        this.headerParams.add(new Tuple(key, value));
        return this;
    }

    @Override
    public String toString() {
        return "Request{" + "urlBaseExtension='" + this.urlBaseExtension + '\'' + ", requestType=" + this.requestType + ", bodyParams=" + this.formattedBodyParams() + ", urlParams=" + this.formattedUrlExtWithParams() + ", headerParams=" + this.headerParams + '}';
    }

    /**
     * @return urlbaseextension and all url parameters formatted properly
     */
    public String formattedUrlExtWithParams() {
        StringBuilder fullUrl = new StringBuilder(this.urlBaseExtension);
        for (int index = 0; index < this.getUrlParams().size(); index++) {
            if (index == 0)
                fullUrl.append("?");
            else
                fullUrl.append("&");
            fullUrl.append(this.getUrlParams().get(index).getFirst()).append("=").append(this.getUrlParams().get(index).getSecond());
        }
        return fullUrl.toString();
    }

    /**
     * @return all body parameters formatted properly
     */
    public String formattedBodyParams() {
        if (this.getBodyParams().size() == 0)
            return null;
        StringBuilder params = new StringBuilder();
        for (int index = 0; index < this.getBodyParams().size(); index++) {
            if (index != 0)
                params.append("&");
            params.append(this.getBodyParams().get(index).getFirst()).append("=").append(this.getBodyParams().get(index).getSecond());
        }
        return params.toString();
    }

    /**
     * @return urlBaseExtension
     */
    public String getUrlBaseExtension() {
        return this.urlBaseExtension;
    }

    /**
     * @return requestType
     */
    public RequestType getRequestType() {
        return this.requestType;
    }

    /**
     * @return bodyParams
     */
    public ArrayList<Tuple<String, String>> getBodyParams() {
        return this.bodyParams;
    }

    /**
     * @return urlParams
     */
    public ArrayList<Tuple<String, String>> getUrlParams() {
        return this.urlParams;
    }

    /**
     * @return headerParams
     */
    public ArrayList<Tuple<String, String>> getHeaderParams() {
        return this.headerParams;
    }

    public enum RequestType {POST, GET, DELETE, PUT}
}
