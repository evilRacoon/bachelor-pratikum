package com.tud.kom.parkinglot.activities.login;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v13.app.ActivityCompat;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.activities.ActivityWithDialog;
import com.tud.kom.parkinglot.activities.navigation.NavigationActivity;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.Response;
import com.tud.kom.parkinglot.notification.LocationServiceHelper;

/**
 * activity for user login
 */
public class LoginActivity extends ActivityWithDialog implements NetworkCallback {

    public static final int REQUEST_CODE_BE_NICE = 0;

    public static final int REQUEST_CODE_BE_AGGRESSIVE = 1;

    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText mNameView;

    private EditText mPasswordView;

    private ProgressDialog mProgressDialog;

    private Button mSignInButton;

    private LoginViewModel mViewModel;

    /**
     * prepares the activity. if username and password are stored, redirect to next activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        askForLocationPermissionAndInitAfterwards();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length >= 1) {
            if (requestCode == REQUEST_CODE_BE_NICE && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                showError(getString(R.string.needLocationPermissionRationale), (dialogInterface, i) -> ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_BE_AGGRESSIVE));
            }
            else {
                init();
            }
        }
        else {
            Log.w(TAG, "no permission results. code: " + requestCode);
        }
    }

    public void askForLocationPermissionAndInitAfterwards() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            init();
        }
        else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                showError(getString(R.string.needLocationPermissionRationale), (dialogInterface, i) -> ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_BE_AGGRESSIVE));
            }
            else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_BE_NICE);
            }
        }
    }

    @Override
    public void handleWebsiteResponse(Response response) {
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mViewModel.setWaitingForNetwork(false);
        startActivity(intent);
        finish();
    }

    public void init() {
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        createObservers();
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mNameView = findViewById(R.id.username);

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE) {
                mViewModel.login(mNameView.getText().toString(), mPasswordView.getText().toString());
                return true;
            }
            return false;
        });

        mSignInButton = findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(view -> mViewModel.login(mNameView.getText().toString(), mPasswordView.getText().toString()));
    }

    /**
     * creates observers for LiveData attributes from ViewModel
     */
    private void createObservers() {
        mViewModel.getError().observe(this, error -> {
            switch (error) {
                case LoginViewModel.UNAUTHORIZED:
                    mPasswordView.setError(getResources().getString(R.string.wrong_login));
                    mPasswordView.requestFocus();
                    break;
                case LoginViewModel.EMPTY_OR_INVALID_USERNAME:
                    mNameView.setError(getResources().getString(R.string.error_incorrect_uname));
                    mNameView.requestFocus();
                    break;
                case LoginViewModel.EMPTY_OR_INVALID_PWD:
                    mPasswordView.setError(getResources().getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                    break;
                case "":
                    break;
                default:
                    try {
                        Integer.parseInt(error);
                        SavedPreferences.savePreference(this, SavedPreferences.Filename.USERNAME, mNameView.getText().toString());
                        SavedPreferences.savePreference(this, SavedPreferences.Filename.PASSWORD, mPasswordView.getText().toString());
                        SavedPreferences.savePreference(this, SavedPreferences.Filename.UID, error);
                        LocationServiceHelper.scheduleNextAlarm(this, this, mNameView.getText().toString(), mPasswordView.getText().toString());
                    } catch (NumberFormatException nfe) {
                        Log.e(TAG, error);
                        showError(getString(R.string.networkError), (dialogInterface, i) -> {
                        });
                    }
            }
        });

        mViewModel.getWaitingForNetwork().observe(this, (Boolean waiting) -> {
            mNameView.setEnabled(!waiting);
            mPasswordView.setEnabled(!waiting);
            mSignInButton.setEnabled(!waiting);

            if (!waiting && mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                return;
            }
            else if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
                mProgressDialog.setMessage(getString(R.string.waitingForLogin));
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(false);
            }
            mProgressDialog.show();
        });
    }
}