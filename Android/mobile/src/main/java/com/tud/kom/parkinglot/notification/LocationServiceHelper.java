package com.tud.kom.parkinglot.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.parser.ReservationParser;
import com.tud.kom.parkinglot.network.NetworkCallback;
import com.tud.kom.parkinglot.network.RequestAbstraction;
import com.tud.kom.parkinglot.notification.location.LocationJobService;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * various helper methods. all are static
 */
public abstract class LocationServiceHelper {
    private static final String TAG = LocationServiceHelper.class.getSimpleName();

    /**
     * @param reservations start-time-ordered list of reservations
     * @return reservation, which should be scheduled for alarm. can be null
     */
    public static Reservation nextReservationForAlarm(ArrayList<Reservation> reservations) {
        if (reservations == null)
            return null;
        for (Reservation res : reservations)
            if (!res.isNotified())
                return res;
        return null;
    }

    /**
     * tries to schedule next service-alarm
     *
     * @param context  context
     * @param username username
     * @param password password
     */
    public static void scheduleNextAlarm(Context context, NetworkCallback finishedCallback, String username, String password) {
        RequestAbstraction.getReservations(response -> {
            if (response.getRespCode() == 200) {
                try {
                    Reservation chosenReservation = nextReservationForAlarm(ReservationParser.parseReservationsArray(response.getResp()));
                    if (chosenReservation == null) {
                        Log.d(TAG, "No New Job Scheduled");
                        if (finishedCallback != null)
                            finishedCallback.handleWebsiteResponse(response);
                        return;
                    }
                    Log.d(TAG, "Schedule Job for: " + chosenReservation.getStartTime());
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    if (alarmManager == null)
                        return;
                    PendingIntent pendingIntent;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        pendingIntent = PendingIntent.getForegroundService(context, 0, new Intent(context, LocationJobService.class), PendingIntent.FLAG_CANCEL_CURRENT);
                    else
                        pendingIntent = PendingIntent.getService(context, 0, new Intent(context, LocationJobService.class), PendingIntent.FLAG_CANCEL_CURRENT);
                    alarmManager.cancel(pendingIntent);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, chosenReservation.getStartTime().getTime(), pendingIntent);
                    else
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, chosenReservation.getStartTime().getTime(), pendingIntent);
                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
            }
            if (finishedCallback != null)
                finishedCallback.handleWebsiteResponse(response);
        }, username, password);
    }
}
