package com.tud.kom.parkinglot.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tud.kom.parkinglot.SavedPreferences;
import com.tud.kom.parkinglot.activities.login.LoginActivity;
import com.tud.kom.parkinglot.activities.navigation.NavigationActivity;
import com.tud.kom.parkinglot.activities.overview.OverviewFragment;

public class MainActivity extends AppCompatActivity {

    /**
     * Launches the {@link LoginActivity} if no username or password is stored. Else launches the {@link
     * OverviewFragment}.
     */
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent;
        if (SavedPreferences.getPreference(this, SavedPreferences.Filename.USERNAME) == null || SavedPreferences.getPreference(this, SavedPreferences.Filename.PASSWORD) == null || SavedPreferences.getPreference(this, SavedPreferences.Filename.UID) == null)
            intent = new Intent(this, LoginActivity.class);
        else
            intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);
        finish();
    }
}