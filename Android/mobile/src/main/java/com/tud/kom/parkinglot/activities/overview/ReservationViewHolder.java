package com.tud.kom.parkinglot.activities.overview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tud.kom.parkinglot.R;
import com.tud.kom.parkinglot.activities.reservation.ReservationActivity;
import com.tud.kom.parkinglot.base.BaseViewHolder;
import com.tud.kom.parkinglot.model.ParkingLot;
import com.tud.kom.parkinglot.model.Reservation;
import com.tud.kom.parkinglot.model.Site;

/**
 * Binds the data of a reservation to a item view of the list.
 */
public class ReservationViewHolder extends BaseViewHolder<Reservation> {

    private final CardView mCard;

    private final Context mContext;

    private final ImageButton mDelete;

    private final TextView mInstruction;

    private final MapView mMap;

    private final TextView mName;

    private final Button mNavigate;

    private final Button mNewReservation;

    private final TextView mSubheader;

    private final TextView mTime;

    /**
     * From the View "itemView" the Card, Instruction, Map, Name, Description, Date, the navigate-button and the
     * extend-button are
     * parsed into their corresponding class variables which are named by the convention: m + <ATTRIBUTE>
     *
     * @param itemView The view to get the specified elements from
     * @param context  The context
     */
    public ReservationViewHolder(final View itemView, final Context context) {
        super(itemView);
        this.mContext = context;

        mCard = itemView.findViewById(R.id.card);
        mInstruction = itemView.findViewById(R.id.instruction);
        mMap = itemView.findViewById(R.id.map);
        mName = itemView.findViewById(R.id.name);
        mSubheader = itemView.findViewById(R.id.site);
        mNavigate = itemView.findViewById(R.id.button_navigate);
        mNewReservation = itemView.findViewById(R.id.button_new_reservation);
        mDelete = itemView.findViewById(R.id.button_delete);
        mTime = itemView.findViewById(R.id.time);
    }

    /**
     * Binds parking lot data to a reservation
     *
     * @param reservation The reservation to bind the parking lot data to
     */
    @Override
    public void bind(Reservation reservation) {
        LatLng parkingLotLocation = reservation.getParkingLot().getLatLng();

        bindHeaders(reservation);
        bindMap(parkingLotLocation);
        bindInstructions(reservation);
        bindNavigation(parkingLotLocation);
        bindNewReservation(reservation);
        bindDelete(reservation);
    }

    private void bindDelete(final Reservation reservation) {
        if (mDelete != null) {
            if (!reservation.isActive()) {
                mDelete.setVisibility(View.VISIBLE);
            }
            else {
                mDelete.setVisibility(View.INVISIBLE);
            }
        }
    }


    private void bindHeaders(final Reservation reservation) {
        mName.setText(reservation.getParkingLot().getName());
        mSubheader.setText(reservation.getParkingLot().getSites()[0].getName());
        if (mTime != null) {
            mTime.setText(reservation.getReservationTimePeriod(mContext));
        }
    }

    private void bindInstructions(final Reservation reservation) {
        if (mInstruction == null) {
            return;
        }

        if (reservation.getParkingLot().isOpen()) {
            displayParkinglotOpen();
        }
        else {
            displayReservationStatus(reservation);
        }
    }

    private void bindMap(LatLng latLng) {
        if (mMap != null) {
            mMap.onCreate(null);
            mMap.getMapAsync(googleMap -> showMap(googleMap, latLng));
        }
    }

    private void bindNavigation(LatLng latLng) {
        if (mNavigate != null) {
            mNavigate.setOnClickListener(view -> startNavigation(latLng));
        }
    }

    private void bindNewReservation(final Reservation reservation) {
        if (mNewReservation != null) {
            mNewReservation.setOnClickListener(view -> showNewReservation(reservation));
        }
    }

    private void displayCardStatus(@ColorRes int color, @StringRes int instructionText) {

        setMapMarginTop(getPixelFromDp(24));

        mCard.setBackgroundColor(itemView.getContext().getResources().getColor(color));

        mInstruction.setVisibility(View.VISIBLE);
        mInstruction.setText(mContext.getText(instructionText));
    }

    /**
     * If the reservation is currently active and the lot has been opened the appearence of the card changes so that
     * the
     * background color is R.color.reservation_open and a text specified in strings.xml::close_parkinglot appears
     */
    private void displayParkinglotOpen() {
        displayCardStatus(R.color.reservation_open, R.string.close_parkinglot);
    }

    private void displayReservationActive() {
        displayCardStatus(R.color.reservation_active, R.string.open_parkinglot);
    }

    private void displayReservationInactive() {
        setMapMarginTop(0);
        mCard.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.reservation_normal));
        mInstruction.setVisibility(View.GONE);
    }

    /**
     * Changes the appearence of the card depending on its status i.e if the reservation is active
     *
     * @param reservation The reservation that is displayed by the card
     */
    private void displayReservationStatus(final Reservation reservation) {
        if (reservation.isActive()) {
            displayReservationActive();
        }
        else {
            displayReservationInactive();
        }
    }

    private int getPixelFromDp(final int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mContext.getResources().getDisplayMetrics());

    }

    private void setMapMarginTop(final int top) {
        if (mMap != null) {
            ((MarginLayoutParams) mMap.getLayoutParams()).setMargins(0, top, 0, 0);
        }
    }

    /**
     * Responsible to show a map at the specified position
     *
     * @param googleMap The Map to show the position on
     * @param latLng    the position to mark on the map
     */
    private void showMap(final GoogleMap googleMap, final LatLng latLng) {
        MapsInitializer.initialize(mContext);
        UiSettings ui = googleMap.getUiSettings();
        ui.setMapToolbarEnabled(false);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    private void showNewReservation(final Reservation reservation) {
        ParkingLot parkingLot = reservation.getParkingLot();
        Site site = parkingLot.getSites()[0];

        Intent intent = new Intent(mContext, ReservationActivity.class);
        intent.putExtra(ReservationActivity.EXTRA_PARKINGLOT_ID, reservation.getParkingLot().getId());
        intent.putExtra(ReservationActivity.EXTRA_SITE_ID, site.getId());
        intent.putExtra(ReservationActivity.EXTRA_ZONE_ID, site.getZones()[0].getId());
        mContext.startActivity(intent);
    }

    /**
     * Starts the navigation in google maps to the location specified in latLng
     *
     * @param latLng The position to navigate to
     */
    private void startNavigation(LatLng latLng) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latLng.latitude + "," + latLng.longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mContext.startActivity(mapIntent);
    }
}
