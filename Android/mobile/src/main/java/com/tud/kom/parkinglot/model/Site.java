package com.tud.kom.parkinglot.model;

import java.io.Serializable;
import java.util.Arrays;

public class Site implements Serializable {
    private String description;
    private int id;
    private String name;
    private String prefix;

    // location
    private Zone[] zones;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Zone[] getZones() {
        return zones;
    }

    public void setZones(final Zone[] zones) {
        this.zones = zones;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public String toString() {
        return "Site{" + "id=" + id + ", name='" + name + '\'' + ", description='" + description + '\'' + ", prefix='" + prefix + '\'' + ", zones=" + Arrays.toString(zones) + '}';
    }
}
