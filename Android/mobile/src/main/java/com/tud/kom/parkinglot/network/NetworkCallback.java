package com.tud.kom.parkinglot.network;

/**
 * interface for handling the network response
 */
public interface NetworkCallback {
    /**
     * callback, handle the website response
     *
     * @param response response
     */
    void handleWebsiteResponse(Response response);
}
